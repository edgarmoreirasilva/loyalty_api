## #####################################################################
## 																	  ##
##                        E-DEPLOY - CONSULTING						  ##
##                                            						  ##
## #####################################################################
## 																	  ##
##   README COM INFORMAÇÕES PARA MONTAGEM DO AMBIENTE DE AUTOMAÇÃO    ##
##   E EXECUÇÃO LOCAL 								                  ##
##            													      ##
## #####################################################################
## CRIADO POR: EDGAR SILVA 									  		  ##
## DATA: 15/12/2020													  ##
## ÁREA: QUALITY ASSURANCE                                            ##
## #####################################################################


TODO: Atualizar o Readme

<!-- 
## #####################################################################
## 																	  ##
##                   CONFIGURAÇÃO ADICIONAL - MAC OS				  ##
##                                            						  ##
## #####################################################################

### Abrir o terminal e executar os seguintes passos:

* Instalar homebrew

		$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"


## #####################################################################
## 																	  ##
##                        ESTRUTURA DO PROJETO  					  ##
##                                            						  ##
## #####################################################################

* No projeto, temos a seguinte estrutura de pastas:

	- app
	- features
	- logs
	- resources
		- keywords
		- pages
		- libs

* Segue descrição das pastas que fazem parte do projeto:

	- app: Nesta pasta ficarão presentes todos os .apks que serão utilizados no nosso teste.
	- logs: Pasta responsável por armazenar as evidências e relatórios pós execução.
	- resources: Na pasta resources, teremos todos os recursos como keywords, pages e libs que serão utilizadas no projeto.
	- keywords: Aqui estarão todas as keywords criadas no projeto separadas por features.
	- pages: Na pasta pages estarão todos os pages objects mapeados por tela.
	- libs: A pasta libs contém todas as libs e implementações externas que poderão ser aplicadas e utilizadas em nossos testes.
	- features: Pasta responsável por amazenarem todos os BDD's e steps dos nossos testes.


## #####################################################################
## 																	  ##
##                CAPABILITIES E CONFIGURAÇÕES GERAIS  			      ##
##                                            						  ##
## #####################################################################

* Onde estão os capabilities do Appium:

	- resources
		- hooks.robot

* Todos os capabilities do Appium, estão presentes dentro da pasta /resources no arquivo hooks.robot
* Qualquer ajuste de capabilitie deverá ser feito apenas no hooks.robot

* Temos um arquivo chamado base.robot que se encontra na seguinte hierarquia:

	- resources
		- base.robot

* Esse arquivo serve para concentrar todas as Libraries e Rosources que serão importados e utilizados no projeto.
* Sendo assim, qualquer recurso ou Library nova, devem ser passandos apenas neste arquivo.


## #####################################################################
## 																	  ##
##                        EXECUTANDO OS TESTES  					  ##
##                                            						  ##
## #####################################################################

* Para executarmos os testes, precisamos abrir o terminal integrado na pasta raiz do projeto.*
* Neste caso a raiz é a pasta automacao-app. 
* Para executarmos todos os testes existentes no projeto, devemos executar o seguinte comando:

	- $ robot -d ./logs features/

* Já para executarmos uma feature específica, devemos informar qual .robot querermos executar.
* Neste caso, vamos executar os testes de login como Exemplo:

	- $ robot -d ./logs features/login.robot

* Também podemos executar cenários específicos, desde que os mesmos possuam sua Tag única. 
* Como exemplo, podemos utilizar os testes de account. Cada cenário possui sua Tag unica e neste caso vamos executar o cenário de Inclusão de endereço.
* Para isso, inserímos o comando -i e logo a frente a tag que desejamos executar. Veja no exemplo:

	- $ robot -d ./logs -i Incluir features/account.robot

* Caso queira alterar o locals onde os logs são salvos, basta ajustar o caminho após o -d.
* Exemplo: Iremos passar no comando que os logs/reports serão salvos na pasta reports:

	- $ robot -d ./reports features/login.robot -->