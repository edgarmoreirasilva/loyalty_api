*** Settings ***
Resource    ../base.robot

*** Keywords ***

setupApiconsumer
    Create Session    customer    https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/enrollment    verify=true


Cadastrar-se na promoção com sucesso - Status Code 201
    ${resp}             ${payload}=    PostNewConsumidor 
    Status Should Be    201            ${resp}

Cadastrar-se na promoção sem informar o CPF - Status Code 422
    ${resp}             ${payload}=    PostNewConsumidorSemCPF 
    Status Should Be    422            ${resp}

Cadastrar-se na promoção sem informar o Email - Status Code 422
    ${resp}             ${payload}=    PostNewConsumidorSemEmail
    Status Should Be    422            ${resp}

Cadastrar-se na promoção sem informar o name - Status Code 422
    ${resp}             ${payload}=    PostNewConsumidorSemName
    Status Should Be    422            ${resp}

Cadastrar-se na promoção sem informar o customer_id - Status Code 422
    ${resp}             ${payload}=    PostNewConsumidorSemCustomerID
    Status Should Be    422            ${resp}

Cadastrar-se na promoção passando um String no parametro cpf - Status Code 422
    ${resp}             ${payload}=    PostNewConsumidorStringCPF
    Status Should Be    422            ${resp}

Cadastrar-se na promoção passando uma Booleano no parametro cpf - Status Code 422
    ${resp}             ${payload}=    PostNewConsumidorBooleanCPF
    Status Should Be    422            ${resp}

Cadastrar-se na promoção passando uma Inteiro no parametro email - Status Code 422
    ${resp}             ${payload}=    PostNewConsumidorIntEmail
    Status Should Be    422            ${resp}

Cadastrar-se na promoção passando uma Booleano no parametro email - Status Code 422
    ${resp}             ${payload}=    PostNewConsumidorBooleanEmail
    Status Should Be    422            ${resp}

Cadastrar-se na promoção passando uma Inteiro no parametro name - Status Code 201
    ${resp}             ${payload}=    PostNewConsumidorIntName
    Status Should Be    201            ${resp}

Cadastrar-se na promoção passando uma Booleano no parametro name - Status Code 201
    ${resp}             ${payload}=    PostNewConsumidorbooleanName
    Status Should Be    201            ${resp}

Cadastrar-se na promoção passando um inteiro no parametro customer_id - Status Code 201
    ${resp}             ${payload}=    PostNewConsumidorIntCustomerId
    Status Should Be    201            ${resp}

Cadastrar-se na promoção passando um Booleano no parametro customer_id - Status Code 201
    ${resp}             ${payload}=    PostNewConsumidorbooleanCustomerId
    Status Should Be    201            ${resp}

Tentar se cadastrar removendo o Body Required - Status Code 422
    ${resp}=            PostNewConsumidorSemBody
    Status Should Be    422                         ${resp}

Tentar se cadastrar removendo do Header o x-api-token - Status Code 403
    ${resp}             ${payload}=    PostNewConsumidorSemToken
    Status Should Be    403            ${resp}

Tentar se cadastrar alterando a URL do Endpoint - Status Code 404
    ${resp}             ${payload}=    PostNewConsumidorUrlIncorreta
    Status Should Be    404            ${resp}

Realizar cadastro com um CPF já cadastrado na FIELO - Status Code 409
    ${resp}             ${payload}=    PostNewConsumidorCPFExistente
    Status Should Be    409            ${resp}

Realizar cadastro com um email já cadastrado na FIELO - Status Code 409
    ${resp}             ${payload}=    PostNewConsumidorEmailExistente
    Status Should Be    409            ${resp}

Realizar cadastro com um name já cadastrado na FIELO - Status Code 201
    ${resp}             ${payload}=    PostNewConsumidorNomeExistente
    Status Should Be    201            ${resp}

Realizar cadastro com um customer_id já cadastrado na FIELO - Status Code 409
    ${resp}             ${payload}=    PostNewConsumidorCustomerIdExistente
    Status Should Be    409            ${resp}

Deletar usuario da Campanha - Status Code 204
    ${na}               ${payloadCreate}=    PostNewConsumidor
    ${resp}=            DeleteConsumidor     ${payloadCreate.customer_id}
    Status Should Be    204                  ${resp}

Deletar passando um customer_id inexistente - Status Code 404
    ${resp}=            DeleteConsumidor    1234563ads2d135135d1a3sd
    Status Should Be    404                 ${resp}

Deletar customer sem informar o x-api-key - Status Code 403
    ${resp}=            DeleteConsumidorSemXApi    123456
    Status Should Be    403                        ${resp}

Alterar a URL do Endpoint DELETE - Status Code 404
    ${resp}=            DeleteConsumidorUrlIncorreta    123456
    Status Should Be    404                             ${resp}

Consultar cpf com sucesso - Status Code 200
    #criar um novo consumidor para o teste:
    ${na}               ${payload}=        PostNewConsumidor
    #buscar o consumidor com sucesso:
    ${resp}             BuscaComsumidor    ${payload.cpf}
    Status Should Be    200                ${resp}

Consultar CPF não cadastrado no Loyalty - Status Code 404
    ${resp}             BuscaComsumidor    16684948090
    Status Should Be    404                ${resp}

Consultar CPF deletado do Loyalty - Status Code 404
    ${na}               ${payloadCreate}=    PostNewConsumidor
    ${respDelete}=      DeleteConsumidor     ${payloadCreate.customer_id}
    Status Should Be    204                  ${respDelete}
    ${respBusca}        BuscaComsumidor      ${payloadCreate.cpf}
    Status Should Be    404                  ${respBusca}

Executar requisição sem informar CPF - Status Code 405
    ${resp}             BuscaComsumidor    ${EMPTY}
    Status Should Be    405                ${resp}


Executar requisição removendo o Header x-api-key - Status Code 403
    ${resp}             BuscaComsumidorSemXApi    16684948090
    Status Should Be    403                       ${resp}

# ----------------------------------------------------------------------------
PostNewConsumidor
    setupApiconsumer
    ${cpf_faker}        FakerLibrary.cpf
    ${email_faker}      FakerLibrary.Email
    ${uuid}             generateUUID
    &{headers}=         Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=         Create Dictionary       cpf=${cpf_faker}
    ...                 email=${email_faker}
    ...                 name=Teste User 001
    ...                 customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorSemCPF
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf=${EMPTY}
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorSemEmail
    ${cpf_faker}    FakerLibrary.cpf
    ${uuid}         generateUUID
    &{headers}=     Create Dictionary      content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=     Create Dictionary      cpf=${cpf_faker}
    ...             email=${EMPTY}
    ...             name=Teste User 001
    ...             customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorSemName
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorSemCustomerID
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${EMPTY}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorStringCPF
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf= '68322766050'
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorBooleanCPF
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf= true
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorIntEmail
    ${cpf_faker}    FakerLibrary.cpf
    ${uuid}         generateUUID
    &{headers}=     Create Dictionary      content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=     Create Dictionary      cpf=${cpf_faker}
    ...             email=${123456789}
    ...             name=Teste User 001
    ...             customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorBooleanEmail
    ${cpf_faker}    FakerLibrary.cpf
    ${uuid}         generateUUID
    &{headers}=     Create Dictionary      content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=     Create Dictionary      cpf=${cpf_faker}
    ...             email=true
    ...             name=Teste User 001
    ...             customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}


PostNewConsumidorIntName
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=${123456789}
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorbooleanName
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=true
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorIntCustomerId

    ${cpf_faker}      FakerLibrary.cpf
    ${number}         FakerLibrary.Random Number
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary             content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${number}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorbooleanCustomerId
    DeleteConsumidor    ${true}

    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${true}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}


PostNewConsumidorSemBody
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary     content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=          Post Request          customer                         /customer                                             headers=${headers}
    [Return]          ${resp}               

PostNewConsumidorSemToken
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    
    &{payload}=       Create Dictionary       cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorUrlIncorreta
    Create Session    customerUrlIncorreta    https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/teste    verify=true
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json                                       x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customerUrlIncorreta    /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}


PostNewConsumidorCPFExistente
    ${na}             ${payload}=             PostNewConsumidor
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf=${payload.cpf}
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorEmailExistente
    ${na}           ${payload}=               PostNewConsumidor
    ${cpf_faker}    FakerLibrary.cpf
    ${uuid}         generateUUID
    &{headers}=     Create Dictionary         content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=     Create Dictionary         cpf=${payload.cpf}
    ...             email=${payload.email}
    ...             name=Teste User 001
    ...             customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorNomeExistente
    ${na}             ${payload}=             PostNewConsumidor
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    ${uuid}           generateUUID
    &{headers}=       Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary       cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=${payload.name}
    ...               customer_id=${uuid}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

PostNewConsumidorCustomerIdExistente
    ${na}             ${payload}=                           PostNewConsumidor
    ${cpf_faker}      FakerLibrary.cpf
    ${email_faker}    FakerLibrary.Email
    &{headers}=       Create Dictionary                     content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary                     cpf=${cpf_faker}
    ...               email=${email_faker}
    ...               name=Teste User 001
    ...               customer_id=${payload.customer_id}

    ${resp}=    Post Request    customer      /customer    data=${payload}    headers=${headers}
    [Return]    ${resp}         ${payload}

DeleteConsumidor
    [Arguments]    ${uuid}
    &{headers}=    Create Dictionary    content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Delete Request       customer                         /customer/${uuid}                                     headers=${headers}
    [Return]       ${resp}


DeleteConsumidorSemXApi
    [Arguments]    ${uuid}
    &{headers}=    Create Dictionary    content-type=application/json    
    ${resp}=       Delete Request       customer                         /customer/${uuid}    headers=${headers}
    [Return]       ${resp}

DeleteConsumidorUrlIncorreta
    [Arguments]       ${uuid}
    Create Session    customerIncorreto    https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/enrollment    verify=true
    &{headers}=       Create Dictionary    content-type=application/json                                            x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=          Delete Request       customerIncorreto                                                        /teste/${uuid}                                        headers=${headers}
    [Return]          ${resp}


BuscaComsumidor
    [Arguments]       ${cpf}
    # remover os caracteres especiais
    ${user_cpf}=      Replace String                                        ${cpf}                                                                   .              ${EMPTY}
    ${user_cpf}=      Replace String                                        ${user_cpf}                                                              -              ${EMPTY}
    Create Session    customer                                              https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/enrollment    verify=true
    &{headers}=       Create Dictionary                                     content-type=application/json 
    ...               x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=          Get Request                                           customer
    ...               /customer/${user_cpf}                                 headers=${headers}
    [Return]          ${resp}

BuscaComsumidorSemXApi
    [Arguments]       ${cpf}
    # remover os caracteres especiais
    ${user_cpf}=      Replace String           ${cpf}                                                                   .              ${EMPTY}
    ${user_cpf}=      Replace String           ${user_cpf}                                                              -              ${EMPTY}
    Create Session    customer                 https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/enrollment    verify=true
    &{headers}=       Create Dictionary        content-type=application/json 
    ${resp}=          Get Request              customer
    ...               /customer/${user_cpf}    headers=${headers}
    [Return]          ${resp}

CriaConsumidor
    setupApiconsumer
    ${cpf_faker}        FakerLibrary.cpf
    ${email_faker}      FakerLibrary.Email
    ${uuid}             generateUUID
    &{headers}=         Create Dictionary       content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=         Create Dictionary       cpf=${cpf_faker}
    ...                 email=${email_faker}
    ...                 name=Teste User 001
    ...                 customer_id=${uuid}
    ${resp}=            Post Request            customer                         /customer                                             data=${payload}    headers=${headers}
    [Return]            ${resp}                 ${payload}

Excluir consumidor com sucesso
    #criar um novo consumidor para o teste:
    ${na}               ${payload}=         PostNewConsumidor
    #deletar o consumidor criado:
    ${resp}             DeleteConsumidor    ${payload.customer_id}
    Status Should Be    204                 ${resp}




