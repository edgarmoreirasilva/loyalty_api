*** Settings ***
Resource    ../base.robot

*** Keywords ***

setupApihome
    Create Session    consumer    https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/customer/home    verify=true

Executar GET com customer_id cadastrado no Loyalty - Status Code 200
    ${resp}             GetConsumerHome
    Status Should Be    200                ${resp}

Executar GET com um customer_id não cadastrado no Loyalty - Status Code 404
    ${resp}             GetConsumerNaoCadastradoHome
    Status Should Be    404                             ${resp}

Executar GET sem informar o parametro customer_id - Status Code 404
    ${resp}             GetConsumerSemID
    Status Should Be    404                 ${resp}

Executar GET sem informar o Header x-api-key - Status Code 403
    ${resp}             GetConsumerSemXApiKey
    Status Should Be    403                      ${resp}

Executar GET passando uma URL incorreta - Status Code 404
    ${resp}             GetConsumerURLincorreta
    Status Should Be    404                        ${resp}

Executar GET na API Extract com customer_id cadastrado no Loyalty - Status Code 200
    ${resp}             GetConsumerExtract
    Status Should Be    200                   ${resp}

Executar GET na API Extract com um customer_id não cadastrado no Loyalty - Status Code 404
    ${resp}             GetConsumerExtractNaoCadastrado
    Status Should Be    404                                ${resp}

Executar GET na API Extract sem informar o parametro customer_id - Status Code 404
    ${resp}             GetConsumerExtractSemID 
    Status Should Be    404                         ${resp}

Executar GET na API Extract sem informar o Header x-api-key - Status Code 403
    ${resp}             GetConsumerExtractSemXApiKey
    Status Should Be    403                             ${resp}

Executar GET na API Extract passando uma URL incorreta - Status Code 404
    ${resp}             GetConsumerExtractURLincorreta
    Status Should Be    404                               ${resp}

Executar GET na API de Point Forecast com customer_id cadastrado no Loyalty - Status Code 200
    ${resp}             GetConsumerPointForecast
    Status Should Be    200                         ${resp}
Executar GET na API de Point Forecast com um customer_id não cadastrado no Loyalty - Status Code 404
    ${resp}             GetConsumerPointForecastNaoCadastrado
    Status Should Be    404                                      ${resp}
Executar GET na API de Point Forecast sem informar o parametro customer_id - Status Code 404
    ${resp}             GetConsumerPointForecastSemID
    Status Should Be    404                              ${resp}
Executar GET na API de Point Forecast sem informar o Header x-api-key - Status Code 403
    ${resp}             GetConsumerPointForecastSemXApiKey
    Status Should Be    403                                   ${resp}
Executar GET na API de Point Forecast passando uma URL incorreta - Status Code 404
    ${resp}             GetConsumerPointForecastURLincorreta
    Status Should Be    404                                     ${resp}

# --------------------------------------------------------------------------------
#    API Home
# --------------------------------------------------------------------------------

GetConsumerHome
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /${payload.customer_id}    headers=${headers}
    [Return]       ${resp}              

GetConsumerNaoCadastradoHome
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /a9399b7e-91d1-4298-b847-95626db87ac8    headers=${headers}
    [Return]       ${resp}              

GetConsumerSemID
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /    headers=${headers}
    [Return]       ${resp}              

GetConsumerSemXApiKey
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=
    ${resp}=       Get Request          consumer                       /${payload.customer_id}    headers=${headers}
    [Return]       ${resp}              

GetConsumerURLincorreta
    Create Session    consumerUrlncorreta    https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/customer/teste    verify=true
    ${resp}           ${payload}             kws_customer.CriaConsumidor
    &{headers}=       Create Dictionary      x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=          Get Request            consumerUrlncorreta                                                          /${payload.customer_id}    headers=${headers}
    [Return]          ${resp}                

# --------------------------------------------------------------------------------
#    API Extract
# --------------------------------------------------------------------------------
GetConsumerExtract
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /${payload.customer_id}/transactions    headers=${headers}
    [Return]       ${resp}              

GetConsumerExtractNaoCadastrado
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /a9399b7e-91d1-4298-b847-95626db87ac8    headers=${headers}
    [Return]       ${resp}              

GetConsumerExtractSemID
    ${Consumer}    set variable         ${EMPTY}
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /${Consumer}/transactions    headers=${headers}
    [Return]       ${resp}              

GetConsumerExtractSemXApiKey
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=
    ${resp}=       Get Request          consumer                       /${payload.customer_id}/transactions    headers=${headers}
    [Return]       ${resp}              

GetConsumerExtractURLincorreta
    Create Session    consumerUrlncorreta    https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/customer/teste    verify=true
    ${resp}           ${payload}             kws_customer.CriaConsumidor
    &{headers}=       Create Dictionary      x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=          Get Request            consumerUrlncorreta                                                          /${payload.customer_id}    headers=${headers}
    [Return]          ${resp}                

# --------------------------------------------------------------------------------
#    API Point Forecast
# --------------------------------------------------------------------------------
GetConsumerPointForecast
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /${payload.customer_id}/pointsforecast    headers=${headers}
    [Return]       ${resp}              

GetConsumerPointForecastNaoCadastrado
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /a9399b7e-91d1-4298-b847-95626db87ac8    headers=${headers}
    [Return]       ${resp}              

GetConsumerPointForecastSemID
    ${Consumer}    set variable         ${EMPTY}
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /${Consumer}/pointsforecast    headers=${headers}
    [Return]       ${resp}              

GetConsumerPointForecastSemXApiKey
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=
    ${resp}=       Get Request          consumer                       /${payload.customer_id}/pointsforecast    headers=${headers}
    [Return]       ${resp}              

GetConsumerPointForecastURLincorreta
    Create Session    consumerUrlncorreta    https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/customer/teste    verify=true
    ${resp}           ${payload}             kws_customer.CriaConsumidor
    &{headers}=       Create Dictionary      x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=          Get Request            consumerUrlncorreta                                                          /${payload.customer_id}    headers=${headers}
    [Return]          ${resp}                

Consultar as transações do consumidor com sucesso
    [Tags]              consumer_transactions
    ${resp}             GetConsumertransactions    
    Status Should Be    200                        ${resp}

Consultar o período de expiração dos pontos do consumidor com sucesso
    [Tags]              consumer_forecast
    ${resp}             GetConsumerforecast    
    Status Should Be    200                    ${resp}

GetConsumertransactions
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /${payload.customer_id}/transactions    headers=${headers}
    [Return]       ${resp}              

GetConsumerforecast
    ${resp}        ${payload}           kws_customer.CriaConsumidor
    &{headers}=    Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=       Get Request          consumer                                              /${payload.customer_id}/pointsforecast    headers=${headers}
    [Return]       ${resp}              
