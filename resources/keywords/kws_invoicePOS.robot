*** Settings ***
Resource    ../base.robot

*** Keywords ***

setupApiOrderPicture
    Create Session    OrcderPicture    https://loyalty-dev.appbk.burgerking.com.br/order/pump/sale    verify=true


Executar Requisição com sucesso enviando informações corretas - Status Code 200
    ${resp}             PostOrderPicture
    Status Should Be    200                 ${resp}

Executar Requisição sem informar o Header x-apy-key - Status Code 403
    ${resp}             PostOrderPictureSemToken
    Status Should Be    403                         ${resp}
Executar Requisição alterando o valor do Header x-api-key - Status Code 403
    ${resp}             PostOrderPictureTokenErrado
    Status Should Be    403                            ${resp}

Executar Requisição sem informar o serviço com Store Code Inexistente
    ${resp}             PostOrderPictureStoreCodeInexistente
    Status Should Be    404                                     ${resp}

Enviar um valor inteiro no paramêtro store_code
    ${resp}             PostOrderPictureStoreCodeInteiro
    Status Should Be    422                                 ${resp}

Enviar um valor Booleano no paramêtro store_code
    ${resp}             PostOrderPictureStoreCodeBoolean
    Status Should Be    422                                 ${resp}

Enviar caracter especial no paramêtro store_code - Status Code 422
    ${resp}             PostOrderPictureStoreCodecaractere
    Status Should Be    422                                   ${resp}

Executar requisição com um order_code já utilizado
    ${resp}             PostOrderPictureOrderCodeJaUsado
    Status Should Be    422                                 ${resp}

Enviar um valor inteiro no paramêtro order_code - Status Code 422
    ${resp}             PostOrderPictureOrderCodeinteiro
    Status Should Be    422                                 ${resp}

Enviar um valor Booleano no paramêtro order_code - Status Code 422
    ${resp}             PostOrderPictureOrderCodeboolean
    Status Should Be    422                                 ${resp}
Enviar caracter especial no paramêtro order_code - Status Code 422
    ${resp}             PostOrderPictureOrderCaractereEspecial
    Status Should Be    422                                       ${resp}

Enviar um valor inteiro no paramêtro pos-code - Status Code 422
    ${resp}             PostOrderPicturePosCodeInteiro
    Status Should Be    422                               ${resp}

Enviar um valor Booleano no paramêtro pos-code - Status Code 422
    ${resp}             PostOrderPicturePosCodeBooleano
    Status Should Be    422                                ${resp}

Enviar caracter especial no paramêtro pos-code - Status Code 422
    ${resp}             PostOrderPicturePosCodeCaractereEspecial
    Status Should Be    422                                         ${resp}

Enviar um valor inteiro no paramêtro creation-dttm - Status Code 422
    ${resp}             PostOrderPictureCreationdttmInteiro
    Status Should Be    422                                    ${resp}

Enviar um valor Booleano no paramêtro creation-dttm - Status Code 422
    ${resp}             PostOrderPictureCreationdttmInteiro
    Status Should Be    422                                    ${resp}

Enviar caracter especial no paramêtro creation-dttm - Status Code 422
    ${resp}             PostOrderPictureCreationdttmCaractereEspecial
    Status Should Be    422                                              ${resp}

Enviar com uma data menor do que a do dia recorrente no parametro creation-dttm
    ${resp}             PostOrderPictureCreationdttmDataMenor
    Status Should Be    422                                      ${resp}

Enviar um valor inteiro no paramêtro order-picture-sale-lines - Status Code 422
    ${resp}             PostOrderPictureSaleLinesInteiro
    Status Should Be    422                                 ${resp}

Enviar um valor Booleano no paramêtro order-picture-sale-lines - Status Code 422
    ${resp}             PostOrderPictureSaleLinesBoolean
    Status Should Be    422                                 ${resp}
Enviar caracter especial no paramêtro order-picture-sale-lines - Status Code 422
    ${resp}             PostOrderPictureSaleLinesCaractereEspecial
    Status Should Be    422                                           ${resp}

Executar teste removendo parte do Body da API
    ${resp}             PostOrderPictureSaleRemovendoParteBody
    Status Should Be    422                                       ${resp}

Executar teste enviando o parametro store_code vazio
    ${resp}             PostOrderPictureSaleStoreCodeVazio
    Status Should Be    422                                   ${resp}

Executar teste enviando o parametro order-code vazio
    ${resp}             PostOrderPictureOrderCodeCodeVazio
    Status Should Be    422                                   ${resp}

Executar teste enviando o paramentro pos-code vazio
    ${resp}             PostOrderPicturePosCodeVazio
    Status Should Be    422                             ${resp}

Executar teste enviando o parametro creation-dttm vazio
    ${resp}             PostOrderPicturecreationdttmVazio
    Status Should Be    422                                  ${resp}

Executar teste enviando o parametro order-picture-sale-lines vazio
    ${resp}             PostOrderPictureSaleLinesVazio
    Status Should Be    422                               ${resp}

Executar teste removendo o parametro store_code
    ${resp}             PostOrderPictureSemStroreCode
    Status Should Be    422                              ${resp}
Executar teste removendo o parametro order-code
    ${resp}             PostOrderPictureSemOrderCode
    Status Should Be    422                             ${resp}
Executar teste removendo o paramentro pos-code
    ${resp}             PostOrderPictureSemPosCode
    Status Should Be    422                           ${resp}

Executar teste removendo o parametro creation-dttm
    ${resp}             PostOrderPictureSemCreationdttm
    Status Should Be    422                                ${resp}

Executar teste removendo o parametro order-picture-sale-lines
    ${resp}             PostOrderPictureSemSaleLines
    Status Should Be    422                             ${resp}




setupApiInvoicePos
    Create Session    invoicePos    https://loyalty-dev.appbk.burgerking.com.br    verify=true

GetBenefit
    [Arguments]           ${QR_CODE}
    setupApiInvoicePos
    &{headers}=           Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=              Put Request          invoicePos                                            /voucher/benefit/byVoucher/${QR_CODE}?storeId=99999&posId=10    headers=${headers}
    ${voucher_id}=        Set Variable         ${resp.json()['id']}
    [Return]              ${resp}              ${voucher_id}

BurnBenefit
    [Arguments]           ${voucher_id}
    setupApiInvoicePos
    &{headers}=           Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=              Put Request          invoicePos                                            /voucher/benefit/${voucher_id}/markUsed?    headers=${headers}
    [Return]              ${resp}

GetBenefitused
    [Arguments]           ${QR_CODE}
    setupApiInvoicePos
    &{headers}=           Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=              Put Request          invoicePos                                            /voucher/benefit/byVoucher/${QR_CODE}?storeId=99999&posId=10    headers=${headers}    
    [Return]              ${resp}

    # ------------------------------------------------------------------
    #                 PICUTURE
    # ------------------------------------------------------------------

getPicturebody
    [Arguments]    ${customer_id}    ${order}    ${store_code}    ${pos-code}    ${creation-dttm}

    ${comment}=        Create List                         
    &{order-lines}=    Create Dictionary                   
    ...                comment=${comment}
    ...                unit-price=${0.0}
    ...                plu=1051
    ...                sub-unit-price=${0.0}
    ...                multiplied-qty=${1}
    ...                level=${0}
    ...                menu-item-code=1
    ...                item-type=COMBO
    ...                product=CM Whopper/Q
    ...                qty=${1}
    ...                inc-qty=${0}
    ...                order-picture-id=${129}
    ...                item-price=${0.0}
    ...                dec-qty=${0}
    ...                category-description=${None}
    ...                part-code=${None}
    ...                sub-category-description=${None}
    ...                line-number=${1}
    ...                price-key=${None}
    ...                added-unit-price=${0.0}

    ${order-picture-sale-lines}    Create List    ${order-lines}

    &{tenders1}    Create Dictionary
    ...            reference-amount=${47.8}
    ...            tender-detail={\"ReceiptMerchant\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"AuthCode\": \"1302\", \"IdAuth\": \"08496898000142\", \"ReceiptCustomer\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"TransactionProcessor\": \"00051\", \"CNPJAuth\": \"68536379000192\", \"Bandeira\": \"4\"}
    ...            tender-amount=${47.8}
    ...            order-picture-id=${129}
    ...            change-amount=${0.0}
    ...            tender-type=2

    ${order-picture-tenders}=    Create List    ${tenders1}
    # ${contactinfo}=    Create List    ${entry 1}    ${entry 2}

    &{order-picture-custom-order-properties}    Create Dictionary
    ...                                         SATISFACTION_SURVEY_VALIDATOR=11901112
    ...                                         FISCALIZATION_DATE=2020-09-28T14:29:48
    ...                                         LOYALTY_SYSTEM_USER_ID=${customer_id}
    ...                                         WHOPPER_WIFI_CODE=DBHOEEVX
    ...                                         SATISFACTION_SURVEY_CODE=0190991920892109

    &{payload}=    Create Dictionary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    ...            order-picture-sale-lines=${order-picture-sale-lines}
    ...            tax-applied=${2.42}
    ...            type-id=${0}
    ...            session=pos=1user=5813count=1period=20200928
    ...            xml-data=CiAgICAgICAgPENGZT48aW5mQ0ZlIElkPSJDRmUzNTE2MTA2MTA5OTAwODAwMDE0MTU5OTAwMDA0MjA3MDAwNDY5NjU2NTkyMSIgdmVyc2FvPSIwLjA2IiB2ZXJzYW9EYWRvc0VudD0iMC4wNiIgdmVyc2FvU0I9IjAxMDEwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj42NTY1OTI8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD45MDAwMDQyMDc8L25zZXJpZVNBVD48bkNGZT4xMjk8L25DRmU+PGRFbWk+MjAyMDA5Mjg8L2RFbWk+PGhFbWk+MTEyOTQ4PC9oRW1pPjxjRFY+MTwvY0RWPjx0cEFtYj4yPC90cEFtYj48Q05QSj4xNjcxNjExNDAwMDE3MjwvQ05QSj48c2lnbkFDPlNHUi1TQVQgU0lTVEVNQSBERSBHRVNUQU8gRSBSRVRBR1VBUkRBIERPIFNBVDwvc2lnbkFDPjxhc3NpbmF0dXJhUVJDT0RFPlE0c1VYWCtNdTh0YytocnlBYU9WRXRvU1FVTzNrL25hb3lwZjVsYlpmV2hGZVFQQzhzalNXNWNaU3ZLWkgxd29VMHFrMmYycmVZTEVhWWpGK1FhOU11cnFqbHhaYmxlWFowNExoWDhYQjNXeGxVUDBvdUwyTU5OeUlNaXJDc3VLZWR2RVE4N3c4THpWeDRzK0NvNmIyYUxDYzNzbG5yWURiRWt6NDVUS0pvNnF3cUlvdytYN05nZ2syNmFxbkFQaTFDR3VXMmlzbThVdHlmM2ZOOGdQeUk1TjJQYW02Mmh1N2d1Y01jek9LQXY1V2F3ZCtGOHBFM2xPT2RueFdidXp4cGxFUytIcVFqcmN6cVRydlJVbU5xT1Zqc2N2WEtQQ0JrbmdZeUVHeG11eFA5cFVwV0FHdjg5M1MydWNtSDVjT3gyVFRMY3pLMC9vS1JscmdUZ3NHZz09PC9hc3NpbmF0dXJhUVJDT0RFPjxudW1lcm9DYWl4YT4wMDE8L251bWVyb0NhaXhhPjwvaWRlPjxlbWl0PjxDTlBKPjYxMDk5MDA4MDAwMTQxPC9DTlBKPjx4Tm9tZT5ESU1BUyBERSBNRUxPIFBJTUVOVEEgU0lTVEVNQVMgREUgUE9OVE8gRSBBQ0VTU08gTFREQTwveE5vbWU+PHhGYW50PkRJTUVQPC94RmFudD48ZW5kZXJFbWl0Pjx4TGdyPkFWRU5JREEgTU9GQVJSRUo8L3hMZ3I+PG5ybz44NDA8L25ybz48eENwbD45MDg8L3hDcGw+PHhCYWlycm8+VkwuIExFT1BPTERJTkE8L3hCYWlycm8+PHhNdW4+U0FPIFBBVUxPPC94TXVuPjxDRVA+MDUzMTEwMDA8L0NFUD48L2VuZGVyRW1pdD48SUU+MTExMTExMTExMTExPC9JRT48Y1JlZ1RyaWI+MzwvY1JlZ1RyaWI+PGNSZWdUcmliSVNTUU4+MTwvY1JlZ1RyaWJJU1NRTj48aW5kUmF0SVNTUU4+TjwvaW5kUmF0SVNTUU4+PC9lbWl0PjxkZXN0PjwvZGVzdD48ZGV0IG5JdGVtPSIxIj48cHJvZD48Y1Byb2Q+MTA1MDwvY1Byb2Q+PHhQcm9kPldob3BwZXIvUTwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT45LjAwMDwvdlVuQ29tPjx2UHJvZD45LjAwPC92UHJvZD48aW5kUmVncmE+QTwvaW5kUmVncmE+PHZJdGVtPjkuMDA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MTkwMjIwMDA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjwvcHJvZD48aW1wb3N0bz48SUNNUz48SUNNUzAwPjxPcmlnPjA8L09yaWc+PENTVD4wMDwvQ1NUPjxwSUNNUz4zLjIwPC9wSUNNUz48dklDTVM+MC4yOTwvdklDTVM+PC9JQ01TMDA+PC9JQ01TPjxQSVM+PFBJU0FsaXE+PENTVD4wMTwvQ1NUPjx2QkM+OS4wMDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNC44NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz45LjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjY4LjQwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMiI+PHByb2Q+PGNQcm9kPjYwMTI8L2NQcm9kPjx4UHJvZD5NRCBCYXRhdGE8L3hQcm9kPjxDRk9QPjUwMDA8L0NGT1A+PHVDb20+dW48L3VDb20+PHFDb20+MS4wMDAwPC9xQ29tPjx2VW5Db20+My4wMDA8L3ZVbkNvbT48dlByb2Q+My4wMDwvdlByb2Q+PGluZFJlZ3JhPkE8L2luZFJlZ3JhPjx2SXRlbT4zLjAwPC92SXRlbT48b2JzRmlzY29EZXQgeENhbXBvRGV0PSJDb2QuTkNNIj48eFRleHRvRGV0PjIwMDUyMDAwPC94VGV4dG9EZXQ+PC9vYnNGaXNjb0RldD48L3Byb2Q+PGltcG9zdG8+PElDTVM+PElDTVMwMD48T3JpZz4wPC9PcmlnPjxDU1Q+MDA8L0NTVD48cElDTVM+My4yMDwvcElDTVM+PHZJQ01TPjAuMTA8L3ZJQ01TPjwvSUNNUzAwPjwvSUNNUz48UElTPjxQSVNBbGlxPjxDU1Q+MDE8L0NTVD48dkJDPjMuMDA8L3ZCQz48cFBJUz4xLjY1MDA8L3BQSVM+PHZQSVM+NC45NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4zLjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjIyLjgwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMyI+PHByb2Q+PGNQcm9kPjkwMDg8L2NQcm9kPjx4UHJvZD5GcmVlIFJlZmlsbDwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT4xMC45MDA8L3ZVbkNvbT48dlByb2Q+MTAuOTA8L3ZQcm9kPjxpbmRSZWdyYT5BPC9pbmRSZWdyYT48dkl0ZW0+MTAuOTA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MjEwNjkwMTA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5DRVNUIj48eFRleHRvRGV0PjMwMDMwMDwveFRleHRvRGV0Pjwvb2JzRmlzY29EZXQ+PC9wcm9kPjxpbXBvc3RvPjxJQ01TPjxJQ01TMDA+PE9yaWc+MDwvT3JpZz48Q1NUPjAwPC9DU1Q+PHBJQ01TPjAuMDA8L3BJQ01TPjx2SUNNUz4wLjAwPC92SUNNUz48L0lDTVMwMD48L0lDTVM+PFBJUz48UElTQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNy45ODwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwQ09GSU5TPjcuNjAwMDwvcENPRklOUz48dkNPRklOUz44Mi44NDwvdkNPRklOUz48L0NPRklOU0FsaXE+PC9DT0ZJTlM+PC9pbXBvc3RvPjwvZGV0Pjx0b3RhbD48SUNNU1RvdD48dklDTVM+MC4zOTwvdklDTVM+PHZQcm9kPjIyLjkwPC92UHJvZD48dkRlc2M+MC4wMDwvdkRlc2M+PHZQSVM+MzcuNzg8L3ZQSVM+PHZDT0ZJTlM+MTc0LjA0PC92Q09GSU5TPjx2UElTU1Q+MC4wMDwvdlBJU1NUPjx2Q09GSU5TU1Q+MC4wMDwvdkNPRklOU1NUPjx2T3V0cm8+MC4wMDwvdk91dHJvPjwvSUNNU1RvdD48dkNGZT4yMi45MDwvdkNGZT48L3RvdGFsPjxwZ3RvPjxNUD48Y01QPjAxPC9jTVA+PHZNUD4yMi45MDwvdk1QPjwvTVA+PHZUcm9jbz4wLjAwPC92VHJvY28+PC9wZ3RvPjwvaW5mQ0ZlPjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L0Nhbm9uaWNhbGl6YXRpb25NZXRob2Q+PFNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvU2lnbmF0dXJlTWV0aG9kPjxSZWZlcmVuY2UgVVJJPSIjQ0ZlMzUxNjEwNjEwOTkwMDgwMDAxNDE1OTkwMDAwNDIwNzAwMDQ2OTY1NjU5MjEiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9UcmFuc2Zvcm0+PFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L1RyYW5zZm9ybT48L1RyYW5zZm9ybXM+PERpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiI+PC9EaWdlc3RNZXRob2Q+PERpZ2VzdFZhbHVlPmRHUEY3OWsrY3ZHcDZFUGdLYzBUdCs5TmhpVklZK0NRV0R2eUF0RG00Rjg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPldiM0h0VEdUSzBHaVhGaW8rVmkxeDNIMWd4MjloTDkvQS9ZYzdXc2Q2Tlc0Wjd0eVBFL2owQ0I4VHZaUGwvYjV1TnVvcS9OaGprWVJPTHBjRVA0K0o3RWtvRXpHS3NXMDNzQ2cvNC9TSGpKdUphQXhuZXBZa01qaXJaTm84VGRyV2xPeFU4OFdPNnFzRlZEMVJLZTlSek9EN3BhbWE2dTZnZVdPSzJsNW82TERpV2NFd2hleG1qZjVuYmUzV2tRRzBLZ0tPUnZWSWhoSU9rekliOHlRcnpLdjhsNTVGWVRmeDFPSER6Z0ROeVM2akZ1c1dsVzJKaDYyMmw0MTdua3NvY2ZBMHgzNy9RN3NFN2NweU4yT2JFS1dkeUc2a01kRGxQK21lUzVBTmhsR2g1emY0WTNMNG9XY1ptd3J6MENTSnFicEM4a25teWFxeVVwbjRYeC85Zz09PC9TaWduYXR1cmVWYWx1ZT48S2V5SW5mbz48WDUwOURhdGE+PFg1MDlDZXJ0aWZpY2F0ZT5NSUlHeVRDQ0JMR2dBd0lCQWdJSkFSemtEaGhIdWZSeU1BMEdDU3FHU0liM0RRRUJDd1VBTUdjeEN6QUpCZ05WQkFZVEFrSlNNVFV3TXdZRFZRUUtFeXhUWldOeVpYUmhjbWxoSUdSaElFWmhlbVZ1WkdFZ1pHOGdSWE4wWVdSdklHUmxJRk5oYnlCUVlYVnNiekVoTUI4R0ExVUVBeE1ZUVVNZ1UwRlVJR1JsSUZSbGMzUmxJRk5GUmtGYUlGTlFNQjRYRFRFMU1EY3dPREl3TURJMU1Wb1hEVEl3TURjd09ESXdNREkxTVZvd2djNHhFakFRQmdOVkJBVVRDVGt3TURBd05ESXdOekVMTUFrR0ExVUVCaE1DUWxJeEVqQVFCZ05WQkFnVENWTmhieUJRWVhWc2J6RVJNQThHQTFVRUNoTUlVMFZHUVZvdFUxQXhEekFOQmdOVkJBc1RCa0ZETFZOQlZERW9NQ1lHQTFVRUN4TWZRWFYwWlc1MGFXTmhaRzhnY0c5eUlFRlNJRk5GUmtGYUlGTlFJRk5CVkRGSk1FY0dBMVVFQXhOQVJFbE5RVk1nUkVVZ1RVVk1UeUJRU1UxRlRsUkJJRk5KVTFSRlRVRlRJRVJGSUZCUFRsUlBJRVVnUVVORlUxTlBJRG8yTVRBNU9UQXdPREF3TURFME1UQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUl1WWZKWW4vUHYzOGNkYjEwRjk5Nll3M2ZDSU9takVyOUV4dlB4SWRFWTFsRmNwYk9mRE5PbmZZYm9MMFRzLy9Qc3J4ZUlQSmxEc3ZRMEw0NndwSEROcEZmQkRxeHlkQlY0ckYrVklPbHZ1NmJ2eitEUEd2OW5xVFRHMldNeVhLdjVhdWVMY1l5VmR1MjRucE03V2VkZDBOaDZBQkk4Ni9qanZaR1JRS3RLYnN0KzRaV3RWSUZ0UU9pVHRIaFdOcEwxdW4rWit2MEg4dGpPRkhuK3N6MTlMcFZoV1NIbkJIVHBlRUFxdXdndmthV3czZXNQanlMUFplL2Z2cmlDbDdTWk9NRFBZcFdOenlXOThyLzVOYm1PNXRzQmd4RmZHcENiYnNqTlBOeG5VbWZ3VldYMzBLMVpMWUZnc2d6Z0J5Znl4UjJsY0FmZ2VnNkZkNWl1MEUyRUNBd0VBQWFPQ0FnNHdnZ0lLTUE0R0ExVWREd0VCL3dRRUF3SUY0REI3QmdOVkhTQUVkREJ5TUhBR0NTc0dBUVFCZ2V3dEF6QmpNR0VHQ0NzR0FRVUZCd0lCRmxWb2RIUndPaTh2WVdOellYUXVhVzF3Y21WdWMyRnZabWxqYVdGc0xtTnZiUzVpY2k5eVpYQnZjMmwwYjNKcGJ5OWtjR012WVdOellYUnpaV1poZW5Od0wyUndZMTloWTNOaGRITmxabUY2YzNBdWNHUm1NR3NHQTFVZEh3UmtNR0l3WUtCZW9GeUdXbWgwZEhBNkx5OWhZM05oZEMxMFpYTjBaUzVwYlhCeVpXNXpZVzltYVdOcFlXd3VZMjl0TG1KeUwzSmxjRzl6YVhSdmNtbHZMMnhqY2k5aFkzTmhkSE5sWm1GNmMzQXZZV056WVhSelpXWmhlbk53WTNKc0xtTnliRENCcGdZSUt3WUJCUVVIQVFFRWdaa3dnWll3TkFZSUt3WUJCUVVITUFHR0tHaDBkSEE2THk5dlkzTndMWEJwYkc5MExtbHRjSEpsYm5OaGIyWnBZMmxoYkM1amIyMHVZbkl3WGdZSUt3WUJCUVVITUFLR1VtaDBkSEE2THk5aFkzTmhkQzEwWlhOMFpTNXBiWEJ5Wlc1ellXOW1hV05wWVd3dVkyOXRMbUp5TDNKbGNHOXphWFJ2Y21sdkwyTmxjblJwWm1sallXUnZjeTloWTNOaGRDMTBaWE4wWlM1d04yTXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0l3Q1FZRFZSMFRCQUl3QURBa0JnTlZIUkVFSFRBYm9Ca0dCV0JNQVFNRG9CQUVEall4TURrNU1EQTRNREF3TVRReE1COEdBMVVkSXdRWU1CYUFGSTQ1UVFCYzhyZ0YycWh0bUxrQlJtMXVZOThDTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFDVlo5Ulc5bW9qMHIrNjF3K3V2MjN2aG9KL0VtVW55bnV6OXFSWU9EeGRQaVlGcSs5REZOYTF3ZXBselNmejJuUEZsSWUyNTNWZ2pVdlVObU9VdmQrV0gwR1ZEVE00dWlCV1kvcmxoZHFSZS9tQTZSUXlUbGNGZXFhLzNLdzhGNlpzYTFadHYyR0RuT0pTS3ZFV3ErVzhrR0l5YzF3SE1LSzREZUFqbllHYStDSVp2UjcwMi9JQVJjSkpleDNQQ25lbFBHejNUcklSZU1kNFNTRWNiOTV6SGFZeFZvY0E4Q3BiWlNIa3hjNXdKSUNWU29SRHZ3ZTl6endmZk9RTnJ3V280T3c2Z2RBNXdNcENIR3ZRN08zVWdnWklyclUzSkpYWUpPYVNuV1lPalp3azNmTlk3T2E5RXBFYnpPanA1Q1NEcXdqTndUcXpqM3VwTDl1alhEeksyVklwaEVjcXE4ZnBGN2IyYzJXREtZbGs3dldZbmp3SGZrVCtOM05KUHYwL3B1cGZzNVB4ajl0bFFqbW8zSXBkRDVteWQwNzRodFZrKyt0Si9UM2dxNFVBblBDczE3Vk4rNXlYcTU0YWs5S0ZmSGxWZmZKS055Y2RVcUtCSHZxNVQzZ1lGdFUyNElnTUQ5cElvNmtESGQyL2srQVlLR20vclN0c0RXYmY2eHdrVGtDNHJSVFpVOWJBNE1oQVVjajNFMnN4cUhuakYyeTd5alB3QllpU1FrT2RkWmJJaENrakJHSDhTVzREQk45ay9XYzAyOC9yNFptZm9Bc2MraE1IWXpuWEg5eEd3WkRDNklpdHFiWS9hRWJ2WmxKYlJvcHRaK05qSmFLaHFhcTZUa2I4MENxOHdzRjZlNVN6cW5kVDl4cUlENTI5SXppVlVzbU9ac2xyR3c9PTwvWDUwOUNlcnRpZmljYXRlPjwvWDUwOURhdGE+PC9LZXlJbmZvPjwvU2lnbmF0dXJlPjwvQ0ZlPgogICAgICAgIA==
    ...            discount-amount=${0.0}
    ...            price-basis=G
    ...            tip=${0.0}
    ...            creation-dttm=${creation-dttm}
    ...            total-tender=${47.8}
    ...            discount-applied=${None}
    ...            order-discount-amount=${0.0}
    ...            state-id=${5}
    ...            order-code=${order}
    ...            store-code=${store_code}
    ...            total-gross=${47.8}
    ...            total-after-discount=${45.38}
    ...            sale-type-id=${0}
    ...            tax-total=${2.42}
    ...            due-amount=${0.0}
    ...            change=${0.0}
    ...            originator-code=POS0001
    ...            price-list=EI
    ...            pos-code=${pos-code}
    ...            business-dt=2020-09-28
    ...            order-picture-custom-order-properties=${order-picture-custom-order-properties}
    ...            order-picture-tenders=${order-picture-tenders}
    ...            total-amount=${45.38}
    ...            pos-user-id=5813
    ...            total-gift=${0.0}
    ...            pod-type=FC

    [Return]    ${payload}

getPicturebodySemSaleLines
    [Arguments]    ${customer_id}    ${order}    ${store_code}    ${pos-code}    ${creation-dttm}

    ${comment}=        Create List                         
    &{order-lines}=    Create Dictionary                   
    ...                comment=${comment}
    ...                unit-price=${0.0}
    ...                plu=1051
    ...                sub-unit-price=${0.0}
    ...                multiplied-qty=${1}
    ...                level=${0}
    ...                menu-item-code=1
    ...                item-type=COMBO
    ...                product=CM Whopper/Q
    ...                qty=${1}
    ...                inc-qty=${0}
    ...                order-picture-id=${129}
    ...                item-price=${0.0}
    ...                dec-qty=${0}
    ...                category-description=${None}
    ...                part-code=${None}
    ...                sub-category-description=${None}
    ...                line-number=${1}
    ...                price-key=${None}
    ...                added-unit-price=${0.0}

    ${order-picture-sale-lines}    Create List    ${order-lines}

    &{tenders1}    Create Dictionary
    ...            reference-amount=${47.8}
    ...            tender-detail={\"ReceiptMerchant\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"AuthCode\": \"1302\", \"IdAuth\": \"08496898000142\", \"ReceiptCustomer\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"TransactionProcessor\": \"00051\", \"CNPJAuth\": \"68536379000192\", \"Bandeira\": \"4\"}
    ...            tender-amount=${47.8}
    ...            order-picture-id=${129}
    ...            change-amount=${0.0}
    ...            tender-type=2

    ${order-picture-tenders}=    Create List    ${tenders1}
    # ${contactinfo}=    Create List    ${entry 1}    ${entry 2}

    &{order-picture-custom-order-properties}    Create Dictionary
    ...                                         SATISFACTION_SURVEY_VALIDATOR=11901112
    ...                                         FISCALIZATION_DATE=2020-09-28T14:29:48
    ...                                         LOYALTY_SYSTEM_USER_ID=${customer_id}
    ...                                         WHOPPER_WIFI_CODE=DBHOEEVX
    ...                                         SATISFACTION_SURVEY_CODE=0190991920892109

    &{payload}=    Create Dictionary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    ...            tax-applied=${2.42}
    ...            type-id=${0}
    ...            session=pos=1user=5813count=1period=20200928
    ...            xml-data=CiAgICAgICAgPENGZT48aW5mQ0ZlIElkPSJDRmUzNTE2MTA2MTA5OTAwODAwMDE0MTU5OTAwMDA0MjA3MDAwNDY5NjU2NTkyMSIgdmVyc2FvPSIwLjA2IiB2ZXJzYW9EYWRvc0VudD0iMC4wNiIgdmVyc2FvU0I9IjAxMDEwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj42NTY1OTI8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD45MDAwMDQyMDc8L25zZXJpZVNBVD48bkNGZT4xMjk8L25DRmU+PGRFbWk+MjAyMDA5Mjg8L2RFbWk+PGhFbWk+MTEyOTQ4PC9oRW1pPjxjRFY+MTwvY0RWPjx0cEFtYj4yPC90cEFtYj48Q05QSj4xNjcxNjExNDAwMDE3MjwvQ05QSj48c2lnbkFDPlNHUi1TQVQgU0lTVEVNQSBERSBHRVNUQU8gRSBSRVRBR1VBUkRBIERPIFNBVDwvc2lnbkFDPjxhc3NpbmF0dXJhUVJDT0RFPlE0c1VYWCtNdTh0YytocnlBYU9WRXRvU1FVTzNrL25hb3lwZjVsYlpmV2hGZVFQQzhzalNXNWNaU3ZLWkgxd29VMHFrMmYycmVZTEVhWWpGK1FhOU11cnFqbHhaYmxlWFowNExoWDhYQjNXeGxVUDBvdUwyTU5OeUlNaXJDc3VLZWR2RVE4N3c4THpWeDRzK0NvNmIyYUxDYzNzbG5yWURiRWt6NDVUS0pvNnF3cUlvdytYN05nZ2syNmFxbkFQaTFDR3VXMmlzbThVdHlmM2ZOOGdQeUk1TjJQYW02Mmh1N2d1Y01jek9LQXY1V2F3ZCtGOHBFM2xPT2RueFdidXp4cGxFUytIcVFqcmN6cVRydlJVbU5xT1Zqc2N2WEtQQ0JrbmdZeUVHeG11eFA5cFVwV0FHdjg5M1MydWNtSDVjT3gyVFRMY3pLMC9vS1JscmdUZ3NHZz09PC9hc3NpbmF0dXJhUVJDT0RFPjxudW1lcm9DYWl4YT4wMDE8L251bWVyb0NhaXhhPjwvaWRlPjxlbWl0PjxDTlBKPjYxMDk5MDA4MDAwMTQxPC9DTlBKPjx4Tm9tZT5ESU1BUyBERSBNRUxPIFBJTUVOVEEgU0lTVEVNQVMgREUgUE9OVE8gRSBBQ0VTU08gTFREQTwveE5vbWU+PHhGYW50PkRJTUVQPC94RmFudD48ZW5kZXJFbWl0Pjx4TGdyPkFWRU5JREEgTU9GQVJSRUo8L3hMZ3I+PG5ybz44NDA8L25ybz48eENwbD45MDg8L3hDcGw+PHhCYWlycm8+VkwuIExFT1BPTERJTkE8L3hCYWlycm8+PHhNdW4+U0FPIFBBVUxPPC94TXVuPjxDRVA+MDUzMTEwMDA8L0NFUD48L2VuZGVyRW1pdD48SUU+MTExMTExMTExMTExPC9JRT48Y1JlZ1RyaWI+MzwvY1JlZ1RyaWI+PGNSZWdUcmliSVNTUU4+MTwvY1JlZ1RyaWJJU1NRTj48aW5kUmF0SVNTUU4+TjwvaW5kUmF0SVNTUU4+PC9lbWl0PjxkZXN0PjwvZGVzdD48ZGV0IG5JdGVtPSIxIj48cHJvZD48Y1Byb2Q+MTA1MDwvY1Byb2Q+PHhQcm9kPldob3BwZXIvUTwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT45LjAwMDwvdlVuQ29tPjx2UHJvZD45LjAwPC92UHJvZD48aW5kUmVncmE+QTwvaW5kUmVncmE+PHZJdGVtPjkuMDA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MTkwMjIwMDA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjwvcHJvZD48aW1wb3N0bz48SUNNUz48SUNNUzAwPjxPcmlnPjA8L09yaWc+PENTVD4wMDwvQ1NUPjxwSUNNUz4zLjIwPC9wSUNNUz48dklDTVM+MC4yOTwvdklDTVM+PC9JQ01TMDA+PC9JQ01TPjxQSVM+PFBJU0FsaXE+PENTVD4wMTwvQ1NUPjx2QkM+OS4wMDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNC44NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz45LjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjY4LjQwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMiI+PHByb2Q+PGNQcm9kPjYwMTI8L2NQcm9kPjx4UHJvZD5NRCBCYXRhdGE8L3hQcm9kPjxDRk9QPjUwMDA8L0NGT1A+PHVDb20+dW48L3VDb20+PHFDb20+MS4wMDAwPC9xQ29tPjx2VW5Db20+My4wMDA8L3ZVbkNvbT48dlByb2Q+My4wMDwvdlByb2Q+PGluZFJlZ3JhPkE8L2luZFJlZ3JhPjx2SXRlbT4zLjAwPC92SXRlbT48b2JzRmlzY29EZXQgeENhbXBvRGV0PSJDb2QuTkNNIj48eFRleHRvRGV0PjIwMDUyMDAwPC94VGV4dG9EZXQ+PC9vYnNGaXNjb0RldD48L3Byb2Q+PGltcG9zdG8+PElDTVM+PElDTVMwMD48T3JpZz4wPC9PcmlnPjxDU1Q+MDA8L0NTVD48cElDTVM+My4yMDwvcElDTVM+PHZJQ01TPjAuMTA8L3ZJQ01TPjwvSUNNUzAwPjwvSUNNUz48UElTPjxQSVNBbGlxPjxDU1Q+MDE8L0NTVD48dkJDPjMuMDA8L3ZCQz48cFBJUz4xLjY1MDA8L3BQSVM+PHZQSVM+NC45NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4zLjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjIyLjgwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMyI+PHByb2Q+PGNQcm9kPjkwMDg8L2NQcm9kPjx4UHJvZD5GcmVlIFJlZmlsbDwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT4xMC45MDA8L3ZVbkNvbT48dlByb2Q+MTAuOTA8L3ZQcm9kPjxpbmRSZWdyYT5BPC9pbmRSZWdyYT48dkl0ZW0+MTAuOTA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MjEwNjkwMTA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5DRVNUIj48eFRleHRvRGV0PjMwMDMwMDwveFRleHRvRGV0Pjwvb2JzRmlzY29EZXQ+PC9wcm9kPjxpbXBvc3RvPjxJQ01TPjxJQ01TMDA+PE9yaWc+MDwvT3JpZz48Q1NUPjAwPC9DU1Q+PHBJQ01TPjAuMDA8L3BJQ01TPjx2SUNNUz4wLjAwPC92SUNNUz48L0lDTVMwMD48L0lDTVM+PFBJUz48UElTQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNy45ODwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwQ09GSU5TPjcuNjAwMDwvcENPRklOUz48dkNPRklOUz44Mi44NDwvdkNPRklOUz48L0NPRklOU0FsaXE+PC9DT0ZJTlM+PC9pbXBvc3RvPjwvZGV0Pjx0b3RhbD48SUNNU1RvdD48dklDTVM+MC4zOTwvdklDTVM+PHZQcm9kPjIyLjkwPC92UHJvZD48dkRlc2M+MC4wMDwvdkRlc2M+PHZQSVM+MzcuNzg8L3ZQSVM+PHZDT0ZJTlM+MTc0LjA0PC92Q09GSU5TPjx2UElTU1Q+MC4wMDwvdlBJU1NUPjx2Q09GSU5TU1Q+MC4wMDwvdkNPRklOU1NUPjx2T3V0cm8+MC4wMDwvdk91dHJvPjwvSUNNU1RvdD48dkNGZT4yMi45MDwvdkNGZT48L3RvdGFsPjxwZ3RvPjxNUD48Y01QPjAxPC9jTVA+PHZNUD4yMi45MDwvdk1QPjwvTVA+PHZUcm9jbz4wLjAwPC92VHJvY28+PC9wZ3RvPjwvaW5mQ0ZlPjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L0Nhbm9uaWNhbGl6YXRpb25NZXRob2Q+PFNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvU2lnbmF0dXJlTWV0aG9kPjxSZWZlcmVuY2UgVVJJPSIjQ0ZlMzUxNjEwNjEwOTkwMDgwMDAxNDE1OTkwMDAwNDIwNzAwMDQ2OTY1NjU5MjEiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9UcmFuc2Zvcm0+PFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L1RyYW5zZm9ybT48L1RyYW5zZm9ybXM+PERpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiI+PC9EaWdlc3RNZXRob2Q+PERpZ2VzdFZhbHVlPmRHUEY3OWsrY3ZHcDZFUGdLYzBUdCs5TmhpVklZK0NRV0R2eUF0RG00Rjg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPldiM0h0VEdUSzBHaVhGaW8rVmkxeDNIMWd4MjloTDkvQS9ZYzdXc2Q2Tlc0Wjd0eVBFL2owQ0I4VHZaUGwvYjV1TnVvcS9OaGprWVJPTHBjRVA0K0o3RWtvRXpHS3NXMDNzQ2cvNC9TSGpKdUphQXhuZXBZa01qaXJaTm84VGRyV2xPeFU4OFdPNnFzRlZEMVJLZTlSek9EN3BhbWE2dTZnZVdPSzJsNW82TERpV2NFd2hleG1qZjVuYmUzV2tRRzBLZ0tPUnZWSWhoSU9rekliOHlRcnpLdjhsNTVGWVRmeDFPSER6Z0ROeVM2akZ1c1dsVzJKaDYyMmw0MTdua3NvY2ZBMHgzNy9RN3NFN2NweU4yT2JFS1dkeUc2a01kRGxQK21lUzVBTmhsR2g1emY0WTNMNG9XY1ptd3J6MENTSnFicEM4a25teWFxeVVwbjRYeC85Zz09PC9TaWduYXR1cmVWYWx1ZT48S2V5SW5mbz48WDUwOURhdGE+PFg1MDlDZXJ0aWZpY2F0ZT5NSUlHeVRDQ0JMR2dBd0lCQWdJSkFSemtEaGhIdWZSeU1BMEdDU3FHU0liM0RRRUJDd1VBTUdjeEN6QUpCZ05WQkFZVEFrSlNNVFV3TXdZRFZRUUtFeXhUWldOeVpYUmhjbWxoSUdSaElFWmhlbVZ1WkdFZ1pHOGdSWE4wWVdSdklHUmxJRk5oYnlCUVlYVnNiekVoTUI4R0ExVUVBeE1ZUVVNZ1UwRlVJR1JsSUZSbGMzUmxJRk5GUmtGYUlGTlFNQjRYRFRFMU1EY3dPREl3TURJMU1Wb1hEVEl3TURjd09ESXdNREkxTVZvd2djNHhFakFRQmdOVkJBVVRDVGt3TURBd05ESXdOekVMTUFrR0ExVUVCaE1DUWxJeEVqQVFCZ05WQkFnVENWTmhieUJRWVhWc2J6RVJNQThHQTFVRUNoTUlVMFZHUVZvdFUxQXhEekFOQmdOVkJBc1RCa0ZETFZOQlZERW9NQ1lHQTFVRUN4TWZRWFYwWlc1MGFXTmhaRzhnY0c5eUlFRlNJRk5GUmtGYUlGTlFJRk5CVkRGSk1FY0dBMVVFQXhOQVJFbE5RVk1nUkVVZ1RVVk1UeUJRU1UxRlRsUkJJRk5KVTFSRlRVRlRJRVJGSUZCUFRsUlBJRVVnUVVORlUxTlBJRG8yTVRBNU9UQXdPREF3TURFME1UQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUl1WWZKWW4vUHYzOGNkYjEwRjk5Nll3M2ZDSU9takVyOUV4dlB4SWRFWTFsRmNwYk9mRE5PbmZZYm9MMFRzLy9Qc3J4ZUlQSmxEc3ZRMEw0NndwSEROcEZmQkRxeHlkQlY0ckYrVklPbHZ1NmJ2eitEUEd2OW5xVFRHMldNeVhLdjVhdWVMY1l5VmR1MjRucE03V2VkZDBOaDZBQkk4Ni9qanZaR1JRS3RLYnN0KzRaV3RWSUZ0UU9pVHRIaFdOcEwxdW4rWit2MEg4dGpPRkhuK3N6MTlMcFZoV1NIbkJIVHBlRUFxdXdndmthV3czZXNQanlMUFplL2Z2cmlDbDdTWk9NRFBZcFdOenlXOThyLzVOYm1PNXRzQmd4RmZHcENiYnNqTlBOeG5VbWZ3VldYMzBLMVpMWUZnc2d6Z0J5Znl4UjJsY0FmZ2VnNkZkNWl1MEUyRUNBd0VBQWFPQ0FnNHdnZ0lLTUE0R0ExVWREd0VCL3dRRUF3SUY0REI3QmdOVkhTQUVkREJ5TUhBR0NTc0dBUVFCZ2V3dEF6QmpNR0VHQ0NzR0FRVUZCd0lCRmxWb2RIUndPaTh2WVdOellYUXVhVzF3Y21WdWMyRnZabWxqYVdGc0xtTnZiUzVpY2k5eVpYQnZjMmwwYjNKcGJ5OWtjR012WVdOellYUnpaV1poZW5Od0wyUndZMTloWTNOaGRITmxabUY2YzNBdWNHUm1NR3NHQTFVZEh3UmtNR0l3WUtCZW9GeUdXbWgwZEhBNkx5OWhZM05oZEMxMFpYTjBaUzVwYlhCeVpXNXpZVzltYVdOcFlXd3VZMjl0TG1KeUwzSmxjRzl6YVhSdmNtbHZMMnhqY2k5aFkzTmhkSE5sWm1GNmMzQXZZV056WVhSelpXWmhlbk53WTNKc0xtTnliRENCcGdZSUt3WUJCUVVIQVFFRWdaa3dnWll3TkFZSUt3WUJCUVVITUFHR0tHaDBkSEE2THk5dlkzTndMWEJwYkc5MExtbHRjSEpsYm5OaGIyWnBZMmxoYkM1amIyMHVZbkl3WGdZSUt3WUJCUVVITUFLR1VtaDBkSEE2THk5aFkzTmhkQzEwWlhOMFpTNXBiWEJ5Wlc1ellXOW1hV05wWVd3dVkyOXRMbUp5TDNKbGNHOXphWFJ2Y21sdkwyTmxjblJwWm1sallXUnZjeTloWTNOaGRDMTBaWE4wWlM1d04yTXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0l3Q1FZRFZSMFRCQUl3QURBa0JnTlZIUkVFSFRBYm9Ca0dCV0JNQVFNRG9CQUVEall4TURrNU1EQTRNREF3TVRReE1COEdBMVVkSXdRWU1CYUFGSTQ1UVFCYzhyZ0YycWh0bUxrQlJtMXVZOThDTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFDVlo5Ulc5bW9qMHIrNjF3K3V2MjN2aG9KL0VtVW55bnV6OXFSWU9EeGRQaVlGcSs5REZOYTF3ZXBselNmejJuUEZsSWUyNTNWZ2pVdlVObU9VdmQrV0gwR1ZEVE00dWlCV1kvcmxoZHFSZS9tQTZSUXlUbGNGZXFhLzNLdzhGNlpzYTFadHYyR0RuT0pTS3ZFV3ErVzhrR0l5YzF3SE1LSzREZUFqbllHYStDSVp2UjcwMi9JQVJjSkpleDNQQ25lbFBHejNUcklSZU1kNFNTRWNiOTV6SGFZeFZvY0E4Q3BiWlNIa3hjNXdKSUNWU29SRHZ3ZTl6endmZk9RTnJ3V280T3c2Z2RBNXdNcENIR3ZRN08zVWdnWklyclUzSkpYWUpPYVNuV1lPalp3azNmTlk3T2E5RXBFYnpPanA1Q1NEcXdqTndUcXpqM3VwTDl1alhEeksyVklwaEVjcXE4ZnBGN2IyYzJXREtZbGs3dldZbmp3SGZrVCtOM05KUHYwL3B1cGZzNVB4ajl0bFFqbW8zSXBkRDVteWQwNzRodFZrKyt0Si9UM2dxNFVBblBDczE3Vk4rNXlYcTU0YWs5S0ZmSGxWZmZKS055Y2RVcUtCSHZxNVQzZ1lGdFUyNElnTUQ5cElvNmtESGQyL2srQVlLR20vclN0c0RXYmY2eHdrVGtDNHJSVFpVOWJBNE1oQVVjajNFMnN4cUhuakYyeTd5alB3QllpU1FrT2RkWmJJaENrakJHSDhTVzREQk45ay9XYzAyOC9yNFptZm9Bc2MraE1IWXpuWEg5eEd3WkRDNklpdHFiWS9hRWJ2WmxKYlJvcHRaK05qSmFLaHFhcTZUa2I4MENxOHdzRjZlNVN6cW5kVDl4cUlENTI5SXppVlVzbU9ac2xyR3c9PTwvWDUwOUNlcnRpZmljYXRlPjwvWDUwOURhdGE+PC9LZXlJbmZvPjwvU2lnbmF0dXJlPjwvQ0ZlPgogICAgICAgIA==
    ...            discount-amount=${0.0}
    ...            price-basis=G
    ...            tip=${0.0}
    ...            creation-dttm=${creation-dttm}
    ...            total-tender=${47.8}
    ...            discount-applied=${None}
    ...            order-discount-amount=${0.0}
    ...            state-id=${5}
    ...            order-code=${order}
    ...            store-code=${store_code}
    ...            total-gross=${47.8}
    ...            total-after-discount=${45.38}
    ...            sale-type-id=${0}
    ...            tax-total=${2.42}
    ...            due-amount=${0.0}
    ...            change=${0.0}
    ...            originator-code=POS0001
    ...            price-list=EI
    ...            pos-code=${pos-code}
    ...            business-dt=2020-09-28
    ...            order-picture-custom-order-properties=${order-picture-custom-order-properties}
    ...            order-picture-tenders=${order-picture-tenders}
    ...            total-amount=${45.38}
    ...            pos-user-id=5813
    ...            total-gift=${0.0}
    ...            pod-type=FC

    [Return]    ${payload}

getPicturebodySemCreationdttm
    [Arguments]    ${customer_id}    ${order}    ${store_code}    ${pos-code}    ${creation-dttm}

    ${comment}=        Create List                         
    &{order-lines}=    Create Dictionary                   
    ...                comment=${comment}
    ...                unit-price=${0.0}
    ...                plu=1051
    ...                sub-unit-price=${0.0}
    ...                multiplied-qty=${1}
    ...                level=${0}
    ...                menu-item-code=1
    ...                item-type=COMBO
    ...                product=CM Whopper/Q
    ...                qty=${1}
    ...                inc-qty=${0}
    ...                order-picture-id=${129}
    ...                item-price=${0.0}
    ...                dec-qty=${0}
    ...                category-description=${None}
    ...                part-code=${None}
    ...                sub-category-description=${None}
    ...                line-number=${1}
    ...                price-key=${None}
    ...                added-unit-price=${0.0}

    ${order-picture-sale-lines}    Create List    ${order-lines}

    &{tenders1}    Create Dictionary
    ...            reference-amount=${47.8}
    ...            tender-detail={\"ReceiptMerchant\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"AuthCode\": \"1302\", \"IdAuth\": \"08496898000142\", \"ReceiptCustomer\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"TransactionProcessor\": \"00051\", \"CNPJAuth\": \"68536379000192\", \"Bandeira\": \"4\"}
    ...            tender-amount=${47.8}
    ...            order-picture-id=${129}
    ...            change-amount=${0.0}
    ...            tender-type=2

    ${order-picture-tenders}=    Create List    ${tenders1}
    # ${contactinfo}=    Create List    ${entry 1}    ${entry 2}

    &{order-picture-custom-order-properties}    Create Dictionary
    ...                                         SATISFACTION_SURVEY_VALIDATOR=11901112
    ...                                         FISCALIZATION_DATE=2020-09-28T14:29:48
    ...                                         LOYALTY_SYSTEM_USER_ID=${customer_id}
    ...                                         WHOPPER_WIFI_CODE=DBHOEEVX
    ...                                         SATISFACTION_SURVEY_CODE=0190991920892109

    &{payload}=    Create Dictionary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    ...            order-picture-sale-lines=${order-picture-sale-lines}
    ...            tax-applied=${2.42}
    ...            type-id=${0}
    ...            session=pos=1user=5813count=1period=20200928
    ...            xml-data=CiAgICAgICAgPENGZT48aW5mQ0ZlIElkPSJDRmUzNTE2MTA2MTA5OTAwODAwMDE0MTU5OTAwMDA0MjA3MDAwNDY5NjU2NTkyMSIgdmVyc2FvPSIwLjA2IiB2ZXJzYW9EYWRvc0VudD0iMC4wNiIgdmVyc2FvU0I9IjAxMDEwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj42NTY1OTI8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD45MDAwMDQyMDc8L25zZXJpZVNBVD48bkNGZT4xMjk8L25DRmU+PGRFbWk+MjAyMDA5Mjg8L2RFbWk+PGhFbWk+MTEyOTQ4PC9oRW1pPjxjRFY+MTwvY0RWPjx0cEFtYj4yPC90cEFtYj48Q05QSj4xNjcxNjExNDAwMDE3MjwvQ05QSj48c2lnbkFDPlNHUi1TQVQgU0lTVEVNQSBERSBHRVNUQU8gRSBSRVRBR1VBUkRBIERPIFNBVDwvc2lnbkFDPjxhc3NpbmF0dXJhUVJDT0RFPlE0c1VYWCtNdTh0YytocnlBYU9WRXRvU1FVTzNrL25hb3lwZjVsYlpmV2hGZVFQQzhzalNXNWNaU3ZLWkgxd29VMHFrMmYycmVZTEVhWWpGK1FhOU11cnFqbHhaYmxlWFowNExoWDhYQjNXeGxVUDBvdUwyTU5OeUlNaXJDc3VLZWR2RVE4N3c4THpWeDRzK0NvNmIyYUxDYzNzbG5yWURiRWt6NDVUS0pvNnF3cUlvdytYN05nZ2syNmFxbkFQaTFDR3VXMmlzbThVdHlmM2ZOOGdQeUk1TjJQYW02Mmh1N2d1Y01jek9LQXY1V2F3ZCtGOHBFM2xPT2RueFdidXp4cGxFUytIcVFqcmN6cVRydlJVbU5xT1Zqc2N2WEtQQ0JrbmdZeUVHeG11eFA5cFVwV0FHdjg5M1MydWNtSDVjT3gyVFRMY3pLMC9vS1JscmdUZ3NHZz09PC9hc3NpbmF0dXJhUVJDT0RFPjxudW1lcm9DYWl4YT4wMDE8L251bWVyb0NhaXhhPjwvaWRlPjxlbWl0PjxDTlBKPjYxMDk5MDA4MDAwMTQxPC9DTlBKPjx4Tm9tZT5ESU1BUyBERSBNRUxPIFBJTUVOVEEgU0lTVEVNQVMgREUgUE9OVE8gRSBBQ0VTU08gTFREQTwveE5vbWU+PHhGYW50PkRJTUVQPC94RmFudD48ZW5kZXJFbWl0Pjx4TGdyPkFWRU5JREEgTU9GQVJSRUo8L3hMZ3I+PG5ybz44NDA8L25ybz48eENwbD45MDg8L3hDcGw+PHhCYWlycm8+VkwuIExFT1BPTERJTkE8L3hCYWlycm8+PHhNdW4+U0FPIFBBVUxPPC94TXVuPjxDRVA+MDUzMTEwMDA8L0NFUD48L2VuZGVyRW1pdD48SUU+MTExMTExMTExMTExPC9JRT48Y1JlZ1RyaWI+MzwvY1JlZ1RyaWI+PGNSZWdUcmliSVNTUU4+MTwvY1JlZ1RyaWJJU1NRTj48aW5kUmF0SVNTUU4+TjwvaW5kUmF0SVNTUU4+PC9lbWl0PjxkZXN0PjwvZGVzdD48ZGV0IG5JdGVtPSIxIj48cHJvZD48Y1Byb2Q+MTA1MDwvY1Byb2Q+PHhQcm9kPldob3BwZXIvUTwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT45LjAwMDwvdlVuQ29tPjx2UHJvZD45LjAwPC92UHJvZD48aW5kUmVncmE+QTwvaW5kUmVncmE+PHZJdGVtPjkuMDA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MTkwMjIwMDA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjwvcHJvZD48aW1wb3N0bz48SUNNUz48SUNNUzAwPjxPcmlnPjA8L09yaWc+PENTVD4wMDwvQ1NUPjxwSUNNUz4zLjIwPC9wSUNNUz48dklDTVM+MC4yOTwvdklDTVM+PC9JQ01TMDA+PC9JQ01TPjxQSVM+PFBJU0FsaXE+PENTVD4wMTwvQ1NUPjx2QkM+OS4wMDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNC44NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz45LjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjY4LjQwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMiI+PHByb2Q+PGNQcm9kPjYwMTI8L2NQcm9kPjx4UHJvZD5NRCBCYXRhdGE8L3hQcm9kPjxDRk9QPjUwMDA8L0NGT1A+PHVDb20+dW48L3VDb20+PHFDb20+MS4wMDAwPC9xQ29tPjx2VW5Db20+My4wMDA8L3ZVbkNvbT48dlByb2Q+My4wMDwvdlByb2Q+PGluZFJlZ3JhPkE8L2luZFJlZ3JhPjx2SXRlbT4zLjAwPC92SXRlbT48b2JzRmlzY29EZXQgeENhbXBvRGV0PSJDb2QuTkNNIj48eFRleHRvRGV0PjIwMDUyMDAwPC94VGV4dG9EZXQ+PC9vYnNGaXNjb0RldD48L3Byb2Q+PGltcG9zdG8+PElDTVM+PElDTVMwMD48T3JpZz4wPC9PcmlnPjxDU1Q+MDA8L0NTVD48cElDTVM+My4yMDwvcElDTVM+PHZJQ01TPjAuMTA8L3ZJQ01TPjwvSUNNUzAwPjwvSUNNUz48UElTPjxQSVNBbGlxPjxDU1Q+MDE8L0NTVD48dkJDPjMuMDA8L3ZCQz48cFBJUz4xLjY1MDA8L3BQSVM+PHZQSVM+NC45NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4zLjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjIyLjgwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMyI+PHByb2Q+PGNQcm9kPjkwMDg8L2NQcm9kPjx4UHJvZD5GcmVlIFJlZmlsbDwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT4xMC45MDA8L3ZVbkNvbT48dlByb2Q+MTAuOTA8L3ZQcm9kPjxpbmRSZWdyYT5BPC9pbmRSZWdyYT48dkl0ZW0+MTAuOTA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MjEwNjkwMTA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5DRVNUIj48eFRleHRvRGV0PjMwMDMwMDwveFRleHRvRGV0Pjwvb2JzRmlzY29EZXQ+PC9wcm9kPjxpbXBvc3RvPjxJQ01TPjxJQ01TMDA+PE9yaWc+MDwvT3JpZz48Q1NUPjAwPC9DU1Q+PHBJQ01TPjAuMDA8L3BJQ01TPjx2SUNNUz4wLjAwPC92SUNNUz48L0lDTVMwMD48L0lDTVM+PFBJUz48UElTQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNy45ODwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwQ09GSU5TPjcuNjAwMDwvcENPRklOUz48dkNPRklOUz44Mi44NDwvdkNPRklOUz48L0NPRklOU0FsaXE+PC9DT0ZJTlM+PC9pbXBvc3RvPjwvZGV0Pjx0b3RhbD48SUNNU1RvdD48dklDTVM+MC4zOTwvdklDTVM+PHZQcm9kPjIyLjkwPC92UHJvZD48dkRlc2M+MC4wMDwvdkRlc2M+PHZQSVM+MzcuNzg8L3ZQSVM+PHZDT0ZJTlM+MTc0LjA0PC92Q09GSU5TPjx2UElTU1Q+MC4wMDwvdlBJU1NUPjx2Q09GSU5TU1Q+MC4wMDwvdkNPRklOU1NUPjx2T3V0cm8+MC4wMDwvdk91dHJvPjwvSUNNU1RvdD48dkNGZT4yMi45MDwvdkNGZT48L3RvdGFsPjxwZ3RvPjxNUD48Y01QPjAxPC9jTVA+PHZNUD4yMi45MDwvdk1QPjwvTVA+PHZUcm9jbz4wLjAwPC92VHJvY28+PC9wZ3RvPjwvaW5mQ0ZlPjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L0Nhbm9uaWNhbGl6YXRpb25NZXRob2Q+PFNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvU2lnbmF0dXJlTWV0aG9kPjxSZWZlcmVuY2UgVVJJPSIjQ0ZlMzUxNjEwNjEwOTkwMDgwMDAxNDE1OTkwMDAwNDIwNzAwMDQ2OTY1NjU5MjEiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9UcmFuc2Zvcm0+PFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L1RyYW5zZm9ybT48L1RyYW5zZm9ybXM+PERpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiI+PC9EaWdlc3RNZXRob2Q+PERpZ2VzdFZhbHVlPmRHUEY3OWsrY3ZHcDZFUGdLYzBUdCs5TmhpVklZK0NRV0R2eUF0RG00Rjg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPldiM0h0VEdUSzBHaVhGaW8rVmkxeDNIMWd4MjloTDkvQS9ZYzdXc2Q2Tlc0Wjd0eVBFL2owQ0I4VHZaUGwvYjV1TnVvcS9OaGprWVJPTHBjRVA0K0o3RWtvRXpHS3NXMDNzQ2cvNC9TSGpKdUphQXhuZXBZa01qaXJaTm84VGRyV2xPeFU4OFdPNnFzRlZEMVJLZTlSek9EN3BhbWE2dTZnZVdPSzJsNW82TERpV2NFd2hleG1qZjVuYmUzV2tRRzBLZ0tPUnZWSWhoSU9rekliOHlRcnpLdjhsNTVGWVRmeDFPSER6Z0ROeVM2akZ1c1dsVzJKaDYyMmw0MTdua3NvY2ZBMHgzNy9RN3NFN2NweU4yT2JFS1dkeUc2a01kRGxQK21lUzVBTmhsR2g1emY0WTNMNG9XY1ptd3J6MENTSnFicEM4a25teWFxeVVwbjRYeC85Zz09PC9TaWduYXR1cmVWYWx1ZT48S2V5SW5mbz48WDUwOURhdGE+PFg1MDlDZXJ0aWZpY2F0ZT5NSUlHeVRDQ0JMR2dBd0lCQWdJSkFSemtEaGhIdWZSeU1BMEdDU3FHU0liM0RRRUJDd1VBTUdjeEN6QUpCZ05WQkFZVEFrSlNNVFV3TXdZRFZRUUtFeXhUWldOeVpYUmhjbWxoSUdSaElFWmhlbVZ1WkdFZ1pHOGdSWE4wWVdSdklHUmxJRk5oYnlCUVlYVnNiekVoTUI4R0ExVUVBeE1ZUVVNZ1UwRlVJR1JsSUZSbGMzUmxJRk5GUmtGYUlGTlFNQjRYRFRFMU1EY3dPREl3TURJMU1Wb1hEVEl3TURjd09ESXdNREkxTVZvd2djNHhFakFRQmdOVkJBVVRDVGt3TURBd05ESXdOekVMTUFrR0ExVUVCaE1DUWxJeEVqQVFCZ05WQkFnVENWTmhieUJRWVhWc2J6RVJNQThHQTFVRUNoTUlVMFZHUVZvdFUxQXhEekFOQmdOVkJBc1RCa0ZETFZOQlZERW9NQ1lHQTFVRUN4TWZRWFYwWlc1MGFXTmhaRzhnY0c5eUlFRlNJRk5GUmtGYUlGTlFJRk5CVkRGSk1FY0dBMVVFQXhOQVJFbE5RVk1nUkVVZ1RVVk1UeUJRU1UxRlRsUkJJRk5KVTFSRlRVRlRJRVJGSUZCUFRsUlBJRVVnUVVORlUxTlBJRG8yTVRBNU9UQXdPREF3TURFME1UQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUl1WWZKWW4vUHYzOGNkYjEwRjk5Nll3M2ZDSU9takVyOUV4dlB4SWRFWTFsRmNwYk9mRE5PbmZZYm9MMFRzLy9Qc3J4ZUlQSmxEc3ZRMEw0NndwSEROcEZmQkRxeHlkQlY0ckYrVklPbHZ1NmJ2eitEUEd2OW5xVFRHMldNeVhLdjVhdWVMY1l5VmR1MjRucE03V2VkZDBOaDZBQkk4Ni9qanZaR1JRS3RLYnN0KzRaV3RWSUZ0UU9pVHRIaFdOcEwxdW4rWit2MEg4dGpPRkhuK3N6MTlMcFZoV1NIbkJIVHBlRUFxdXdndmthV3czZXNQanlMUFplL2Z2cmlDbDdTWk9NRFBZcFdOenlXOThyLzVOYm1PNXRzQmd4RmZHcENiYnNqTlBOeG5VbWZ3VldYMzBLMVpMWUZnc2d6Z0J5Znl4UjJsY0FmZ2VnNkZkNWl1MEUyRUNBd0VBQWFPQ0FnNHdnZ0lLTUE0R0ExVWREd0VCL3dRRUF3SUY0REI3QmdOVkhTQUVkREJ5TUhBR0NTc0dBUVFCZ2V3dEF6QmpNR0VHQ0NzR0FRVUZCd0lCRmxWb2RIUndPaTh2WVdOellYUXVhVzF3Y21WdWMyRnZabWxqYVdGc0xtTnZiUzVpY2k5eVpYQnZjMmwwYjNKcGJ5OWtjR012WVdOellYUnpaV1poZW5Od0wyUndZMTloWTNOaGRITmxabUY2YzNBdWNHUm1NR3NHQTFVZEh3UmtNR0l3WUtCZW9GeUdXbWgwZEhBNkx5OWhZM05oZEMxMFpYTjBaUzVwYlhCeVpXNXpZVzltYVdOcFlXd3VZMjl0TG1KeUwzSmxjRzl6YVhSdmNtbHZMMnhqY2k5aFkzTmhkSE5sWm1GNmMzQXZZV056WVhSelpXWmhlbk53WTNKc0xtTnliRENCcGdZSUt3WUJCUVVIQVFFRWdaa3dnWll3TkFZSUt3WUJCUVVITUFHR0tHaDBkSEE2THk5dlkzTndMWEJwYkc5MExtbHRjSEpsYm5OaGIyWnBZMmxoYkM1amIyMHVZbkl3WGdZSUt3WUJCUVVITUFLR1VtaDBkSEE2THk5aFkzTmhkQzEwWlhOMFpTNXBiWEJ5Wlc1ellXOW1hV05wWVd3dVkyOXRMbUp5TDNKbGNHOXphWFJ2Y21sdkwyTmxjblJwWm1sallXUnZjeTloWTNOaGRDMTBaWE4wWlM1d04yTXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0l3Q1FZRFZSMFRCQUl3QURBa0JnTlZIUkVFSFRBYm9Ca0dCV0JNQVFNRG9CQUVEall4TURrNU1EQTRNREF3TVRReE1COEdBMVVkSXdRWU1CYUFGSTQ1UVFCYzhyZ0YycWh0bUxrQlJtMXVZOThDTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFDVlo5Ulc5bW9qMHIrNjF3K3V2MjN2aG9KL0VtVW55bnV6OXFSWU9EeGRQaVlGcSs5REZOYTF3ZXBselNmejJuUEZsSWUyNTNWZ2pVdlVObU9VdmQrV0gwR1ZEVE00dWlCV1kvcmxoZHFSZS9tQTZSUXlUbGNGZXFhLzNLdzhGNlpzYTFadHYyR0RuT0pTS3ZFV3ErVzhrR0l5YzF3SE1LSzREZUFqbllHYStDSVp2UjcwMi9JQVJjSkpleDNQQ25lbFBHejNUcklSZU1kNFNTRWNiOTV6SGFZeFZvY0E4Q3BiWlNIa3hjNXdKSUNWU29SRHZ3ZTl6endmZk9RTnJ3V280T3c2Z2RBNXdNcENIR3ZRN08zVWdnWklyclUzSkpYWUpPYVNuV1lPalp3azNmTlk3T2E5RXBFYnpPanA1Q1NEcXdqTndUcXpqM3VwTDl1alhEeksyVklwaEVjcXE4ZnBGN2IyYzJXREtZbGs3dldZbmp3SGZrVCtOM05KUHYwL3B1cGZzNVB4ajl0bFFqbW8zSXBkRDVteWQwNzRodFZrKyt0Si9UM2dxNFVBblBDczE3Vk4rNXlYcTU0YWs5S0ZmSGxWZmZKS055Y2RVcUtCSHZxNVQzZ1lGdFUyNElnTUQ5cElvNmtESGQyL2srQVlLR20vclN0c0RXYmY2eHdrVGtDNHJSVFpVOWJBNE1oQVVjajNFMnN4cUhuakYyeTd5alB3QllpU1FrT2RkWmJJaENrakJHSDhTVzREQk45ay9XYzAyOC9yNFptZm9Bc2MraE1IWXpuWEg5eEd3WkRDNklpdHFiWS9hRWJ2WmxKYlJvcHRaK05qSmFLaHFhcTZUa2I4MENxOHdzRjZlNVN6cW5kVDl4cUlENTI5SXppVlVzbU9ac2xyR3c9PTwvWDUwOUNlcnRpZmljYXRlPjwvWDUwOURhdGE+PC9LZXlJbmZvPjwvU2lnbmF0dXJlPjwvQ0ZlPgogICAgICAgIA==
    ...            discount-amount=${0.0}
    ...            price-basis=G
    ...            tip=${0.0}
    ...            total-tender=${47.8}
    ...            discount-applied=${None}
    ...            order-discount-amount=${0.0}
    ...            state-id=${5}
    ...            order-code=${order}
    ...            store-code=${store_code}
    ...            total-gross=${47.8}
    ...            total-after-discount=${45.38}
    ...            sale-type-id=${0}
    ...            tax-total=${2.42}
    ...            due-amount=${0.0}
    ...            change=${0.0}
    ...            originator-code=POS0001
    ...            price-list=EI
    ...            pos-code=${pos-code}
    ...            business-dt=2020-09-28
    ...            order-picture-custom-order-properties=${order-picture-custom-order-properties}
    ...            order-picture-tenders=${order-picture-tenders}
    ...            total-amount=${45.38}
    ...            pos-user-id=5813
    ...            total-gift=${0.0}
    ...            pod-type=FC

    [Return]    ${payload}

getPicturebodySemPosCode
    [Arguments]    ${customer_id}    ${order}    ${store_code}    ${pos-code}    ${creation-dttm}

    ${comment}=        Create List                         
    &{order-lines}=    Create Dictionary                   
    ...                comment=${comment}
    ...                unit-price=${0.0}
    ...                plu=1051
    ...                sub-unit-price=${0.0}
    ...                multiplied-qty=${1}
    ...                level=${0}
    ...                menu-item-code=1
    ...                item-type=COMBO
    ...                product=CM Whopper/Q
    ...                qty=${1}
    ...                inc-qty=${0}
    ...                order-picture-id=${129}
    ...                item-price=${0.0}
    ...                dec-qty=${0}
    ...                category-description=${None}
    ...                part-code=${None}
    ...                sub-category-description=${None}
    ...                line-number=${1}
    ...                price-key=${None}
    ...                added-unit-price=${0.0}

    ${order-picture-sale-lines}    Create List    ${order-lines}

    &{tenders1}    Create Dictionary
    ...            reference-amount=${47.8}
    ...            tender-detail={\"ReceiptMerchant\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"AuthCode\": \"1302\", \"IdAuth\": \"08496898000142\", \"ReceiptCustomer\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"TransactionProcessor\": \"00051\", \"CNPJAuth\": \"68536379000192\", \"Bandeira\": \"4\"}
    ...            tender-amount=${47.8}
    ...            order-picture-id=${129}
    ...            change-amount=${0.0}
    ...            tender-type=2

    ${order-picture-tenders}=    Create List    ${tenders1}
    # ${contactinfo}=    Create List    ${entry 1}    ${entry 2}

    &{order-picture-custom-order-properties}    Create Dictionary
    ...                                         SATISFACTION_SURVEY_VALIDATOR=11901112
    ...                                         FISCALIZATION_DATE=2020-09-28T14:29:48
    ...                                         LOYALTY_SYSTEM_USER_ID=${customer_id}
    ...                                         WHOPPER_WIFI_CODE=DBHOEEVX
    ...                                         SATISFACTION_SURVEY_CODE=0190991920892109

    &{payload}=    Create Dictionary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    ...            order-picture-sale-lines=${order-picture-sale-lines}
    ...            tax-applied=${2.42}
    ...            type-id=${0}
    ...            session=pos=1user=5813count=1period=20200928
    ...            xml-data=CiAgICAgICAgPENGZT48aW5mQ0ZlIElkPSJDRmUzNTE2MTA2MTA5OTAwODAwMDE0MTU5OTAwMDA0MjA3MDAwNDY5NjU2NTkyMSIgdmVyc2FvPSIwLjA2IiB2ZXJzYW9EYWRvc0VudD0iMC4wNiIgdmVyc2FvU0I9IjAxMDEwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj42NTY1OTI8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD45MDAwMDQyMDc8L25zZXJpZVNBVD48bkNGZT4xMjk8L25DRmU+PGRFbWk+MjAyMDA5Mjg8L2RFbWk+PGhFbWk+MTEyOTQ4PC9oRW1pPjxjRFY+MTwvY0RWPjx0cEFtYj4yPC90cEFtYj48Q05QSj4xNjcxNjExNDAwMDE3MjwvQ05QSj48c2lnbkFDPlNHUi1TQVQgU0lTVEVNQSBERSBHRVNUQU8gRSBSRVRBR1VBUkRBIERPIFNBVDwvc2lnbkFDPjxhc3NpbmF0dXJhUVJDT0RFPlE0c1VYWCtNdTh0YytocnlBYU9WRXRvU1FVTzNrL25hb3lwZjVsYlpmV2hGZVFQQzhzalNXNWNaU3ZLWkgxd29VMHFrMmYycmVZTEVhWWpGK1FhOU11cnFqbHhaYmxlWFowNExoWDhYQjNXeGxVUDBvdUwyTU5OeUlNaXJDc3VLZWR2RVE4N3c4THpWeDRzK0NvNmIyYUxDYzNzbG5yWURiRWt6NDVUS0pvNnF3cUlvdytYN05nZ2syNmFxbkFQaTFDR3VXMmlzbThVdHlmM2ZOOGdQeUk1TjJQYW02Mmh1N2d1Y01jek9LQXY1V2F3ZCtGOHBFM2xPT2RueFdidXp4cGxFUytIcVFqcmN6cVRydlJVbU5xT1Zqc2N2WEtQQ0JrbmdZeUVHeG11eFA5cFVwV0FHdjg5M1MydWNtSDVjT3gyVFRMY3pLMC9vS1JscmdUZ3NHZz09PC9hc3NpbmF0dXJhUVJDT0RFPjxudW1lcm9DYWl4YT4wMDE8L251bWVyb0NhaXhhPjwvaWRlPjxlbWl0PjxDTlBKPjYxMDk5MDA4MDAwMTQxPC9DTlBKPjx4Tm9tZT5ESU1BUyBERSBNRUxPIFBJTUVOVEEgU0lTVEVNQVMgREUgUE9OVE8gRSBBQ0VTU08gTFREQTwveE5vbWU+PHhGYW50PkRJTUVQPC94RmFudD48ZW5kZXJFbWl0Pjx4TGdyPkFWRU5JREEgTU9GQVJSRUo8L3hMZ3I+PG5ybz44NDA8L25ybz48eENwbD45MDg8L3hDcGw+PHhCYWlycm8+VkwuIExFT1BPTERJTkE8L3hCYWlycm8+PHhNdW4+U0FPIFBBVUxPPC94TXVuPjxDRVA+MDUzMTEwMDA8L0NFUD48L2VuZGVyRW1pdD48SUU+MTExMTExMTExMTExPC9JRT48Y1JlZ1RyaWI+MzwvY1JlZ1RyaWI+PGNSZWdUcmliSVNTUU4+MTwvY1JlZ1RyaWJJU1NRTj48aW5kUmF0SVNTUU4+TjwvaW5kUmF0SVNTUU4+PC9lbWl0PjxkZXN0PjwvZGVzdD48ZGV0IG5JdGVtPSIxIj48cHJvZD48Y1Byb2Q+MTA1MDwvY1Byb2Q+PHhQcm9kPldob3BwZXIvUTwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT45LjAwMDwvdlVuQ29tPjx2UHJvZD45LjAwPC92UHJvZD48aW5kUmVncmE+QTwvaW5kUmVncmE+PHZJdGVtPjkuMDA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MTkwMjIwMDA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjwvcHJvZD48aW1wb3N0bz48SUNNUz48SUNNUzAwPjxPcmlnPjA8L09yaWc+PENTVD4wMDwvQ1NUPjxwSUNNUz4zLjIwPC9wSUNNUz48dklDTVM+MC4yOTwvdklDTVM+PC9JQ01TMDA+PC9JQ01TPjxQSVM+PFBJU0FsaXE+PENTVD4wMTwvQ1NUPjx2QkM+OS4wMDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNC44NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz45LjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjY4LjQwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMiI+PHByb2Q+PGNQcm9kPjYwMTI8L2NQcm9kPjx4UHJvZD5NRCBCYXRhdGE8L3hQcm9kPjxDRk9QPjUwMDA8L0NGT1A+PHVDb20+dW48L3VDb20+PHFDb20+MS4wMDAwPC9xQ29tPjx2VW5Db20+My4wMDA8L3ZVbkNvbT48dlByb2Q+My4wMDwvdlByb2Q+PGluZFJlZ3JhPkE8L2luZFJlZ3JhPjx2SXRlbT4zLjAwPC92SXRlbT48b2JzRmlzY29EZXQgeENhbXBvRGV0PSJDb2QuTkNNIj48eFRleHRvRGV0PjIwMDUyMDAwPC94VGV4dG9EZXQ+PC9vYnNGaXNjb0RldD48L3Byb2Q+PGltcG9zdG8+PElDTVM+PElDTVMwMD48T3JpZz4wPC9PcmlnPjxDU1Q+MDA8L0NTVD48cElDTVM+My4yMDwvcElDTVM+PHZJQ01TPjAuMTA8L3ZJQ01TPjwvSUNNUzAwPjwvSUNNUz48UElTPjxQSVNBbGlxPjxDU1Q+MDE8L0NTVD48dkJDPjMuMDA8L3ZCQz48cFBJUz4xLjY1MDA8L3BQSVM+PHZQSVM+NC45NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4zLjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjIyLjgwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMyI+PHByb2Q+PGNQcm9kPjkwMDg8L2NQcm9kPjx4UHJvZD5GcmVlIFJlZmlsbDwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT4xMC45MDA8L3ZVbkNvbT48dlByb2Q+MTAuOTA8L3ZQcm9kPjxpbmRSZWdyYT5BPC9pbmRSZWdyYT48dkl0ZW0+MTAuOTA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MjEwNjkwMTA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5DRVNUIj48eFRleHRvRGV0PjMwMDMwMDwveFRleHRvRGV0Pjwvb2JzRmlzY29EZXQ+PC9wcm9kPjxpbXBvc3RvPjxJQ01TPjxJQ01TMDA+PE9yaWc+MDwvT3JpZz48Q1NUPjAwPC9DU1Q+PHBJQ01TPjAuMDA8L3BJQ01TPjx2SUNNUz4wLjAwPC92SUNNUz48L0lDTVMwMD48L0lDTVM+PFBJUz48UElTQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNy45ODwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwQ09GSU5TPjcuNjAwMDwvcENPRklOUz48dkNPRklOUz44Mi44NDwvdkNPRklOUz48L0NPRklOU0FsaXE+PC9DT0ZJTlM+PC9pbXBvc3RvPjwvZGV0Pjx0b3RhbD48SUNNU1RvdD48dklDTVM+MC4zOTwvdklDTVM+PHZQcm9kPjIyLjkwPC92UHJvZD48dkRlc2M+MC4wMDwvdkRlc2M+PHZQSVM+MzcuNzg8L3ZQSVM+PHZDT0ZJTlM+MTc0LjA0PC92Q09GSU5TPjx2UElTU1Q+MC4wMDwvdlBJU1NUPjx2Q09GSU5TU1Q+MC4wMDwvdkNPRklOU1NUPjx2T3V0cm8+MC4wMDwvdk91dHJvPjwvSUNNU1RvdD48dkNGZT4yMi45MDwvdkNGZT48L3RvdGFsPjxwZ3RvPjxNUD48Y01QPjAxPC9jTVA+PHZNUD4yMi45MDwvdk1QPjwvTVA+PHZUcm9jbz4wLjAwPC92VHJvY28+PC9wZ3RvPjwvaW5mQ0ZlPjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L0Nhbm9uaWNhbGl6YXRpb25NZXRob2Q+PFNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvU2lnbmF0dXJlTWV0aG9kPjxSZWZlcmVuY2UgVVJJPSIjQ0ZlMzUxNjEwNjEwOTkwMDgwMDAxNDE1OTkwMDAwNDIwNzAwMDQ2OTY1NjU5MjEiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9UcmFuc2Zvcm0+PFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L1RyYW5zZm9ybT48L1RyYW5zZm9ybXM+PERpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiI+PC9EaWdlc3RNZXRob2Q+PERpZ2VzdFZhbHVlPmRHUEY3OWsrY3ZHcDZFUGdLYzBUdCs5TmhpVklZK0NRV0R2eUF0RG00Rjg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPldiM0h0VEdUSzBHaVhGaW8rVmkxeDNIMWd4MjloTDkvQS9ZYzdXc2Q2Tlc0Wjd0eVBFL2owQ0I4VHZaUGwvYjV1TnVvcS9OaGprWVJPTHBjRVA0K0o3RWtvRXpHS3NXMDNzQ2cvNC9TSGpKdUphQXhuZXBZa01qaXJaTm84VGRyV2xPeFU4OFdPNnFzRlZEMVJLZTlSek9EN3BhbWE2dTZnZVdPSzJsNW82TERpV2NFd2hleG1qZjVuYmUzV2tRRzBLZ0tPUnZWSWhoSU9rekliOHlRcnpLdjhsNTVGWVRmeDFPSER6Z0ROeVM2akZ1c1dsVzJKaDYyMmw0MTdua3NvY2ZBMHgzNy9RN3NFN2NweU4yT2JFS1dkeUc2a01kRGxQK21lUzVBTmhsR2g1emY0WTNMNG9XY1ptd3J6MENTSnFicEM4a25teWFxeVVwbjRYeC85Zz09PC9TaWduYXR1cmVWYWx1ZT48S2V5SW5mbz48WDUwOURhdGE+PFg1MDlDZXJ0aWZpY2F0ZT5NSUlHeVRDQ0JMR2dBd0lCQWdJSkFSemtEaGhIdWZSeU1BMEdDU3FHU0liM0RRRUJDd1VBTUdjeEN6QUpCZ05WQkFZVEFrSlNNVFV3TXdZRFZRUUtFeXhUWldOeVpYUmhjbWxoSUdSaElFWmhlbVZ1WkdFZ1pHOGdSWE4wWVdSdklHUmxJRk5oYnlCUVlYVnNiekVoTUI4R0ExVUVBeE1ZUVVNZ1UwRlVJR1JsSUZSbGMzUmxJRk5GUmtGYUlGTlFNQjRYRFRFMU1EY3dPREl3TURJMU1Wb1hEVEl3TURjd09ESXdNREkxTVZvd2djNHhFakFRQmdOVkJBVVRDVGt3TURBd05ESXdOekVMTUFrR0ExVUVCaE1DUWxJeEVqQVFCZ05WQkFnVENWTmhieUJRWVhWc2J6RVJNQThHQTFVRUNoTUlVMFZHUVZvdFUxQXhEekFOQmdOVkJBc1RCa0ZETFZOQlZERW9NQ1lHQTFVRUN4TWZRWFYwWlc1MGFXTmhaRzhnY0c5eUlFRlNJRk5GUmtGYUlGTlFJRk5CVkRGSk1FY0dBMVVFQXhOQVJFbE5RVk1nUkVVZ1RVVk1UeUJRU1UxRlRsUkJJRk5KVTFSRlRVRlRJRVJGSUZCUFRsUlBJRVVnUVVORlUxTlBJRG8yTVRBNU9UQXdPREF3TURFME1UQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUl1WWZKWW4vUHYzOGNkYjEwRjk5Nll3M2ZDSU9takVyOUV4dlB4SWRFWTFsRmNwYk9mRE5PbmZZYm9MMFRzLy9Qc3J4ZUlQSmxEc3ZRMEw0NndwSEROcEZmQkRxeHlkQlY0ckYrVklPbHZ1NmJ2eitEUEd2OW5xVFRHMldNeVhLdjVhdWVMY1l5VmR1MjRucE03V2VkZDBOaDZBQkk4Ni9qanZaR1JRS3RLYnN0KzRaV3RWSUZ0UU9pVHRIaFdOcEwxdW4rWit2MEg4dGpPRkhuK3N6MTlMcFZoV1NIbkJIVHBlRUFxdXdndmthV3czZXNQanlMUFplL2Z2cmlDbDdTWk9NRFBZcFdOenlXOThyLzVOYm1PNXRzQmd4RmZHcENiYnNqTlBOeG5VbWZ3VldYMzBLMVpMWUZnc2d6Z0J5Znl4UjJsY0FmZ2VnNkZkNWl1MEUyRUNBd0VBQWFPQ0FnNHdnZ0lLTUE0R0ExVWREd0VCL3dRRUF3SUY0REI3QmdOVkhTQUVkREJ5TUhBR0NTc0dBUVFCZ2V3dEF6QmpNR0VHQ0NzR0FRVUZCd0lCRmxWb2RIUndPaTh2WVdOellYUXVhVzF3Y21WdWMyRnZabWxqYVdGc0xtTnZiUzVpY2k5eVpYQnZjMmwwYjNKcGJ5OWtjR012WVdOellYUnpaV1poZW5Od0wyUndZMTloWTNOaGRITmxabUY2YzNBdWNHUm1NR3NHQTFVZEh3UmtNR0l3WUtCZW9GeUdXbWgwZEhBNkx5OWhZM05oZEMxMFpYTjBaUzVwYlhCeVpXNXpZVzltYVdOcFlXd3VZMjl0TG1KeUwzSmxjRzl6YVhSdmNtbHZMMnhqY2k5aFkzTmhkSE5sWm1GNmMzQXZZV056WVhSelpXWmhlbk53WTNKc0xtTnliRENCcGdZSUt3WUJCUVVIQVFFRWdaa3dnWll3TkFZSUt3WUJCUVVITUFHR0tHaDBkSEE2THk5dlkzTndMWEJwYkc5MExtbHRjSEpsYm5OaGIyWnBZMmxoYkM1amIyMHVZbkl3WGdZSUt3WUJCUVVITUFLR1VtaDBkSEE2THk5aFkzTmhkQzEwWlhOMFpTNXBiWEJ5Wlc1ellXOW1hV05wWVd3dVkyOXRMbUp5TDNKbGNHOXphWFJ2Y21sdkwyTmxjblJwWm1sallXUnZjeTloWTNOaGRDMTBaWE4wWlM1d04yTXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0l3Q1FZRFZSMFRCQUl3QURBa0JnTlZIUkVFSFRBYm9Ca0dCV0JNQVFNRG9CQUVEall4TURrNU1EQTRNREF3TVRReE1COEdBMVVkSXdRWU1CYUFGSTQ1UVFCYzhyZ0YycWh0bUxrQlJtMXVZOThDTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFDVlo5Ulc5bW9qMHIrNjF3K3V2MjN2aG9KL0VtVW55bnV6OXFSWU9EeGRQaVlGcSs5REZOYTF3ZXBselNmejJuUEZsSWUyNTNWZ2pVdlVObU9VdmQrV0gwR1ZEVE00dWlCV1kvcmxoZHFSZS9tQTZSUXlUbGNGZXFhLzNLdzhGNlpzYTFadHYyR0RuT0pTS3ZFV3ErVzhrR0l5YzF3SE1LSzREZUFqbllHYStDSVp2UjcwMi9JQVJjSkpleDNQQ25lbFBHejNUcklSZU1kNFNTRWNiOTV6SGFZeFZvY0E4Q3BiWlNIa3hjNXdKSUNWU29SRHZ3ZTl6endmZk9RTnJ3V280T3c2Z2RBNXdNcENIR3ZRN08zVWdnWklyclUzSkpYWUpPYVNuV1lPalp3azNmTlk3T2E5RXBFYnpPanA1Q1NEcXdqTndUcXpqM3VwTDl1alhEeksyVklwaEVjcXE4ZnBGN2IyYzJXREtZbGs3dldZbmp3SGZrVCtOM05KUHYwL3B1cGZzNVB4ajl0bFFqbW8zSXBkRDVteWQwNzRodFZrKyt0Si9UM2dxNFVBblBDczE3Vk4rNXlYcTU0YWs5S0ZmSGxWZmZKS055Y2RVcUtCSHZxNVQzZ1lGdFUyNElnTUQ5cElvNmtESGQyL2srQVlLR20vclN0c0RXYmY2eHdrVGtDNHJSVFpVOWJBNE1oQVVjajNFMnN4cUhuakYyeTd5alB3QllpU1FrT2RkWmJJaENrakJHSDhTVzREQk45ay9XYzAyOC9yNFptZm9Bc2MraE1IWXpuWEg5eEd3WkRDNklpdHFiWS9hRWJ2WmxKYlJvcHRaK05qSmFLaHFhcTZUa2I4MENxOHdzRjZlNVN6cW5kVDl4cUlENTI5SXppVlVzbU9ac2xyR3c9PTwvWDUwOUNlcnRpZmljYXRlPjwvWDUwOURhdGE+PC9LZXlJbmZvPjwvU2lnbmF0dXJlPjwvQ0ZlPgogICAgICAgIA==
    ...            discount-amount=${0.0}
    ...            price-basis=G
    ...            tip=${0.0}
    ...            creation-dttm=${creation-dttm}
    ...            total-tender=${47.8}
    ...            discount-applied=${None}
    ...            order-discount-amount=${0.0}
    ...            state-id=${5}
    ...            order-code=${order}
    ...            store-code=${store_code}
    ...            total-gross=${47.8}
    ...            total-after-discount=${45.38}
    ...            sale-type-id=${0}
    ...            tax-total=${2.42}
    ...            due-amount=${0.0}
    ...            change=${0.0}
    ...            originator-code=POS0001
    ...            price-list=EI
    ...            business-dt=2020-09-28
    ...            order-picture-custom-order-properties=${order-picture-custom-order-properties}
    ...            order-picture-tenders=${order-picture-tenders}
    ...            total-amount=${45.38}
    ...            pos-user-id=5813
    ...            total-gift=${0.0}
    ...            pod-type=FC

    [Return]    ${payload}

getPicturebodySemOrderCode
    [Arguments]    ${customer_id}    ${order}    ${store_code}    ${pos-code}    ${creation-dttm}

    ${comment}=        Create List                         
    &{order-lines}=    Create Dictionary                   
    ...                comment=${comment}
    ...                unit-price=${0.0}
    ...                plu=1051
    ...                sub-unit-price=${0.0}
    ...                multiplied-qty=${1}
    ...                level=${0}
    ...                menu-item-code=1
    ...                item-type=COMBO
    ...                product=CM Whopper/Q
    ...                qty=${1}
    ...                inc-qty=${0}
    ...                order-picture-id=${129}
    ...                item-price=${0.0}
    ...                dec-qty=${0}
    ...                category-description=${None}
    ...                part-code=${None}
    ...                sub-category-description=${None}
    ...                line-number=${1}
    ...                price-key=${None}
    ...                added-unit-price=${0.0}

    ${order-picture-sale-lines}    Create List    ${order-lines}

    &{tenders1}    Create Dictionary
    ...            reference-amount=${47.8}
    ...            tender-detail={\"ReceiptMerchant\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"AuthCode\": \"1302\", \"IdAuth\": \"08496898000142\", \"ReceiptCustomer\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"TransactionProcessor\": \"00051\", \"CNPJAuth\": \"68536379000192\", \"Bandeira\": \"4\"}
    ...            tender-amount=${47.8}
    ...            order-picture-id=${129}
    ...            change-amount=${0.0}
    ...            tender-type=2

    ${order-picture-tenders}=    Create List    ${tenders1}
    # ${contactinfo}=    Create List    ${entry 1}    ${entry 2}

    &{order-picture-custom-order-properties}    Create Dictionary
    ...                                         SATISFACTION_SURVEY_VALIDATOR=11901112
    ...                                         FISCALIZATION_DATE=2020-09-28T14:29:48
    ...                                         LOYALTY_SYSTEM_USER_ID=${customer_id}
    ...                                         WHOPPER_WIFI_CODE=DBHOEEVX
    ...                                         SATISFACTION_SURVEY_CODE=0190991920892109

    &{payload}=    Create Dictionary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    ...            order-picture-sale-lines=${order-picture-sale-lines}
    ...            tax-applied=${2.42}
    ...            type-id=${0}
    ...            session=pos=1user=5813count=1period=20200928
    ...            xml-data=CiAgICAgICAgPENGZT48aW5mQ0ZlIElkPSJDRmUzNTE2MTA2MTA5OTAwODAwMDE0MTU5OTAwMDA0MjA3MDAwNDY5NjU2NTkyMSIgdmVyc2FvPSIwLjA2IiB2ZXJzYW9EYWRvc0VudD0iMC4wNiIgdmVyc2FvU0I9IjAxMDEwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj42NTY1OTI8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD45MDAwMDQyMDc8L25zZXJpZVNBVD48bkNGZT4xMjk8L25DRmU+PGRFbWk+MjAyMDA5Mjg8L2RFbWk+PGhFbWk+MTEyOTQ4PC9oRW1pPjxjRFY+MTwvY0RWPjx0cEFtYj4yPC90cEFtYj48Q05QSj4xNjcxNjExNDAwMDE3MjwvQ05QSj48c2lnbkFDPlNHUi1TQVQgU0lTVEVNQSBERSBHRVNUQU8gRSBSRVRBR1VBUkRBIERPIFNBVDwvc2lnbkFDPjxhc3NpbmF0dXJhUVJDT0RFPlE0c1VYWCtNdTh0YytocnlBYU9WRXRvU1FVTzNrL25hb3lwZjVsYlpmV2hGZVFQQzhzalNXNWNaU3ZLWkgxd29VMHFrMmYycmVZTEVhWWpGK1FhOU11cnFqbHhaYmxlWFowNExoWDhYQjNXeGxVUDBvdUwyTU5OeUlNaXJDc3VLZWR2RVE4N3c4THpWeDRzK0NvNmIyYUxDYzNzbG5yWURiRWt6NDVUS0pvNnF3cUlvdytYN05nZ2syNmFxbkFQaTFDR3VXMmlzbThVdHlmM2ZOOGdQeUk1TjJQYW02Mmh1N2d1Y01jek9LQXY1V2F3ZCtGOHBFM2xPT2RueFdidXp4cGxFUytIcVFqcmN6cVRydlJVbU5xT1Zqc2N2WEtQQ0JrbmdZeUVHeG11eFA5cFVwV0FHdjg5M1MydWNtSDVjT3gyVFRMY3pLMC9vS1JscmdUZ3NHZz09PC9hc3NpbmF0dXJhUVJDT0RFPjxudW1lcm9DYWl4YT4wMDE8L251bWVyb0NhaXhhPjwvaWRlPjxlbWl0PjxDTlBKPjYxMDk5MDA4MDAwMTQxPC9DTlBKPjx4Tm9tZT5ESU1BUyBERSBNRUxPIFBJTUVOVEEgU0lTVEVNQVMgREUgUE9OVE8gRSBBQ0VTU08gTFREQTwveE5vbWU+PHhGYW50PkRJTUVQPC94RmFudD48ZW5kZXJFbWl0Pjx4TGdyPkFWRU5JREEgTU9GQVJSRUo8L3hMZ3I+PG5ybz44NDA8L25ybz48eENwbD45MDg8L3hDcGw+PHhCYWlycm8+VkwuIExFT1BPTERJTkE8L3hCYWlycm8+PHhNdW4+U0FPIFBBVUxPPC94TXVuPjxDRVA+MDUzMTEwMDA8L0NFUD48L2VuZGVyRW1pdD48SUU+MTExMTExMTExMTExPC9JRT48Y1JlZ1RyaWI+MzwvY1JlZ1RyaWI+PGNSZWdUcmliSVNTUU4+MTwvY1JlZ1RyaWJJU1NRTj48aW5kUmF0SVNTUU4+TjwvaW5kUmF0SVNTUU4+PC9lbWl0PjxkZXN0PjwvZGVzdD48ZGV0IG5JdGVtPSIxIj48cHJvZD48Y1Byb2Q+MTA1MDwvY1Byb2Q+PHhQcm9kPldob3BwZXIvUTwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT45LjAwMDwvdlVuQ29tPjx2UHJvZD45LjAwPC92UHJvZD48aW5kUmVncmE+QTwvaW5kUmVncmE+PHZJdGVtPjkuMDA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MTkwMjIwMDA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjwvcHJvZD48aW1wb3N0bz48SUNNUz48SUNNUzAwPjxPcmlnPjA8L09yaWc+PENTVD4wMDwvQ1NUPjxwSUNNUz4zLjIwPC9wSUNNUz48dklDTVM+MC4yOTwvdklDTVM+PC9JQ01TMDA+PC9JQ01TPjxQSVM+PFBJU0FsaXE+PENTVD4wMTwvQ1NUPjx2QkM+OS4wMDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNC44NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz45LjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjY4LjQwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMiI+PHByb2Q+PGNQcm9kPjYwMTI8L2NQcm9kPjx4UHJvZD5NRCBCYXRhdGE8L3hQcm9kPjxDRk9QPjUwMDA8L0NGT1A+PHVDb20+dW48L3VDb20+PHFDb20+MS4wMDAwPC9xQ29tPjx2VW5Db20+My4wMDA8L3ZVbkNvbT48dlByb2Q+My4wMDwvdlByb2Q+PGluZFJlZ3JhPkE8L2luZFJlZ3JhPjx2SXRlbT4zLjAwPC92SXRlbT48b2JzRmlzY29EZXQgeENhbXBvRGV0PSJDb2QuTkNNIj48eFRleHRvRGV0PjIwMDUyMDAwPC94VGV4dG9EZXQ+PC9vYnNGaXNjb0RldD48L3Byb2Q+PGltcG9zdG8+PElDTVM+PElDTVMwMD48T3JpZz4wPC9PcmlnPjxDU1Q+MDA8L0NTVD48cElDTVM+My4yMDwvcElDTVM+PHZJQ01TPjAuMTA8L3ZJQ01TPjwvSUNNUzAwPjwvSUNNUz48UElTPjxQSVNBbGlxPjxDU1Q+MDE8L0NTVD48dkJDPjMuMDA8L3ZCQz48cFBJUz4xLjY1MDA8L3BQSVM+PHZQSVM+NC45NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4zLjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjIyLjgwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMyI+PHByb2Q+PGNQcm9kPjkwMDg8L2NQcm9kPjx4UHJvZD5GcmVlIFJlZmlsbDwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT4xMC45MDA8L3ZVbkNvbT48dlByb2Q+MTAuOTA8L3ZQcm9kPjxpbmRSZWdyYT5BPC9pbmRSZWdyYT48dkl0ZW0+MTAuOTA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MjEwNjkwMTA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5DRVNUIj48eFRleHRvRGV0PjMwMDMwMDwveFRleHRvRGV0Pjwvb2JzRmlzY29EZXQ+PC9wcm9kPjxpbXBvc3RvPjxJQ01TPjxJQ01TMDA+PE9yaWc+MDwvT3JpZz48Q1NUPjAwPC9DU1Q+PHBJQ01TPjAuMDA8L3BJQ01TPjx2SUNNUz4wLjAwPC92SUNNUz48L0lDTVMwMD48L0lDTVM+PFBJUz48UElTQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNy45ODwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwQ09GSU5TPjcuNjAwMDwvcENPRklOUz48dkNPRklOUz44Mi44NDwvdkNPRklOUz48L0NPRklOU0FsaXE+PC9DT0ZJTlM+PC9pbXBvc3RvPjwvZGV0Pjx0b3RhbD48SUNNU1RvdD48dklDTVM+MC4zOTwvdklDTVM+PHZQcm9kPjIyLjkwPC92UHJvZD48dkRlc2M+MC4wMDwvdkRlc2M+PHZQSVM+MzcuNzg8L3ZQSVM+PHZDT0ZJTlM+MTc0LjA0PC92Q09GSU5TPjx2UElTU1Q+MC4wMDwvdlBJU1NUPjx2Q09GSU5TU1Q+MC4wMDwvdkNPRklOU1NUPjx2T3V0cm8+MC4wMDwvdk91dHJvPjwvSUNNU1RvdD48dkNGZT4yMi45MDwvdkNGZT48L3RvdGFsPjxwZ3RvPjxNUD48Y01QPjAxPC9jTVA+PHZNUD4yMi45MDwvdk1QPjwvTVA+PHZUcm9jbz4wLjAwPC92VHJvY28+PC9wZ3RvPjwvaW5mQ0ZlPjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L0Nhbm9uaWNhbGl6YXRpb25NZXRob2Q+PFNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvU2lnbmF0dXJlTWV0aG9kPjxSZWZlcmVuY2UgVVJJPSIjQ0ZlMzUxNjEwNjEwOTkwMDgwMDAxNDE1OTkwMDAwNDIwNzAwMDQ2OTY1NjU5MjEiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9UcmFuc2Zvcm0+PFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L1RyYW5zZm9ybT48L1RyYW5zZm9ybXM+PERpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiI+PC9EaWdlc3RNZXRob2Q+PERpZ2VzdFZhbHVlPmRHUEY3OWsrY3ZHcDZFUGdLYzBUdCs5TmhpVklZK0NRV0R2eUF0RG00Rjg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPldiM0h0VEdUSzBHaVhGaW8rVmkxeDNIMWd4MjloTDkvQS9ZYzdXc2Q2Tlc0Wjd0eVBFL2owQ0I4VHZaUGwvYjV1TnVvcS9OaGprWVJPTHBjRVA0K0o3RWtvRXpHS3NXMDNzQ2cvNC9TSGpKdUphQXhuZXBZa01qaXJaTm84VGRyV2xPeFU4OFdPNnFzRlZEMVJLZTlSek9EN3BhbWE2dTZnZVdPSzJsNW82TERpV2NFd2hleG1qZjVuYmUzV2tRRzBLZ0tPUnZWSWhoSU9rekliOHlRcnpLdjhsNTVGWVRmeDFPSER6Z0ROeVM2akZ1c1dsVzJKaDYyMmw0MTdua3NvY2ZBMHgzNy9RN3NFN2NweU4yT2JFS1dkeUc2a01kRGxQK21lUzVBTmhsR2g1emY0WTNMNG9XY1ptd3J6MENTSnFicEM4a25teWFxeVVwbjRYeC85Zz09PC9TaWduYXR1cmVWYWx1ZT48S2V5SW5mbz48WDUwOURhdGE+PFg1MDlDZXJ0aWZpY2F0ZT5NSUlHeVRDQ0JMR2dBd0lCQWdJSkFSemtEaGhIdWZSeU1BMEdDU3FHU0liM0RRRUJDd1VBTUdjeEN6QUpCZ05WQkFZVEFrSlNNVFV3TXdZRFZRUUtFeXhUWldOeVpYUmhjbWxoSUdSaElFWmhlbVZ1WkdFZ1pHOGdSWE4wWVdSdklHUmxJRk5oYnlCUVlYVnNiekVoTUI4R0ExVUVBeE1ZUVVNZ1UwRlVJR1JsSUZSbGMzUmxJRk5GUmtGYUlGTlFNQjRYRFRFMU1EY3dPREl3TURJMU1Wb1hEVEl3TURjd09ESXdNREkxTVZvd2djNHhFakFRQmdOVkJBVVRDVGt3TURBd05ESXdOekVMTUFrR0ExVUVCaE1DUWxJeEVqQVFCZ05WQkFnVENWTmhieUJRWVhWc2J6RVJNQThHQTFVRUNoTUlVMFZHUVZvdFUxQXhEekFOQmdOVkJBc1RCa0ZETFZOQlZERW9NQ1lHQTFVRUN4TWZRWFYwWlc1MGFXTmhaRzhnY0c5eUlFRlNJRk5GUmtGYUlGTlFJRk5CVkRGSk1FY0dBMVVFQXhOQVJFbE5RVk1nUkVVZ1RVVk1UeUJRU1UxRlRsUkJJRk5KVTFSRlRVRlRJRVJGSUZCUFRsUlBJRVVnUVVORlUxTlBJRG8yTVRBNU9UQXdPREF3TURFME1UQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUl1WWZKWW4vUHYzOGNkYjEwRjk5Nll3M2ZDSU9takVyOUV4dlB4SWRFWTFsRmNwYk9mRE5PbmZZYm9MMFRzLy9Qc3J4ZUlQSmxEc3ZRMEw0NndwSEROcEZmQkRxeHlkQlY0ckYrVklPbHZ1NmJ2eitEUEd2OW5xVFRHMldNeVhLdjVhdWVMY1l5VmR1MjRucE03V2VkZDBOaDZBQkk4Ni9qanZaR1JRS3RLYnN0KzRaV3RWSUZ0UU9pVHRIaFdOcEwxdW4rWit2MEg4dGpPRkhuK3N6MTlMcFZoV1NIbkJIVHBlRUFxdXdndmthV3czZXNQanlMUFplL2Z2cmlDbDdTWk9NRFBZcFdOenlXOThyLzVOYm1PNXRzQmd4RmZHcENiYnNqTlBOeG5VbWZ3VldYMzBLMVpMWUZnc2d6Z0J5Znl4UjJsY0FmZ2VnNkZkNWl1MEUyRUNBd0VBQWFPQ0FnNHdnZ0lLTUE0R0ExVWREd0VCL3dRRUF3SUY0REI3QmdOVkhTQUVkREJ5TUhBR0NTc0dBUVFCZ2V3dEF6QmpNR0VHQ0NzR0FRVUZCd0lCRmxWb2RIUndPaTh2WVdOellYUXVhVzF3Y21WdWMyRnZabWxqYVdGc0xtTnZiUzVpY2k5eVpYQnZjMmwwYjNKcGJ5OWtjR012WVdOellYUnpaV1poZW5Od0wyUndZMTloWTNOaGRITmxabUY2YzNBdWNHUm1NR3NHQTFVZEh3UmtNR0l3WUtCZW9GeUdXbWgwZEhBNkx5OWhZM05oZEMxMFpYTjBaUzVwYlhCeVpXNXpZVzltYVdOcFlXd3VZMjl0TG1KeUwzSmxjRzl6YVhSdmNtbHZMMnhqY2k5aFkzTmhkSE5sWm1GNmMzQXZZV056WVhSelpXWmhlbk53WTNKc0xtTnliRENCcGdZSUt3WUJCUVVIQVFFRWdaa3dnWll3TkFZSUt3WUJCUVVITUFHR0tHaDBkSEE2THk5dlkzTndMWEJwYkc5MExtbHRjSEpsYm5OaGIyWnBZMmxoYkM1amIyMHVZbkl3WGdZSUt3WUJCUVVITUFLR1VtaDBkSEE2THk5aFkzTmhkQzEwWlhOMFpTNXBiWEJ5Wlc1ellXOW1hV05wWVd3dVkyOXRMbUp5TDNKbGNHOXphWFJ2Y21sdkwyTmxjblJwWm1sallXUnZjeTloWTNOaGRDMTBaWE4wWlM1d04yTXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0l3Q1FZRFZSMFRCQUl3QURBa0JnTlZIUkVFSFRBYm9Ca0dCV0JNQVFNRG9CQUVEall4TURrNU1EQTRNREF3TVRReE1COEdBMVVkSXdRWU1CYUFGSTQ1UVFCYzhyZ0YycWh0bUxrQlJtMXVZOThDTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFDVlo5Ulc5bW9qMHIrNjF3K3V2MjN2aG9KL0VtVW55bnV6OXFSWU9EeGRQaVlGcSs5REZOYTF3ZXBselNmejJuUEZsSWUyNTNWZ2pVdlVObU9VdmQrV0gwR1ZEVE00dWlCV1kvcmxoZHFSZS9tQTZSUXlUbGNGZXFhLzNLdzhGNlpzYTFadHYyR0RuT0pTS3ZFV3ErVzhrR0l5YzF3SE1LSzREZUFqbllHYStDSVp2UjcwMi9JQVJjSkpleDNQQ25lbFBHejNUcklSZU1kNFNTRWNiOTV6SGFZeFZvY0E4Q3BiWlNIa3hjNXdKSUNWU29SRHZ3ZTl6endmZk9RTnJ3V280T3c2Z2RBNXdNcENIR3ZRN08zVWdnWklyclUzSkpYWUpPYVNuV1lPalp3azNmTlk3T2E5RXBFYnpPanA1Q1NEcXdqTndUcXpqM3VwTDl1alhEeksyVklwaEVjcXE4ZnBGN2IyYzJXREtZbGs3dldZbmp3SGZrVCtOM05KUHYwL3B1cGZzNVB4ajl0bFFqbW8zSXBkRDVteWQwNzRodFZrKyt0Si9UM2dxNFVBblBDczE3Vk4rNXlYcTU0YWs5S0ZmSGxWZmZKS055Y2RVcUtCSHZxNVQzZ1lGdFUyNElnTUQ5cElvNmtESGQyL2srQVlLR20vclN0c0RXYmY2eHdrVGtDNHJSVFpVOWJBNE1oQVVjajNFMnN4cUhuakYyeTd5alB3QllpU1FrT2RkWmJJaENrakJHSDhTVzREQk45ay9XYzAyOC9yNFptZm9Bc2MraE1IWXpuWEg5eEd3WkRDNklpdHFiWS9hRWJ2WmxKYlJvcHRaK05qSmFLaHFhcTZUa2I4MENxOHdzRjZlNVN6cW5kVDl4cUlENTI5SXppVlVzbU9ac2xyR3c9PTwvWDUwOUNlcnRpZmljYXRlPjwvWDUwOURhdGE+PC9LZXlJbmZvPjwvU2lnbmF0dXJlPjwvQ0ZlPgogICAgICAgIA==
    ...            discount-amount=${0.0}
    ...            price-basis=G
    ...            tip=${0.0}
    ...            creation-dttm=${creation-dttm}
    ...            total-tender=${47.8}
    ...            discount-applied=${None}
    ...            order-discount-amount=${0.0}
    ...            state-id=${5}
    ...            store-code=${store_code}
    ...            total-gross=${47.8}
    ...            total-after-discount=${45.38}
    ...            sale-type-id=${0}
    ...            tax-total=${2.42}
    ...            due-amount=${0.0}
    ...            change=${0.0}
    ...            originator-code=POS0001
    ...            price-list=EI
    ...            pos-code=${pos-code}
    ...            business-dt=2020-09-28
    ...            order-picture-custom-order-properties=${order-picture-custom-order-properties}
    ...            order-picture-tenders=${order-picture-tenders}
    ...            total-amount=${45.38}
    ...            pos-user-id=5813
    ...            total-gift=${0.0}
    ...            pod-type=FC

    [Return]    ${payload}

getPicturebodySemStorecode
    [Arguments]    ${customer_id}    ${order}    ${store_code}    ${pos-code}    ${creation-dttm}

    ${comment}=        Create List                         
    &{order-lines}=    Create Dictionary                   
    ...                comment=${comment}
    ...                unit-price=${0.0}
    ...                plu=1051
    ...                sub-unit-price=${0.0}
    ...                multiplied-qty=${1}
    ...                level=${0}
    ...                menu-item-code=1
    ...                item-type=COMBO
    ...                product=CM Whopper/Q
    ...                qty=${1}
    ...                inc-qty=${0}
    ...                order-picture-id=${129}
    ...                item-price=${0.0}
    ...                dec-qty=${0}
    ...                category-description=${None}
    ...                part-code=${None}
    ...                sub-category-description=${None}
    ...                line-number=${1}
    ...                price-key=${None}
    ...                added-unit-price=${0.0}

    ${order-picture-sale-lines}    Create List    ${order-lines}

    &{tenders1}    Create Dictionary
    ...            reference-amount=${47.8}
    ...            tender-detail={\"ReceiptMerchant\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"AuthCode\": \"1302\", \"IdAuth\": \"08496898000142\", \"ReceiptCustomer\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"TransactionProcessor\": \"00051\", \"CNPJAuth\": \"68536379000192\", \"Bandeira\": \"4\"}
    ...            tender-amount=${47.8}
    ...            order-picture-id=${129}
    ...            change-amount=${0.0}
    ...            tender-type=2

    ${order-picture-tenders}=    Create List    ${tenders1}
    # ${contactinfo}=    Create List    ${entry 1}    ${entry 2}

    &{order-picture-custom-order-properties}    Create Dictionary
    ...                                         SATISFACTION_SURVEY_VALIDATOR=11901112
    ...                                         FISCALIZATION_DATE=2020-09-28T14:29:48
    ...                                         LOYALTY_SYSTEM_USER_ID=${customer_id}
    ...                                         WHOPPER_WIFI_CODE=DBHOEEVX
    ...                                         SATISFACTION_SURVEY_CODE=0190991920892109

    &{payload}=    Create Dictionary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    ...            order-picture-sale-lines=${order-picture-sale-lines}
    ...            tax-applied=${2.42}
    ...            type-id=${0}
    ...            session=pos=1user=5813count=1period=20200928
    ...            xml-data=CiAgICAgICAgPENGZT48aW5mQ0ZlIElkPSJDRmUzNTE2MTA2MTA5OTAwODAwMDE0MTU5OTAwMDA0MjA3MDAwNDY5NjU2NTkyMSIgdmVyc2FvPSIwLjA2IiB2ZXJzYW9EYWRvc0VudD0iMC4wNiIgdmVyc2FvU0I9IjAxMDEwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj42NTY1OTI8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD45MDAwMDQyMDc8L25zZXJpZVNBVD48bkNGZT4xMjk8L25DRmU+PGRFbWk+MjAyMDA5Mjg8L2RFbWk+PGhFbWk+MTEyOTQ4PC9oRW1pPjxjRFY+MTwvY0RWPjx0cEFtYj4yPC90cEFtYj48Q05QSj4xNjcxNjExNDAwMDE3MjwvQ05QSj48c2lnbkFDPlNHUi1TQVQgU0lTVEVNQSBERSBHRVNUQU8gRSBSRVRBR1VBUkRBIERPIFNBVDwvc2lnbkFDPjxhc3NpbmF0dXJhUVJDT0RFPlE0c1VYWCtNdTh0YytocnlBYU9WRXRvU1FVTzNrL25hb3lwZjVsYlpmV2hGZVFQQzhzalNXNWNaU3ZLWkgxd29VMHFrMmYycmVZTEVhWWpGK1FhOU11cnFqbHhaYmxlWFowNExoWDhYQjNXeGxVUDBvdUwyTU5OeUlNaXJDc3VLZWR2RVE4N3c4THpWeDRzK0NvNmIyYUxDYzNzbG5yWURiRWt6NDVUS0pvNnF3cUlvdytYN05nZ2syNmFxbkFQaTFDR3VXMmlzbThVdHlmM2ZOOGdQeUk1TjJQYW02Mmh1N2d1Y01jek9LQXY1V2F3ZCtGOHBFM2xPT2RueFdidXp4cGxFUytIcVFqcmN6cVRydlJVbU5xT1Zqc2N2WEtQQ0JrbmdZeUVHeG11eFA5cFVwV0FHdjg5M1MydWNtSDVjT3gyVFRMY3pLMC9vS1JscmdUZ3NHZz09PC9hc3NpbmF0dXJhUVJDT0RFPjxudW1lcm9DYWl4YT4wMDE8L251bWVyb0NhaXhhPjwvaWRlPjxlbWl0PjxDTlBKPjYxMDk5MDA4MDAwMTQxPC9DTlBKPjx4Tm9tZT5ESU1BUyBERSBNRUxPIFBJTUVOVEEgU0lTVEVNQVMgREUgUE9OVE8gRSBBQ0VTU08gTFREQTwveE5vbWU+PHhGYW50PkRJTUVQPC94RmFudD48ZW5kZXJFbWl0Pjx4TGdyPkFWRU5JREEgTU9GQVJSRUo8L3hMZ3I+PG5ybz44NDA8L25ybz48eENwbD45MDg8L3hDcGw+PHhCYWlycm8+VkwuIExFT1BPTERJTkE8L3hCYWlycm8+PHhNdW4+U0FPIFBBVUxPPC94TXVuPjxDRVA+MDUzMTEwMDA8L0NFUD48L2VuZGVyRW1pdD48SUU+MTExMTExMTExMTExPC9JRT48Y1JlZ1RyaWI+MzwvY1JlZ1RyaWI+PGNSZWdUcmliSVNTUU4+MTwvY1JlZ1RyaWJJU1NRTj48aW5kUmF0SVNTUU4+TjwvaW5kUmF0SVNTUU4+PC9lbWl0PjxkZXN0PjwvZGVzdD48ZGV0IG5JdGVtPSIxIj48cHJvZD48Y1Byb2Q+MTA1MDwvY1Byb2Q+PHhQcm9kPldob3BwZXIvUTwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT45LjAwMDwvdlVuQ29tPjx2UHJvZD45LjAwPC92UHJvZD48aW5kUmVncmE+QTwvaW5kUmVncmE+PHZJdGVtPjkuMDA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MTkwMjIwMDA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjwvcHJvZD48aW1wb3N0bz48SUNNUz48SUNNUzAwPjxPcmlnPjA8L09yaWc+PENTVD4wMDwvQ1NUPjxwSUNNUz4zLjIwPC9wSUNNUz48dklDTVM+MC4yOTwvdklDTVM+PC9JQ01TMDA+PC9JQ01TPjxQSVM+PFBJU0FsaXE+PENTVD4wMTwvQ1NUPjx2QkM+OS4wMDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNC44NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz45LjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjY4LjQwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMiI+PHByb2Q+PGNQcm9kPjYwMTI8L2NQcm9kPjx4UHJvZD5NRCBCYXRhdGE8L3hQcm9kPjxDRk9QPjUwMDA8L0NGT1A+PHVDb20+dW48L3VDb20+PHFDb20+MS4wMDAwPC9xQ29tPjx2VW5Db20+My4wMDA8L3ZVbkNvbT48dlByb2Q+My4wMDwvdlByb2Q+PGluZFJlZ3JhPkE8L2luZFJlZ3JhPjx2SXRlbT4zLjAwPC92SXRlbT48b2JzRmlzY29EZXQgeENhbXBvRGV0PSJDb2QuTkNNIj48eFRleHRvRGV0PjIwMDUyMDAwPC94VGV4dG9EZXQ+PC9vYnNGaXNjb0RldD48L3Byb2Q+PGltcG9zdG8+PElDTVM+PElDTVMwMD48T3JpZz4wPC9PcmlnPjxDU1Q+MDA8L0NTVD48cElDTVM+My4yMDwvcElDTVM+PHZJQ01TPjAuMTA8L3ZJQ01TPjwvSUNNUzAwPjwvSUNNUz48UElTPjxQSVNBbGlxPjxDU1Q+MDE8L0NTVD48dkJDPjMuMDA8L3ZCQz48cFBJUz4xLjY1MDA8L3BQSVM+PHZQSVM+NC45NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4zLjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjIyLjgwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMyI+PHByb2Q+PGNQcm9kPjkwMDg8L2NQcm9kPjx4UHJvZD5GcmVlIFJlZmlsbDwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT4xMC45MDA8L3ZVbkNvbT48dlByb2Q+MTAuOTA8L3ZQcm9kPjxpbmRSZWdyYT5BPC9pbmRSZWdyYT48dkl0ZW0+MTAuOTA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MjEwNjkwMTA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5DRVNUIj48eFRleHRvRGV0PjMwMDMwMDwveFRleHRvRGV0Pjwvb2JzRmlzY29EZXQ+PC9wcm9kPjxpbXBvc3RvPjxJQ01TPjxJQ01TMDA+PE9yaWc+MDwvT3JpZz48Q1NUPjAwPC9DU1Q+PHBJQ01TPjAuMDA8L3BJQ01TPjx2SUNNUz4wLjAwPC92SUNNUz48L0lDTVMwMD48L0lDTVM+PFBJUz48UElTQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNy45ODwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwQ09GSU5TPjcuNjAwMDwvcENPRklOUz48dkNPRklOUz44Mi44NDwvdkNPRklOUz48L0NPRklOU0FsaXE+PC9DT0ZJTlM+PC9pbXBvc3RvPjwvZGV0Pjx0b3RhbD48SUNNU1RvdD48dklDTVM+MC4zOTwvdklDTVM+PHZQcm9kPjIyLjkwPC92UHJvZD48dkRlc2M+MC4wMDwvdkRlc2M+PHZQSVM+MzcuNzg8L3ZQSVM+PHZDT0ZJTlM+MTc0LjA0PC92Q09GSU5TPjx2UElTU1Q+MC4wMDwvdlBJU1NUPjx2Q09GSU5TU1Q+MC4wMDwvdkNPRklOU1NUPjx2T3V0cm8+MC4wMDwvdk91dHJvPjwvSUNNU1RvdD48dkNGZT4yMi45MDwvdkNGZT48L3RvdGFsPjxwZ3RvPjxNUD48Y01QPjAxPC9jTVA+PHZNUD4yMi45MDwvdk1QPjwvTVA+PHZUcm9jbz4wLjAwPC92VHJvY28+PC9wZ3RvPjwvaW5mQ0ZlPjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L0Nhbm9uaWNhbGl6YXRpb25NZXRob2Q+PFNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvU2lnbmF0dXJlTWV0aG9kPjxSZWZlcmVuY2UgVVJJPSIjQ0ZlMzUxNjEwNjEwOTkwMDgwMDAxNDE1OTkwMDAwNDIwNzAwMDQ2OTY1NjU5MjEiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9UcmFuc2Zvcm0+PFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L1RyYW5zZm9ybT48L1RyYW5zZm9ybXM+PERpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiI+PC9EaWdlc3RNZXRob2Q+PERpZ2VzdFZhbHVlPmRHUEY3OWsrY3ZHcDZFUGdLYzBUdCs5TmhpVklZK0NRV0R2eUF0RG00Rjg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPldiM0h0VEdUSzBHaVhGaW8rVmkxeDNIMWd4MjloTDkvQS9ZYzdXc2Q2Tlc0Wjd0eVBFL2owQ0I4VHZaUGwvYjV1TnVvcS9OaGprWVJPTHBjRVA0K0o3RWtvRXpHS3NXMDNzQ2cvNC9TSGpKdUphQXhuZXBZa01qaXJaTm84VGRyV2xPeFU4OFdPNnFzRlZEMVJLZTlSek9EN3BhbWE2dTZnZVdPSzJsNW82TERpV2NFd2hleG1qZjVuYmUzV2tRRzBLZ0tPUnZWSWhoSU9rekliOHlRcnpLdjhsNTVGWVRmeDFPSER6Z0ROeVM2akZ1c1dsVzJKaDYyMmw0MTdua3NvY2ZBMHgzNy9RN3NFN2NweU4yT2JFS1dkeUc2a01kRGxQK21lUzVBTmhsR2g1emY0WTNMNG9XY1ptd3J6MENTSnFicEM4a25teWFxeVVwbjRYeC85Zz09PC9TaWduYXR1cmVWYWx1ZT48S2V5SW5mbz48WDUwOURhdGE+PFg1MDlDZXJ0aWZpY2F0ZT5NSUlHeVRDQ0JMR2dBd0lCQWdJSkFSemtEaGhIdWZSeU1BMEdDU3FHU0liM0RRRUJDd1VBTUdjeEN6QUpCZ05WQkFZVEFrSlNNVFV3TXdZRFZRUUtFeXhUWldOeVpYUmhjbWxoSUdSaElFWmhlbVZ1WkdFZ1pHOGdSWE4wWVdSdklHUmxJRk5oYnlCUVlYVnNiekVoTUI4R0ExVUVBeE1ZUVVNZ1UwRlVJR1JsSUZSbGMzUmxJRk5GUmtGYUlGTlFNQjRYRFRFMU1EY3dPREl3TURJMU1Wb1hEVEl3TURjd09ESXdNREkxTVZvd2djNHhFakFRQmdOVkJBVVRDVGt3TURBd05ESXdOekVMTUFrR0ExVUVCaE1DUWxJeEVqQVFCZ05WQkFnVENWTmhieUJRWVhWc2J6RVJNQThHQTFVRUNoTUlVMFZHUVZvdFUxQXhEekFOQmdOVkJBc1RCa0ZETFZOQlZERW9NQ1lHQTFVRUN4TWZRWFYwWlc1MGFXTmhaRzhnY0c5eUlFRlNJRk5GUmtGYUlGTlFJRk5CVkRGSk1FY0dBMVVFQXhOQVJFbE5RVk1nUkVVZ1RVVk1UeUJRU1UxRlRsUkJJRk5KVTFSRlRVRlRJRVJGSUZCUFRsUlBJRVVnUVVORlUxTlBJRG8yTVRBNU9UQXdPREF3TURFME1UQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUl1WWZKWW4vUHYzOGNkYjEwRjk5Nll3M2ZDSU9takVyOUV4dlB4SWRFWTFsRmNwYk9mRE5PbmZZYm9MMFRzLy9Qc3J4ZUlQSmxEc3ZRMEw0NndwSEROcEZmQkRxeHlkQlY0ckYrVklPbHZ1NmJ2eitEUEd2OW5xVFRHMldNeVhLdjVhdWVMY1l5VmR1MjRucE03V2VkZDBOaDZBQkk4Ni9qanZaR1JRS3RLYnN0KzRaV3RWSUZ0UU9pVHRIaFdOcEwxdW4rWit2MEg4dGpPRkhuK3N6MTlMcFZoV1NIbkJIVHBlRUFxdXdndmthV3czZXNQanlMUFplL2Z2cmlDbDdTWk9NRFBZcFdOenlXOThyLzVOYm1PNXRzQmd4RmZHcENiYnNqTlBOeG5VbWZ3VldYMzBLMVpMWUZnc2d6Z0J5Znl4UjJsY0FmZ2VnNkZkNWl1MEUyRUNBd0VBQWFPQ0FnNHdnZ0lLTUE0R0ExVWREd0VCL3dRRUF3SUY0REI3QmdOVkhTQUVkREJ5TUhBR0NTc0dBUVFCZ2V3dEF6QmpNR0VHQ0NzR0FRVUZCd0lCRmxWb2RIUndPaTh2WVdOellYUXVhVzF3Y21WdWMyRnZabWxqYVdGc0xtTnZiUzVpY2k5eVpYQnZjMmwwYjNKcGJ5OWtjR012WVdOellYUnpaV1poZW5Od0wyUndZMTloWTNOaGRITmxabUY2YzNBdWNHUm1NR3NHQTFVZEh3UmtNR0l3WUtCZW9GeUdXbWgwZEhBNkx5OWhZM05oZEMxMFpYTjBaUzVwYlhCeVpXNXpZVzltYVdOcFlXd3VZMjl0TG1KeUwzSmxjRzl6YVhSdmNtbHZMMnhqY2k5aFkzTmhkSE5sWm1GNmMzQXZZV056WVhSelpXWmhlbk53WTNKc0xtTnliRENCcGdZSUt3WUJCUVVIQVFFRWdaa3dnWll3TkFZSUt3WUJCUVVITUFHR0tHaDBkSEE2THk5dlkzTndMWEJwYkc5MExtbHRjSEpsYm5OaGIyWnBZMmxoYkM1amIyMHVZbkl3WGdZSUt3WUJCUVVITUFLR1VtaDBkSEE2THk5aFkzTmhkQzEwWlhOMFpTNXBiWEJ5Wlc1ellXOW1hV05wWVd3dVkyOXRMbUp5TDNKbGNHOXphWFJ2Y21sdkwyTmxjblJwWm1sallXUnZjeTloWTNOaGRDMTBaWE4wWlM1d04yTXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0l3Q1FZRFZSMFRCQUl3QURBa0JnTlZIUkVFSFRBYm9Ca0dCV0JNQVFNRG9CQUVEall4TURrNU1EQTRNREF3TVRReE1COEdBMVVkSXdRWU1CYUFGSTQ1UVFCYzhyZ0YycWh0bUxrQlJtMXVZOThDTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFDVlo5Ulc5bW9qMHIrNjF3K3V2MjN2aG9KL0VtVW55bnV6OXFSWU9EeGRQaVlGcSs5REZOYTF3ZXBselNmejJuUEZsSWUyNTNWZ2pVdlVObU9VdmQrV0gwR1ZEVE00dWlCV1kvcmxoZHFSZS9tQTZSUXlUbGNGZXFhLzNLdzhGNlpzYTFadHYyR0RuT0pTS3ZFV3ErVzhrR0l5YzF3SE1LSzREZUFqbllHYStDSVp2UjcwMi9JQVJjSkpleDNQQ25lbFBHejNUcklSZU1kNFNTRWNiOTV6SGFZeFZvY0E4Q3BiWlNIa3hjNXdKSUNWU29SRHZ3ZTl6endmZk9RTnJ3V280T3c2Z2RBNXdNcENIR3ZRN08zVWdnWklyclUzSkpYWUpPYVNuV1lPalp3azNmTlk3T2E5RXBFYnpPanA1Q1NEcXdqTndUcXpqM3VwTDl1alhEeksyVklwaEVjcXE4ZnBGN2IyYzJXREtZbGs3dldZbmp3SGZrVCtOM05KUHYwL3B1cGZzNVB4ajl0bFFqbW8zSXBkRDVteWQwNzRodFZrKyt0Si9UM2dxNFVBblBDczE3Vk4rNXlYcTU0YWs5S0ZmSGxWZmZKS055Y2RVcUtCSHZxNVQzZ1lGdFUyNElnTUQ5cElvNmtESGQyL2srQVlLR20vclN0c0RXYmY2eHdrVGtDNHJSVFpVOWJBNE1oQVVjajNFMnN4cUhuakYyeTd5alB3QllpU1FrT2RkWmJJaENrakJHSDhTVzREQk45ay9XYzAyOC9yNFptZm9Bc2MraE1IWXpuWEg5eEd3WkRDNklpdHFiWS9hRWJ2WmxKYlJvcHRaK05qSmFLaHFhcTZUa2I4MENxOHdzRjZlNVN6cW5kVDl4cUlENTI5SXppVlVzbU9ac2xyR3c9PTwvWDUwOUNlcnRpZmljYXRlPjwvWDUwOURhdGE+PC9LZXlJbmZvPjwvU2lnbmF0dXJlPjwvQ0ZlPgogICAgICAgIA==
    ...            discount-amount=${0.0}
    ...            price-basis=G
    ...            tip=${0.0}
    ...            creation-dttm=${creation-dttm}
    ...            total-tender=${47.8}
    ...            discount-applied=${None}
    ...            order-discount-amount=${0.0}
    ...            state-id=${5}
    ...            order-code=${order}
    ...            total-gross=${47.8}
    ...            total-after-discount=${45.38}
    ...            sale-type-id=${0}
    ...            tax-total=${2.42}
    ...            due-amount=${0.0}
    ...            change=${0.0}
    ...            originator-code=POS0001
    ...            price-list=EI
    ...            pos-code=${pos-code}
    ...            business-dt=2020-09-28
    ...            order-picture-custom-order-properties=${order-picture-custom-order-properties}
    ...            order-picture-tenders=${order-picture-tenders}
    ...            total-amount=${45.38}
    ...            pos-user-id=5813
    ...            total-gift=${0.0}
    ...            pod-type=FC

    [Return]    ${payload}

getPicturebodySaleLines
    [Arguments]    ${customer_id}    ${order}    ${store_code}    ${pos-code}    ${creation-dttm}    ${sale-lines}

    ${comment}=    Create List    

    ${order-picture-sale-lines}    Create List    ${sale-lines}

    &{tenders1}    Create Dictionary
    ...            reference-amount=${47.8}
    ...            tender-detail={\"ReceiptMerchant\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"AuthCode\": \"1302\", \"IdAuth\": \"08496898000142\", \"ReceiptCustomer\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"TransactionProcessor\": \"00051\", \"CNPJAuth\": \"68536379000192\", \"Bandeira\": \"4\"}
    ...            tender-amount=${47.8}
    ...            order-picture-id=${129}
    ...            change-amount=${0.0}
    ...            tender-type=2

    ${order-picture-tenders}=    Create List    ${tenders1}
    # ${contactinfo}=    Create List    ${entry 1}    ${entry 2}

    &{order-picture-custom-order-properties}    Create Dictionary
    ...                                         SATISFACTION_SURVEY_VALIDATOR=11901112
    ...                                         FISCALIZATION_DATE=2020-09-28T14:29:48
    ...                                         LOYALTY_SYSTEM_USER_ID=${customer_id}
    ...                                         WHOPPER_WIFI_CODE=DBHOEEVX
    ...                                         SATISFACTION_SURVEY_CODE=0190991920892109

    &{payload}=    Create Dictionary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    ...            order-picture-sale-lines=${order-picture-sale-lines}
    ...            tax-applied=${2.42}
    ...            type-id=${0}
    ...            session=pos=1user=5813count=1period=20200928
    ...            xml-data=CiAgICAgICAgPENGZT48aW5mQ0ZlIElkPSJDRmUzNTE2MTA2MTA5OTAwODAwMDE0MTU5OTAwMDA0MjA3MDAwNDY5NjU2NTkyMSIgdmVyc2FvPSIwLjA2IiB2ZXJzYW9EYWRvc0VudD0iMC4wNiIgdmVyc2FvU0I9IjAxMDEwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj42NTY1OTI8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD45MDAwMDQyMDc8L25zZXJpZVNBVD48bkNGZT4xMjk8L25DRmU+PGRFbWk+MjAyMDA5Mjg8L2RFbWk+PGhFbWk+MTEyOTQ4PC9oRW1pPjxjRFY+MTwvY0RWPjx0cEFtYj4yPC90cEFtYj48Q05QSj4xNjcxNjExNDAwMDE3MjwvQ05QSj48c2lnbkFDPlNHUi1TQVQgU0lTVEVNQSBERSBHRVNUQU8gRSBSRVRBR1VBUkRBIERPIFNBVDwvc2lnbkFDPjxhc3NpbmF0dXJhUVJDT0RFPlE0c1VYWCtNdTh0YytocnlBYU9WRXRvU1FVTzNrL25hb3lwZjVsYlpmV2hGZVFQQzhzalNXNWNaU3ZLWkgxd29VMHFrMmYycmVZTEVhWWpGK1FhOU11cnFqbHhaYmxlWFowNExoWDhYQjNXeGxVUDBvdUwyTU5OeUlNaXJDc3VLZWR2RVE4N3c4THpWeDRzK0NvNmIyYUxDYzNzbG5yWURiRWt6NDVUS0pvNnF3cUlvdytYN05nZ2syNmFxbkFQaTFDR3VXMmlzbThVdHlmM2ZOOGdQeUk1TjJQYW02Mmh1N2d1Y01jek9LQXY1V2F3ZCtGOHBFM2xPT2RueFdidXp4cGxFUytIcVFqcmN6cVRydlJVbU5xT1Zqc2N2WEtQQ0JrbmdZeUVHeG11eFA5cFVwV0FHdjg5M1MydWNtSDVjT3gyVFRMY3pLMC9vS1JscmdUZ3NHZz09PC9hc3NpbmF0dXJhUVJDT0RFPjxudW1lcm9DYWl4YT4wMDE8L251bWVyb0NhaXhhPjwvaWRlPjxlbWl0PjxDTlBKPjYxMDk5MDA4MDAwMTQxPC9DTlBKPjx4Tm9tZT5ESU1BUyBERSBNRUxPIFBJTUVOVEEgU0lTVEVNQVMgREUgUE9OVE8gRSBBQ0VTU08gTFREQTwveE5vbWU+PHhGYW50PkRJTUVQPC94RmFudD48ZW5kZXJFbWl0Pjx4TGdyPkFWRU5JREEgTU9GQVJSRUo8L3hMZ3I+PG5ybz44NDA8L25ybz48eENwbD45MDg8L3hDcGw+PHhCYWlycm8+VkwuIExFT1BPTERJTkE8L3hCYWlycm8+PHhNdW4+U0FPIFBBVUxPPC94TXVuPjxDRVA+MDUzMTEwMDA8L0NFUD48L2VuZGVyRW1pdD48SUU+MTExMTExMTExMTExPC9JRT48Y1JlZ1RyaWI+MzwvY1JlZ1RyaWI+PGNSZWdUcmliSVNTUU4+MTwvY1JlZ1RyaWJJU1NRTj48aW5kUmF0SVNTUU4+TjwvaW5kUmF0SVNTUU4+PC9lbWl0PjxkZXN0PjwvZGVzdD48ZGV0IG5JdGVtPSIxIj48cHJvZD48Y1Byb2Q+MTA1MDwvY1Byb2Q+PHhQcm9kPldob3BwZXIvUTwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT45LjAwMDwvdlVuQ29tPjx2UHJvZD45LjAwPC92UHJvZD48aW5kUmVncmE+QTwvaW5kUmVncmE+PHZJdGVtPjkuMDA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MTkwMjIwMDA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjwvcHJvZD48aW1wb3N0bz48SUNNUz48SUNNUzAwPjxPcmlnPjA8L09yaWc+PENTVD4wMDwvQ1NUPjxwSUNNUz4zLjIwPC9wSUNNUz48dklDTVM+MC4yOTwvdklDTVM+PC9JQ01TMDA+PC9JQ01TPjxQSVM+PFBJU0FsaXE+PENTVD4wMTwvQ1NUPjx2QkM+OS4wMDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNC44NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz45LjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjY4LjQwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMiI+PHByb2Q+PGNQcm9kPjYwMTI8L2NQcm9kPjx4UHJvZD5NRCBCYXRhdGE8L3hQcm9kPjxDRk9QPjUwMDA8L0NGT1A+PHVDb20+dW48L3VDb20+PHFDb20+MS4wMDAwPC9xQ29tPjx2VW5Db20+My4wMDA8L3ZVbkNvbT48dlByb2Q+My4wMDwvdlByb2Q+PGluZFJlZ3JhPkE8L2luZFJlZ3JhPjx2SXRlbT4zLjAwPC92SXRlbT48b2JzRmlzY29EZXQgeENhbXBvRGV0PSJDb2QuTkNNIj48eFRleHRvRGV0PjIwMDUyMDAwPC94VGV4dG9EZXQ+PC9vYnNGaXNjb0RldD48L3Byb2Q+PGltcG9zdG8+PElDTVM+PElDTVMwMD48T3JpZz4wPC9PcmlnPjxDU1Q+MDA8L0NTVD48cElDTVM+My4yMDwvcElDTVM+PHZJQ01TPjAuMTA8L3ZJQ01TPjwvSUNNUzAwPjwvSUNNUz48UElTPjxQSVNBbGlxPjxDU1Q+MDE8L0NTVD48dkJDPjMuMDA8L3ZCQz48cFBJUz4xLjY1MDA8L3BQSVM+PHZQSVM+NC45NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4zLjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjIyLjgwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMyI+PHByb2Q+PGNQcm9kPjkwMDg8L2NQcm9kPjx4UHJvZD5GcmVlIFJlZmlsbDwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT4xMC45MDA8L3ZVbkNvbT48dlByb2Q+MTAuOTA8L3ZQcm9kPjxpbmRSZWdyYT5BPC9pbmRSZWdyYT48dkl0ZW0+MTAuOTA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MjEwNjkwMTA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5DRVNUIj48eFRleHRvRGV0PjMwMDMwMDwveFRleHRvRGV0Pjwvb2JzRmlzY29EZXQ+PC9wcm9kPjxpbXBvc3RvPjxJQ01TPjxJQ01TMDA+PE9yaWc+MDwvT3JpZz48Q1NUPjAwPC9DU1Q+PHBJQ01TPjAuMDA8L3BJQ01TPjx2SUNNUz4wLjAwPC92SUNNUz48L0lDTVMwMD48L0lDTVM+PFBJUz48UElTQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNy45ODwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwQ09GSU5TPjcuNjAwMDwvcENPRklOUz48dkNPRklOUz44Mi44NDwvdkNPRklOUz48L0NPRklOU0FsaXE+PC9DT0ZJTlM+PC9pbXBvc3RvPjwvZGV0Pjx0b3RhbD48SUNNU1RvdD48dklDTVM+MC4zOTwvdklDTVM+PHZQcm9kPjIyLjkwPC92UHJvZD48dkRlc2M+MC4wMDwvdkRlc2M+PHZQSVM+MzcuNzg8L3ZQSVM+PHZDT0ZJTlM+MTc0LjA0PC92Q09GSU5TPjx2UElTU1Q+MC4wMDwvdlBJU1NUPjx2Q09GSU5TU1Q+MC4wMDwvdkNPRklOU1NUPjx2T3V0cm8+MC4wMDwvdk91dHJvPjwvSUNNU1RvdD48dkNGZT4yMi45MDwvdkNGZT48L3RvdGFsPjxwZ3RvPjxNUD48Y01QPjAxPC9jTVA+PHZNUD4yMi45MDwvdk1QPjwvTVA+PHZUcm9jbz4wLjAwPC92VHJvY28+PC9wZ3RvPjwvaW5mQ0ZlPjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L0Nhbm9uaWNhbGl6YXRpb25NZXRob2Q+PFNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvU2lnbmF0dXJlTWV0aG9kPjxSZWZlcmVuY2UgVVJJPSIjQ0ZlMzUxNjEwNjEwOTkwMDgwMDAxNDE1OTkwMDAwNDIwNzAwMDQ2OTY1NjU5MjEiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9UcmFuc2Zvcm0+PFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L1RyYW5zZm9ybT48L1RyYW5zZm9ybXM+PERpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiI+PC9EaWdlc3RNZXRob2Q+PERpZ2VzdFZhbHVlPmRHUEY3OWsrY3ZHcDZFUGdLYzBUdCs5TmhpVklZK0NRV0R2eUF0RG00Rjg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPldiM0h0VEdUSzBHaVhGaW8rVmkxeDNIMWd4MjloTDkvQS9ZYzdXc2Q2Tlc0Wjd0eVBFL2owQ0I4VHZaUGwvYjV1TnVvcS9OaGprWVJPTHBjRVA0K0o3RWtvRXpHS3NXMDNzQ2cvNC9TSGpKdUphQXhuZXBZa01qaXJaTm84VGRyV2xPeFU4OFdPNnFzRlZEMVJLZTlSek9EN3BhbWE2dTZnZVdPSzJsNW82TERpV2NFd2hleG1qZjVuYmUzV2tRRzBLZ0tPUnZWSWhoSU9rekliOHlRcnpLdjhsNTVGWVRmeDFPSER6Z0ROeVM2akZ1c1dsVzJKaDYyMmw0MTdua3NvY2ZBMHgzNy9RN3NFN2NweU4yT2JFS1dkeUc2a01kRGxQK21lUzVBTmhsR2g1emY0WTNMNG9XY1ptd3J6MENTSnFicEM4a25teWFxeVVwbjRYeC85Zz09PC9TaWduYXR1cmVWYWx1ZT48S2V5SW5mbz48WDUwOURhdGE+PFg1MDlDZXJ0aWZpY2F0ZT5NSUlHeVRDQ0JMR2dBd0lCQWdJSkFSemtEaGhIdWZSeU1BMEdDU3FHU0liM0RRRUJDd1VBTUdjeEN6QUpCZ05WQkFZVEFrSlNNVFV3TXdZRFZRUUtFeXhUWldOeVpYUmhjbWxoSUdSaElFWmhlbVZ1WkdFZ1pHOGdSWE4wWVdSdklHUmxJRk5oYnlCUVlYVnNiekVoTUI4R0ExVUVBeE1ZUVVNZ1UwRlVJR1JsSUZSbGMzUmxJRk5GUmtGYUlGTlFNQjRYRFRFMU1EY3dPREl3TURJMU1Wb1hEVEl3TURjd09ESXdNREkxTVZvd2djNHhFakFRQmdOVkJBVVRDVGt3TURBd05ESXdOekVMTUFrR0ExVUVCaE1DUWxJeEVqQVFCZ05WQkFnVENWTmhieUJRWVhWc2J6RVJNQThHQTFVRUNoTUlVMFZHUVZvdFUxQXhEekFOQmdOVkJBc1RCa0ZETFZOQlZERW9NQ1lHQTFVRUN4TWZRWFYwWlc1MGFXTmhaRzhnY0c5eUlFRlNJRk5GUmtGYUlGTlFJRk5CVkRGSk1FY0dBMVVFQXhOQVJFbE5RVk1nUkVVZ1RVVk1UeUJRU1UxRlRsUkJJRk5KVTFSRlRVRlRJRVJGSUZCUFRsUlBJRVVnUVVORlUxTlBJRG8yTVRBNU9UQXdPREF3TURFME1UQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUl1WWZKWW4vUHYzOGNkYjEwRjk5Nll3M2ZDSU9takVyOUV4dlB4SWRFWTFsRmNwYk9mRE5PbmZZYm9MMFRzLy9Qc3J4ZUlQSmxEc3ZRMEw0NndwSEROcEZmQkRxeHlkQlY0ckYrVklPbHZ1NmJ2eitEUEd2OW5xVFRHMldNeVhLdjVhdWVMY1l5VmR1MjRucE03V2VkZDBOaDZBQkk4Ni9qanZaR1JRS3RLYnN0KzRaV3RWSUZ0UU9pVHRIaFdOcEwxdW4rWit2MEg4dGpPRkhuK3N6MTlMcFZoV1NIbkJIVHBlRUFxdXdndmthV3czZXNQanlMUFplL2Z2cmlDbDdTWk9NRFBZcFdOenlXOThyLzVOYm1PNXRzQmd4RmZHcENiYnNqTlBOeG5VbWZ3VldYMzBLMVpMWUZnc2d6Z0J5Znl4UjJsY0FmZ2VnNkZkNWl1MEUyRUNBd0VBQWFPQ0FnNHdnZ0lLTUE0R0ExVWREd0VCL3dRRUF3SUY0REI3QmdOVkhTQUVkREJ5TUhBR0NTc0dBUVFCZ2V3dEF6QmpNR0VHQ0NzR0FRVUZCd0lCRmxWb2RIUndPaTh2WVdOellYUXVhVzF3Y21WdWMyRnZabWxqYVdGc0xtTnZiUzVpY2k5eVpYQnZjMmwwYjNKcGJ5OWtjR012WVdOellYUnpaV1poZW5Od0wyUndZMTloWTNOaGRITmxabUY2YzNBdWNHUm1NR3NHQTFVZEh3UmtNR0l3WUtCZW9GeUdXbWgwZEhBNkx5OWhZM05oZEMxMFpYTjBaUzVwYlhCeVpXNXpZVzltYVdOcFlXd3VZMjl0TG1KeUwzSmxjRzl6YVhSdmNtbHZMMnhqY2k5aFkzTmhkSE5sWm1GNmMzQXZZV056WVhSelpXWmhlbk53WTNKc0xtTnliRENCcGdZSUt3WUJCUVVIQVFFRWdaa3dnWll3TkFZSUt3WUJCUVVITUFHR0tHaDBkSEE2THk5dlkzTndMWEJwYkc5MExtbHRjSEpsYm5OaGIyWnBZMmxoYkM1amIyMHVZbkl3WGdZSUt3WUJCUVVITUFLR1VtaDBkSEE2THk5aFkzTmhkQzEwWlhOMFpTNXBiWEJ5Wlc1ellXOW1hV05wWVd3dVkyOXRMbUp5TDNKbGNHOXphWFJ2Y21sdkwyTmxjblJwWm1sallXUnZjeTloWTNOaGRDMTBaWE4wWlM1d04yTXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0l3Q1FZRFZSMFRCQUl3QURBa0JnTlZIUkVFSFRBYm9Ca0dCV0JNQVFNRG9CQUVEall4TURrNU1EQTRNREF3TVRReE1COEdBMVVkSXdRWU1CYUFGSTQ1UVFCYzhyZ0YycWh0bUxrQlJtMXVZOThDTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFDVlo5Ulc5bW9qMHIrNjF3K3V2MjN2aG9KL0VtVW55bnV6OXFSWU9EeGRQaVlGcSs5REZOYTF3ZXBselNmejJuUEZsSWUyNTNWZ2pVdlVObU9VdmQrV0gwR1ZEVE00dWlCV1kvcmxoZHFSZS9tQTZSUXlUbGNGZXFhLzNLdzhGNlpzYTFadHYyR0RuT0pTS3ZFV3ErVzhrR0l5YzF3SE1LSzREZUFqbllHYStDSVp2UjcwMi9JQVJjSkpleDNQQ25lbFBHejNUcklSZU1kNFNTRWNiOTV6SGFZeFZvY0E4Q3BiWlNIa3hjNXdKSUNWU29SRHZ3ZTl6endmZk9RTnJ3V280T3c2Z2RBNXdNcENIR3ZRN08zVWdnWklyclUzSkpYWUpPYVNuV1lPalp3azNmTlk3T2E5RXBFYnpPanA1Q1NEcXdqTndUcXpqM3VwTDl1alhEeksyVklwaEVjcXE4ZnBGN2IyYzJXREtZbGs3dldZbmp3SGZrVCtOM05KUHYwL3B1cGZzNVB4ajl0bFFqbW8zSXBkRDVteWQwNzRodFZrKyt0Si9UM2dxNFVBblBDczE3Vk4rNXlYcTU0YWs5S0ZmSGxWZmZKS055Y2RVcUtCSHZxNVQzZ1lGdFUyNElnTUQ5cElvNmtESGQyL2srQVlLR20vclN0c0RXYmY2eHdrVGtDNHJSVFpVOWJBNE1oQVVjajNFMnN4cUhuakYyeTd5alB3QllpU1FrT2RkWmJJaENrakJHSDhTVzREQk45ay9XYzAyOC9yNFptZm9Bc2MraE1IWXpuWEg5eEd3WkRDNklpdHFiWS9hRWJ2WmxKYlJvcHRaK05qSmFLaHFhcTZUa2I4MENxOHdzRjZlNVN6cW5kVDl4cUlENTI5SXppVlVzbU9ac2xyR3c9PTwvWDUwOUNlcnRpZmljYXRlPjwvWDUwOURhdGE+PC9LZXlJbmZvPjwvU2lnbmF0dXJlPjwvQ0ZlPgogICAgICAgIA==
    ...            discount-amount=${0.0}
    ...            price-basis=G
    ...            tip=${0.0}
    ...            creation-dttm=${creation-dttm}
    ...            total-tender=${47.8}
    ...            discount-applied=${None}
    ...            order-discount-amount=${0.0}
    ...            state-id=${5}
    ...            order-code=${order}
    ...            store-code=${store_code}
    ...            total-gross=${47.8}
    ...            total-after-discount=${45.38}
    ...            sale-type-id=${0}
    ...            tax-total=${2.42}
    ...            due-amount=${0.0}
    ...            change=${0.0}
    ...            originator-code=POS0001
    ...            price-list=EI
    ...            pos-code=${pos-code}
    ...            business-dt=2020-09-28
    ...            order-picture-custom-order-properties=${order-picture-custom-order-properties}
    ...            order-picture-tenders=${order-picture-tenders}
    ...            total-amount=${45.38}
    ...            pos-user-id=5813
    ...            total-gift=${0.0}
    ...            pod-type=FC

    [Return]    ${payload}

getPicturebodyParte
    [Arguments]    ${customer_id}    ${order}    ${store_code}    ${pos-code}    ${creation-dttm}    

    &{tenders1}    Create Dictionary
    ...            reference-amount=${47.8}
    ...            tender-detail={\"ReceiptMerchant\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"AuthCode\": \"1302\", \"IdAuth\": \"08496898000142\", \"ReceiptCustomer\": \"UmVjaWJvIGRlIHRlc3RlLgpTaXRlZiBGQUtF\", \"TransactionProcessor\": \"00051\", \"CNPJAuth\": \"68536379000192\", \"Bandeira\": \"4\"}
    ...            tender-amount=${47.8}
    ...            order-picture-id=${129}
    ...            change-amount=${0.0}
    ...            tender-type=2

    ${order-picture-tenders}=    Create List    ${tenders1}
    # ${contactinfo}=    Create List    ${entry 1}    ${entry 2}

    &{order-picture-custom-order-properties}    Create Dictionary
    ...                                         SATISFACTION_SURVEY_VALIDATOR=11901112
    ...                                         FISCALIZATION_DATE=2020-09-28T14:29:48
    ...                                         LOYALTY_SYSTEM_USER_ID=${customer_id}
    ...                                         WHOPPER_WIFI_CODE=DBHOEEVX
    ...                                         SATISFACTION_SURVEY_CODE=0190991920892109

    &{payload}=    Create Dictionary                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    ...            tax-applied=${2.42}
    ...            type-id=${0}
    ...            session=pos=1user=5813count=1period=20200928
    ...            xml-data=CiAgICAgICAgPENGZT48aW5mQ0ZlIElkPSJDRmUzNTE2MTA2MTA5OTAwODAwMDE0MTU5OTAwMDA0MjA3MDAwNDY5NjU2NTkyMSIgdmVyc2FvPSIwLjA2IiB2ZXJzYW9EYWRvc0VudD0iMC4wNiIgdmVyc2FvU0I9IjAxMDEwMCI+PGlkZT48Y1VGPjM1PC9jVUY+PGNORj42NTY1OTI8L2NORj48bW9kPjU5PC9tb2Q+PG5zZXJpZVNBVD45MDAwMDQyMDc8L25zZXJpZVNBVD48bkNGZT4xMjk8L25DRmU+PGRFbWk+MjAyMDA5Mjg8L2RFbWk+PGhFbWk+MTEyOTQ4PC9oRW1pPjxjRFY+MTwvY0RWPjx0cEFtYj4yPC90cEFtYj48Q05QSj4xNjcxNjExNDAwMDE3MjwvQ05QSj48c2lnbkFDPlNHUi1TQVQgU0lTVEVNQSBERSBHRVNUQU8gRSBSRVRBR1VBUkRBIERPIFNBVDwvc2lnbkFDPjxhc3NpbmF0dXJhUVJDT0RFPlE0c1VYWCtNdTh0YytocnlBYU9WRXRvU1FVTzNrL25hb3lwZjVsYlpmV2hGZVFQQzhzalNXNWNaU3ZLWkgxd29VMHFrMmYycmVZTEVhWWpGK1FhOU11cnFqbHhaYmxlWFowNExoWDhYQjNXeGxVUDBvdUwyTU5OeUlNaXJDc3VLZWR2RVE4N3c4THpWeDRzK0NvNmIyYUxDYzNzbG5yWURiRWt6NDVUS0pvNnF3cUlvdytYN05nZ2syNmFxbkFQaTFDR3VXMmlzbThVdHlmM2ZOOGdQeUk1TjJQYW02Mmh1N2d1Y01jek9LQXY1V2F3ZCtGOHBFM2xPT2RueFdidXp4cGxFUytIcVFqcmN6cVRydlJVbU5xT1Zqc2N2WEtQQ0JrbmdZeUVHeG11eFA5cFVwV0FHdjg5M1MydWNtSDVjT3gyVFRMY3pLMC9vS1JscmdUZ3NHZz09PC9hc3NpbmF0dXJhUVJDT0RFPjxudW1lcm9DYWl4YT4wMDE8L251bWVyb0NhaXhhPjwvaWRlPjxlbWl0PjxDTlBKPjYxMDk5MDA4MDAwMTQxPC9DTlBKPjx4Tm9tZT5ESU1BUyBERSBNRUxPIFBJTUVOVEEgU0lTVEVNQVMgREUgUE9OVE8gRSBBQ0VTU08gTFREQTwveE5vbWU+PHhGYW50PkRJTUVQPC94RmFudD48ZW5kZXJFbWl0Pjx4TGdyPkFWRU5JREEgTU9GQVJSRUo8L3hMZ3I+PG5ybz44NDA8L25ybz48eENwbD45MDg8L3hDcGw+PHhCYWlycm8+VkwuIExFT1BPTERJTkE8L3hCYWlycm8+PHhNdW4+U0FPIFBBVUxPPC94TXVuPjxDRVA+MDUzMTEwMDA8L0NFUD48L2VuZGVyRW1pdD48SUU+MTExMTExMTExMTExPC9JRT48Y1JlZ1RyaWI+MzwvY1JlZ1RyaWI+PGNSZWdUcmliSVNTUU4+MTwvY1JlZ1RyaWJJU1NRTj48aW5kUmF0SVNTUU4+TjwvaW5kUmF0SVNTUU4+PC9lbWl0PjxkZXN0PjwvZGVzdD48ZGV0IG5JdGVtPSIxIj48cHJvZD48Y1Byb2Q+MTA1MDwvY1Byb2Q+PHhQcm9kPldob3BwZXIvUTwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT45LjAwMDwvdlVuQ29tPjx2UHJvZD45LjAwPC92UHJvZD48aW5kUmVncmE+QTwvaW5kUmVncmE+PHZJdGVtPjkuMDA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MTkwMjIwMDA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjwvcHJvZD48aW1wb3N0bz48SUNNUz48SUNNUzAwPjxPcmlnPjA8L09yaWc+PENTVD4wMDwvQ1NUPjxwSUNNUz4zLjIwPC9wSUNNUz48dklDTVM+MC4yOTwvdklDTVM+PC9JQ01TMDA+PC9JQ01TPjxQSVM+PFBJU0FsaXE+PENTVD4wMTwvQ1NUPjx2QkM+OS4wMDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNC44NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz45LjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjY4LjQwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMiI+PHByb2Q+PGNQcm9kPjYwMTI8L2NQcm9kPjx4UHJvZD5NRCBCYXRhdGE8L3hQcm9kPjxDRk9QPjUwMDA8L0NGT1A+PHVDb20+dW48L3VDb20+PHFDb20+MS4wMDAwPC9xQ29tPjx2VW5Db20+My4wMDA8L3ZVbkNvbT48dlByb2Q+My4wMDwvdlByb2Q+PGluZFJlZ3JhPkE8L2luZFJlZ3JhPjx2SXRlbT4zLjAwPC92SXRlbT48b2JzRmlzY29EZXQgeENhbXBvRGV0PSJDb2QuTkNNIj48eFRleHRvRGV0PjIwMDUyMDAwPC94VGV4dG9EZXQ+PC9vYnNGaXNjb0RldD48L3Byb2Q+PGltcG9zdG8+PElDTVM+PElDTVMwMD48T3JpZz4wPC9PcmlnPjxDU1Q+MDA8L0NTVD48cElDTVM+My4yMDwvcElDTVM+PHZJQ01TPjAuMTA8L3ZJQ01TPjwvSUNNUzAwPjwvSUNNUz48UElTPjxQSVNBbGlxPjxDU1Q+MDE8L0NTVD48dkJDPjMuMDA8L3ZCQz48cFBJUz4xLjY1MDA8L3BQSVM+PHZQSVM+NC45NTwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4zLjAwPC92QkM+PHBDT0ZJTlM+Ny42MDAwPC9wQ09GSU5TPjx2Q09GSU5TPjIyLjgwPC92Q09GSU5TPjwvQ09GSU5TQWxpcT48L0NPRklOUz48L2ltcG9zdG8+PC9kZXQ+PGRldCBuSXRlbT0iMyI+PHByb2Q+PGNQcm9kPjkwMDg8L2NQcm9kPjx4UHJvZD5GcmVlIFJlZmlsbDwveFByb2Q+PENGT1A+NTAwMDwvQ0ZPUD48dUNvbT51bjwvdUNvbT48cUNvbT4xLjAwMDA8L3FDb20+PHZVbkNvbT4xMC45MDA8L3ZVbkNvbT48dlByb2Q+MTAuOTA8L3ZQcm9kPjxpbmRSZWdyYT5BPC9pbmRSZWdyYT48dkl0ZW0+MTAuOTA8L3ZJdGVtPjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5OQ00iPjx4VGV4dG9EZXQ+MjEwNjkwMTA8L3hUZXh0b0RldD48L29ic0Zpc2NvRGV0PjxvYnNGaXNjb0RldCB4Q2FtcG9EZXQ9IkNvZC5DRVNUIj48eFRleHRvRGV0PjMwMDMwMDwveFRleHRvRGV0Pjwvb2JzRmlzY29EZXQ+PC9wcm9kPjxpbXBvc3RvPjxJQ01TPjxJQ01TMDA+PE9yaWc+MDwvT3JpZz48Q1NUPjAwPC9DU1Q+PHBJQ01TPjAuMDA8L3BJQ01TPjx2SUNNUz4wLjAwPC92SUNNUz48L0lDTVMwMD48L0lDTVM+PFBJUz48UElTQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwUElTPjEuNjUwMDwvcFBJUz48dlBJUz4xNy45ODwvdlBJUz48L1BJU0FsaXE+PC9QSVM+PENPRklOUz48Q09GSU5TQWxpcT48Q1NUPjAxPC9DU1Q+PHZCQz4xMC45MDwvdkJDPjxwQ09GSU5TPjcuNjAwMDwvcENPRklOUz48dkNPRklOUz44Mi44NDwvdkNPRklOUz48L0NPRklOU0FsaXE+PC9DT0ZJTlM+PC9pbXBvc3RvPjwvZGV0Pjx0b3RhbD48SUNNU1RvdD48dklDTVM+MC4zOTwvdklDTVM+PHZQcm9kPjIyLjkwPC92UHJvZD48dkRlc2M+MC4wMDwvdkRlc2M+PHZQSVM+MzcuNzg8L3ZQSVM+PHZDT0ZJTlM+MTc0LjA0PC92Q09GSU5TPjx2UElTU1Q+MC4wMDwvdlBJU1NUPjx2Q09GSU5TU1Q+MC4wMDwvdkNPRklOU1NUPjx2T3V0cm8+MC4wMDwvdk91dHJvPjwvSUNNU1RvdD48dkNGZT4yMi45MDwvdkNGZT48L3RvdGFsPjxwZ3RvPjxNUD48Y01QPjAxPC9jTVA+PHZNUD4yMi45MDwvdk1QPjwvTVA+PHZUcm9jbz4wLjAwPC92VHJvY28+PC9wZ3RvPjwvaW5mQ0ZlPjxTaWduYXR1cmUgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvMDkveG1sZHNpZyMiPjxTaWduZWRJbmZvIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjIj48Q2Fub25pY2FsaXphdGlvbk1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L0Nhbm9uaWNhbGl6YXRpb25NZXRob2Q+PFNpZ25hdHVyZU1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZHNpZy1tb3JlI3JzYS1zaGEyNTYiPjwvU2lnbmF0dXJlTWV0aG9kPjxSZWZlcmVuY2UgVVJJPSIjQ0ZlMzUxNjEwNjEwOTkwMDgwMDAxNDE1OTkwMDAwNDIwNzAwMDQ2OTY1NjU5MjEiPjxUcmFuc2Zvcm1zPjxUcmFuc2Zvcm0gQWxnb3JpdGhtPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwLzA5L3htbGRzaWcjZW52ZWxvcGVkLXNpZ25hdHVyZSI+PC9UcmFuc2Zvcm0+PFRyYW5zZm9ybSBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLXhtbC1jMTRuLTIwMDEwMzE1Ij48L1RyYW5zZm9ybT48L1RyYW5zZm9ybXM+PERpZ2VzdE1ldGhvZCBBbGdvcml0aG09Imh0dHA6Ly93d3cudzMub3JnLzIwMDEvMDQveG1sZW5jI3NoYTI1NiI+PC9EaWdlc3RNZXRob2Q+PERpZ2VzdFZhbHVlPmRHUEY3OWsrY3ZHcDZFUGdLYzBUdCs5TmhpVklZK0NRV0R2eUF0RG00Rjg9PC9EaWdlc3RWYWx1ZT48L1JlZmVyZW5jZT48L1NpZ25lZEluZm8+PFNpZ25hdHVyZVZhbHVlPldiM0h0VEdUSzBHaVhGaW8rVmkxeDNIMWd4MjloTDkvQS9ZYzdXc2Q2Tlc0Wjd0eVBFL2owQ0I4VHZaUGwvYjV1TnVvcS9OaGprWVJPTHBjRVA0K0o3RWtvRXpHS3NXMDNzQ2cvNC9TSGpKdUphQXhuZXBZa01qaXJaTm84VGRyV2xPeFU4OFdPNnFzRlZEMVJLZTlSek9EN3BhbWE2dTZnZVdPSzJsNW82TERpV2NFd2hleG1qZjVuYmUzV2tRRzBLZ0tPUnZWSWhoSU9rekliOHlRcnpLdjhsNTVGWVRmeDFPSER6Z0ROeVM2akZ1c1dsVzJKaDYyMmw0MTdua3NvY2ZBMHgzNy9RN3NFN2NweU4yT2JFS1dkeUc2a01kRGxQK21lUzVBTmhsR2g1emY0WTNMNG9XY1ptd3J6MENTSnFicEM4a25teWFxeVVwbjRYeC85Zz09PC9TaWduYXR1cmVWYWx1ZT48S2V5SW5mbz48WDUwOURhdGE+PFg1MDlDZXJ0aWZpY2F0ZT5NSUlHeVRDQ0JMR2dBd0lCQWdJSkFSemtEaGhIdWZSeU1BMEdDU3FHU0liM0RRRUJDd1VBTUdjeEN6QUpCZ05WQkFZVEFrSlNNVFV3TXdZRFZRUUtFeXhUWldOeVpYUmhjbWxoSUdSaElFWmhlbVZ1WkdFZ1pHOGdSWE4wWVdSdklHUmxJRk5oYnlCUVlYVnNiekVoTUI4R0ExVUVBeE1ZUVVNZ1UwRlVJR1JsSUZSbGMzUmxJRk5GUmtGYUlGTlFNQjRYRFRFMU1EY3dPREl3TURJMU1Wb1hEVEl3TURjd09ESXdNREkxTVZvd2djNHhFakFRQmdOVkJBVVRDVGt3TURBd05ESXdOekVMTUFrR0ExVUVCaE1DUWxJeEVqQVFCZ05WQkFnVENWTmhieUJRWVhWc2J6RVJNQThHQTFVRUNoTUlVMFZHUVZvdFUxQXhEekFOQmdOVkJBc1RCa0ZETFZOQlZERW9NQ1lHQTFVRUN4TWZRWFYwWlc1MGFXTmhaRzhnY0c5eUlFRlNJRk5GUmtGYUlGTlFJRk5CVkRGSk1FY0dBMVVFQXhOQVJFbE5RVk1nUkVVZ1RVVk1UeUJRU1UxRlRsUkJJRk5KVTFSRlRVRlRJRVJGSUZCUFRsUlBJRVVnUVVORlUxTlBJRG8yTVRBNU9UQXdPREF3TURFME1UQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCQUl1WWZKWW4vUHYzOGNkYjEwRjk5Nll3M2ZDSU9takVyOUV4dlB4SWRFWTFsRmNwYk9mRE5PbmZZYm9MMFRzLy9Qc3J4ZUlQSmxEc3ZRMEw0NndwSEROcEZmQkRxeHlkQlY0ckYrVklPbHZ1NmJ2eitEUEd2OW5xVFRHMldNeVhLdjVhdWVMY1l5VmR1MjRucE03V2VkZDBOaDZBQkk4Ni9qanZaR1JRS3RLYnN0KzRaV3RWSUZ0UU9pVHRIaFdOcEwxdW4rWit2MEg4dGpPRkhuK3N6MTlMcFZoV1NIbkJIVHBlRUFxdXdndmthV3czZXNQanlMUFplL2Z2cmlDbDdTWk9NRFBZcFdOenlXOThyLzVOYm1PNXRzQmd4RmZHcENiYnNqTlBOeG5VbWZ3VldYMzBLMVpMWUZnc2d6Z0J5Znl4UjJsY0FmZ2VnNkZkNWl1MEUyRUNBd0VBQWFPQ0FnNHdnZ0lLTUE0R0ExVWREd0VCL3dRRUF3SUY0REI3QmdOVkhTQUVkREJ5TUhBR0NTc0dBUVFCZ2V3dEF6QmpNR0VHQ0NzR0FRVUZCd0lCRmxWb2RIUndPaTh2WVdOellYUXVhVzF3Y21WdWMyRnZabWxqYVdGc0xtTnZiUzVpY2k5eVpYQnZjMmwwYjNKcGJ5OWtjR012WVdOellYUnpaV1poZW5Od0wyUndZMTloWTNOaGRITmxabUY2YzNBdWNHUm1NR3NHQTFVZEh3UmtNR0l3WUtCZW9GeUdXbWgwZEhBNkx5OWhZM05oZEMxMFpYTjBaUzVwYlhCeVpXNXpZVzltYVdOcFlXd3VZMjl0TG1KeUwzSmxjRzl6YVhSdmNtbHZMMnhqY2k5aFkzTmhkSE5sWm1GNmMzQXZZV056WVhSelpXWmhlbk53WTNKc0xtTnliRENCcGdZSUt3WUJCUVVIQVFFRWdaa3dnWll3TkFZSUt3WUJCUVVITUFHR0tHaDBkSEE2THk5dlkzTndMWEJwYkc5MExtbHRjSEpsYm5OaGIyWnBZMmxoYkM1amIyMHVZbkl3WGdZSUt3WUJCUVVITUFLR1VtaDBkSEE2THk5aFkzTmhkQzEwWlhOMFpTNXBiWEJ5Wlc1ellXOW1hV05wWVd3dVkyOXRMbUp5TDNKbGNHOXphWFJ2Y21sdkwyTmxjblJwWm1sallXUnZjeTloWTNOaGRDMTBaWE4wWlM1d04yTXdFd1lEVlIwbEJBd3dDZ1lJS3dZQkJRVUhBd0l3Q1FZRFZSMFRCQUl3QURBa0JnTlZIUkVFSFRBYm9Ca0dCV0JNQVFNRG9CQUVEall4TURrNU1EQTRNREF3TVRReE1COEdBMVVkSXdRWU1CYUFGSTQ1UVFCYzhyZ0YycWh0bUxrQlJtMXVZOThDTUEwR0NTcUdTSWIzRFFFQkN3VUFBNElDQVFDVlo5Ulc5bW9qMHIrNjF3K3V2MjN2aG9KL0VtVW55bnV6OXFSWU9EeGRQaVlGcSs5REZOYTF3ZXBselNmejJuUEZsSWUyNTNWZ2pVdlVObU9VdmQrV0gwR1ZEVE00dWlCV1kvcmxoZHFSZS9tQTZSUXlUbGNGZXFhLzNLdzhGNlpzYTFadHYyR0RuT0pTS3ZFV3ErVzhrR0l5YzF3SE1LSzREZUFqbllHYStDSVp2UjcwMi9JQVJjSkpleDNQQ25lbFBHejNUcklSZU1kNFNTRWNiOTV6SGFZeFZvY0E4Q3BiWlNIa3hjNXdKSUNWU29SRHZ3ZTl6endmZk9RTnJ3V280T3c2Z2RBNXdNcENIR3ZRN08zVWdnWklyclUzSkpYWUpPYVNuV1lPalp3azNmTlk3T2E5RXBFYnpPanA1Q1NEcXdqTndUcXpqM3VwTDl1alhEeksyVklwaEVjcXE4ZnBGN2IyYzJXREtZbGs3dldZbmp3SGZrVCtOM05KUHYwL3B1cGZzNVB4ajl0bFFqbW8zSXBkRDVteWQwNzRodFZrKyt0Si9UM2dxNFVBblBDczE3Vk4rNXlYcTU0YWs5S0ZmSGxWZmZKS055Y2RVcUtCSHZxNVQzZ1lGdFUyNElnTUQ5cElvNmtESGQyL2srQVlLR20vclN0c0RXYmY2eHdrVGtDNHJSVFpVOWJBNE1oQVVjajNFMnN4cUhuakYyeTd5alB3QllpU1FrT2RkWmJJaENrakJHSDhTVzREQk45ay9XYzAyOC9yNFptZm9Bc2MraE1IWXpuWEg5eEd3WkRDNklpdHFiWS9hRWJ2WmxKYlJvcHRaK05qSmFLaHFhcTZUa2I4MENxOHdzRjZlNVN6cW5kVDl4cUlENTI5SXppVlVzbU9ac2xyR3c9PTwvWDUwOUNlcnRpZmljYXRlPjwvWDUwOURhdGE+PC9LZXlJbmZvPjwvU2lnbmF0dXJlPjwvQ0ZlPgogICAgICAgIA==
    ...            discount-amount=${0.0}
    ...            price-basis=G
    ...            tip=${0.0}
    ...            creation-dttm=${creation-dttm}
    ...            total-tender=${47.8}
    ...            discount-applied=${None}
    ...            order-discount-amount=${0.0}
    ...            state-id=${5}
    ...            order-code=${order}
    ...            store-code=${store_code}
    ...            total-gross=${47.8}
    ...            total-after-discount=${45.38}
    ...            sale-type-id=${0}
    ...            tax-total=${2.42}
    ...            due-amount=${0.0}
    ...            change=${0.0}
    ...            originator-code=POS0001
    ...            price-list=EI
    ...            pos-code=${pos-code}
    ...            business-dt=2020-09-28
    ...            order-picture-custom-order-properties=${order-picture-custom-order-properties}
    ...            order-picture-tenders=${order-picture-tenders}
    ...            total-amount=${45.38}
    ...            pos-user-id=5813
    ...            total-gift=${0.0}
    ...            pod-type=FC

    [Return]    ${payload}


PostOrderPicture
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              9999               1                     2020-09-28T14:29:36.224Z
    ${resp}=                Post Request              OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureSemToken
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}          9999               1                     2020-09-28T14:29:36.224Z
    ${resp}=                Post Request              OrcderPicture                     /order-picture    data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureTokenErrado
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=teste
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}           9999               1                     2020-09-28T14:29:36.224Z
    ${resp}=                Post Request              OrcderPicture                     /order-picture     data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureStoreCodeInexistente
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${store_code}           set Variable              00000
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              ${store_code}    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureStoreCodeInteiro
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${store_code}           set Variable              ${00000}
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              ${store_code}    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureStoreCodeBoolean
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${store_code}           Convert To Boolean        true
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              ${store_code}    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureStoreCodecaractere
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${store_code}           set Variable              $%#¨&#%&
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              ${store_code}    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureOrderCodeJaUsado
    setupApiOrderPicture
    &{headers}=             Create Dictionary    content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Set Variable         9815
    ${na}                   ${payloadCreate}=    kws_customer.PostNewConsumidor
    ${payload}              getPicturebody       ${payloadCreate.customer_id}      ${order}                                              9999               1                     2020-09-28T14:29:36.224Z
    ${resp}=                Post Request         OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureOrderCodeinteiro
    setupApiOrderPicture
    &{headers}=             Create Dictionary    content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Set Variable         ${9815}
    ${na}                   ${payloadCreate}=    kws_customer.PostNewConsumidor
    ${payload}              getPicturebody       ${payloadCreate.customer_id}      ${order}                                              9999               1                     2020-09-28T14:29:36.224Z
    ${resp}=                Post Request         OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureOrderCodeboolean
    setupApiOrderPicture
    &{headers}=             Create Dictionary     content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Convert To Boolean    true
    ${na}                   ${payloadCreate}=     kws_customer.PostNewConsumidor
    ${payload}              getPicturebody        ${payloadCreate.customer_id}      ${order}                                              9999               1                     2020-09-28T14:29:36.224Z
    ${resp}=                Post Request          OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureOrderCaractereEspecial
    setupApiOrderPicture
    &{headers}=             Create Dictionary    content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Set Variable         @#@!)@!U#J@!
    ${na}                   ${payloadCreate}=    kws_customer.PostNewConsumidor
    ${payload}              getPicturebody       ${payloadCreate.customer_id}      ${order}                                              9999               1                     2020-09-28T14:29:36.224Z
    ${resp}=                Post Request         OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPicturePosCodeInteiro
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${pos-code}             Set Variable              ${1}
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              9999               ${pos-code}           2020-09-28T14:29:36.224Z
    ${resp}=                Post Request              OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPicturePosCodeBooleano
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${pos-code}             Convert To Boolean        true
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              9999               ${pos-code}           2020-09-28T14:29:36.224Z
    ${resp}=                Post Request              OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPicturePosCodeCaractereEspecial
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${pos-code}             Set Variable              @$@%@$%@
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              9999               ${pos-code}           2020-09-28T14:29:36.224Z
    ${resp}=                Post Request              OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureCreationdttmInteiro
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${creation-dttm}        Set Variable              ${123456}
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              9999               1                     ${creation-dttm}    
    ${resp}=                Post Request              OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureCreationdttmCaractereEspecial
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${creation-dttm}        Set Variable              @#$@%@%
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              9999               1                     ${creation-dttm}
    ${resp}=                Post Request              OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureCreationdttmDataMenor
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${creation-dttm}        Set Variable              2020-09-28T14:29:36.224Z
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor
    ${payload}              getPicturebody            ${payloadCreate.customer_id}      ${order}                                              9999               1                     ${creation-dttm}
    ${resp}=                Post Request              OrcderPicture                     /order-picture                                        data=${payload}    headers=${headers}
    [Return]                ${resp}

PostOrderPictureSaleLinesInteiro
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${sale-lines}           Set Variable              ${1}
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebodySaleLines    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z    ${sale-lines}

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSaleLinesBoolean
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${sale-lines}           Convert To Boolean        true
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebodySaleLines    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z    ${sale-lines}

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSaleLinesCaractereEspecial
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                0123456789

    ${sale-lines}    Set Variable         @$@%@$%@
    ${na}            ${payloadCreate}=    kws_customer.PostNewConsumidor

    ${payload}    getPicturebodySaleLines    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z    ${sale-lines}

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSaleRemovendoParteBody
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebodyParte    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSaleStoreCodeVazio
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebody    ${payloadCreate.customer_id}    ${order}    ${EMPTY}    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureOrderCodeCodeVazio
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebody    ${payloadCreate.customer_id}    ${EMPTY}    9999    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPicturePosCodeVazio
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebody    ${payloadCreate.customer_id}    ${order}    9999    ${EMPTY}    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPicturecreationdttmVazio
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebody    ${payloadCreate.customer_id}    ${order}    9999    1    ${EMPTY}

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSaleLinesVazio
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebodySaleLines    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z    ${EMPTY}

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSemStroreCode
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebodySemStorecode    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSemOrderCode
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebodySemOrderCode    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSemPosCode
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebodySemPosCode    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSemCreationdttm
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebodySemCreationdttm    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

PostOrderPictureSemSaleLines
    setupApiOrderPicture
    &{headers}=             Create Dictionary         content-type=application/json     x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${order}                Generate random string    4                                 0123456789
    ${na}                   ${payloadCreate}=         kws_customer.PostNewConsumidor

    ${payload}    getPicturebodySemSaleLines    ${payloadCreate.customer_id}    ${order}    9999    1    2020-09-28T14:29:36.224Z

    ${resp}=    Post Request    OrcderPicture    /order-picture    data=${payload}    headers=${headers}
    [Return]    ${resp}

