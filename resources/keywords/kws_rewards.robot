*** Settings ***
Resource    ../base.robot

*** Keywords ***

setupApiRewards
    Create Session    rewards    https://rfvuzkrqx8.execute-api.us-east-1.amazonaws.com/dev/reward    verify=true


Resgatar a recompensa com sucesso - Status Code 201
    ${resp}             ResgatarRecompensa
    Status Should Be    201                   ${resp}

Resgatar recompensa com customer_id inexistente - Status Code 404
    ${resp}             ResgatarRecompensaIdInexistente
    Status Should Be    404                                ${resp}

Resgatar recompensa passando um inteiro no paramentro customer_id - Status Code 422
    ${resp}             ResgatarRecompensaIntID
    Status Should Be    422                        ${resp}


Resgatar recompensa passando um Booleano no paramentro customer_id - Status Code 422
    ${resp}             ResgatarRecompensaBooleanID
    Status Should Be    422                            ${resp}

Resgatar recompensa passando customer_id vázio - Status Code 404
    ${resp}             ResgatarRecompensaIDVazio
    Status Should Be    404                          ${resp}

Resgatar recompensa com reward_id inexistente - Status Code 404
    ${resp}             ResgatarRecompensaRewardIdInexistente
    Status Should Be    404                                      ${resp}

Resgatar recompensa passando um inteiro no paramentro reward_id - Status Code 404
    ${resp}             ResgatarRecompensaRewardIdInteiro
    Status Should Be    404                                  ${resp}

Resgatar recompensa passando um Booleano no paramentro reward_id - Status Code 422
    ${resp}             ResgatarRecompensaRewardIdboolean
    Status Should Be    422                                  ${resp}

Resgatar recompensa passando reward_id vázio - Status Code 422
    ${resp}             ResgatarRecompensaRewardIdvazio
    Status Should Be    422                                ${resp}

Resgatar recompensa já resgatada - Status Code 406
    ${resp}             ResgatarRecompensaJaResgatada
    Status Should Be    406                              ${resp}

Resgatar recompensa com pontuação maior do que o saldo existente - Status Code 400
    ${resp}             ResgatarRecompensaPontuacaoMaior
    Status Should Be    400                                 ${resp}

Resgatar recompensa passando sem o Body - Status Code 422
    ${resp}             ResgatarRecompensaSemBody
    Status Should Be    422                          ${resp}

Resgatar removendo do Header o x-api-token - Status Code 403
    ${resp}             ResgatarRecompensaSemToken
    Status Should Be    403                           ${resp}

Resgatar alterando a URL do Endpoint - Status Code 404
    ${resp}             ResgatarRecompensaUrlIncorreta
    Status Should Be    404                               ${resp}

# -----------------------------------------------------------------------------
Criar usuário e pegar o cupon
    ${resp}               ${payload}                kws_customer.CriaConsumidor
    # log to console        ${\n}
    # log to console        CUSTOMER_ID: ${payload.customer_id}
    &{headers}=           Create Dictionary         x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=              Get Request               rewards                                               /rewards?member_id=${payload.customer_id}    headers=${headers}
    FOR                   ${item}                   IN                                                    @{resp.json()}
    ${points}=            Set variable              ${item['points']}
    ${reward_id}=         Run Keyword IF            ${points}<=50                                         get reward_id                                ${item}
    Exit For Loop IF      ${points}<=50
    set suite variable    ${reward_id}
    END
    # log to console        REWARD ID: ${reward_id}
    [Return]              ${payload.customer_id}    ${reward_id}

Criar usuário e pegar o cupon maior que a pontuação
    ${resp}              ${payload}                kws_customer.CriaConsumidor
    # log to console    ${\n}
    # log to console    CUSTOMER_ID: ${payload.customer_id}
    &{headers}=          Create Dictionary         x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=             Get Request               rewards                                               /rewards?member_id=${payload.customer_id}    headers=${headers}
    FOR                  ${item}                   IN                                                    @{resp.json()}
    ${points}=           Set variable              ${item['points']}
    ${reward_id}=        Run Keyword IF            ${points}>50                                          Set Variable                                 ${item['reward_id']}
    Exit For Loop IF     ${points}>50
    set Test variable    ${reward_id}
    END
    # log to console       REWARD ID: ${reward_id}
    [Return]             ${payload.customer_id}    ${reward_id}

Pegar voucher ID
    [Arguments]         ${QR_CODE}
    ${resp}             ${voucher_id}=    kws_invoicePOS.GetBenefit    ${QR_CODE}
    Status Should Be    200               ${resp}
    [Return]            ${voucher_id}

Queimar Beneficio
    [Arguments]         ${voucher_id}
    ${resp}=            kws_invoicePOS.BurnBenefit    ${voucher_id}
    Status Should Be    204                           ${resp}

Verificar Voucher usado
    [Arguments]         ${QR_CODE}
    ${resp}=            kws_invoicePOS.GetBenefitused    ${QR_CODE}
    Status Should Be    406                              ${resp}
    [Return]            ${resp}

ResgatarRecompensa
    ${customer_id}             ${reward_id}                  Criar usuário e pegar o cupon
    &{headers}=                Create Dictionary             content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=                Create Dictionary             
    ...                        customer_id=${customer_id}
    ...                        reward_id=${reward_id}
    ${resp}=                   Post Request                  rewards                          /rewards/redeem                                       data=${payload}    headers=${headers}
    ${QR_CODE}=                set variable                  ${resp.json()['qr_code']}
    # Log to console             QRCODE: ${QR_CODE}
    ${voucher_id}=             Pegar voucher ID              ${QR_CODE}
    Queimar Beneficio          ${voucher_id}
    Verificar Voucher usado    ${QR_CODE}
    [Return]                   ${resp}

CriarQRCode
    ${customer_id}    ${reward_id}                  Criar usuário e pegar o cupon
    &{headers}=       Create Dictionary             content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                          /rewards/redeem                                       data=${payload}    headers=${headers}
    ${QR_CODE}=       set variable                  ${resp.json()['qr_code']}
    [Return]          ${QR_CODE}

CriarQRCodeMaiorque50
    ${customer_id}    ${reward_id}                  Criar usuário e pegar o cupon maior que a pontuação
    &{headers}=       Create Dictionary             content-type=application/json                          x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                                                /rewards/redeem                                       data=${payload}    headers=${headers}
    [Return]          ${resp}

CriarQRCodeUrlIncorreta
    ${customer_id}    ${reward_id}                  Criar usuário e pegar o cupon maior que a pontuação
    &{headers}=       Create Dictionary             content-type=application/json                          x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                                                /rewards/teste                                        data=${payload}    headers=${headers}
    [Return]          ${resp}

CriarQRCodeSemToken
    ${customer_id}    ${reward_id}                  Criar usuário e pegar o cupon maior que a pontuação
    &{headers}=       Create Dictionary             content-type=application/json                          
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                                                /rewards/redeem    data=${payload}    headers=${headers}
    [Return]          ${resp}

CriarQRCodeSemBody
    ${customer_id}    ${reward_id}         Criar usuário e pegar o cupon maior que a pontuação
    &{headers}=       Create Dictionary    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    ${resp}=          Post Request         rewards                                                /rewards/redeem    headers=${headers}
    [Return]          ${resp}

ResgatarRecompensaIdInexistente
    ${customer_id}    set variable                  testeautomaçãouserinexistente
    ${reward_id}      set variable                  a0s6s0000000D4LAAU
    &{headers}=       Create Dictionary             content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                          /rewards/redeem                                       data=${payload}    headers=${headers}
    [Return]          ${resp}

ResgatarRecompensaIntID
    ${customer_id}    set variable                  ${1234}
    ${reward_id}      set variable                  a0s6s0000000D4LAAU
    &{headers}=       Create Dictionary             content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                          /rewards/redeem                                       data=${payload}    headers=${headers}
    [Return]          ${resp}

ResgatarRecompensaBooleanID
    ${customer_id}    set variable                  true
    ${reward_id}      set variable                  a0s6s0000000D4LAAU
    &{headers}=       Create Dictionary             content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                          /rewards/redeem                                       data=${payload}    headers=${headers}
    [Return]          ${resp}

ResgatarRecompensaIDVazio
    ${customer_id}    set variable                  ${EMPTY}
    ${reward_id}      set variable                  a0s6s0000000D4LAAU
    &{headers}=       Create Dictionary             content-type=application/json    x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                          /rewards/redeem                                       data=${payload}    headers=${headers}
    [Return]          ${resp}

getReward_id
    [Arguments]      ${item}
    ${reward_id}=    Set variable    ${item['reward_id']}
    [Return]         ${reward_id}

ResgatarRecompensaRewardIdInexistente
    ${customer_id}    set variable                  987ca522-f75d-4b9d-879b-5308315e206b
    ${reward_id}      set variable                  a32a13a13aa3153a5a13a51aa
    &{headers}=       Create Dictionary             content-type=application/json           x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                                 /rewards/redeem                                       data=${payload}    headers=${headers}
    [Return]          ${resp}

ResgatarRecompensaRewardIdInteiro
    ${customer_id}    set variable                  987ca522-f75d-4b9d-879b-5308315e206b
    ${reward_id}      set variable                  1234567891023151
    &{headers}=       Create Dictionary             content-type=application/json           x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                                 /rewards/redeem                                       data=${payload}    headers=${headers}
    [Return]          ${resp}

ResgatarRecompensaRewardIdboolean
    ${customer_id}    set variable                  987ca522-f75d-4b9d-879b-5308315e206b
    ${reward_id}      set variable                  true
    &{headers}=       Create Dictionary             content-type=application/json           x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                                 /rewards/redeem                                       data=${payload}    headers=${headers}
    [Return]          ${resp}

ResgatarRecompensaRewardIdvazio
    ${customer_id}    set variable                  987ca522-f75d-4b9d-879b-5308315e206b
    ${reward_id}      set variable                  ${EMPTY}
    &{headers}=       Create Dictionary             content-type=application/json           x-api-key=D5ZrrdKjoz4KdwnmZzi701H9aE6cgVmU7ueX8hz7
    &{payload}=       Create Dictionary             
    ...               customer_id=${customer_id}
    ...               reward_id=${reward_id}
    ${resp}=          Post Request                  rewards                                 /rewards/redeem                                       data=${payload}    headers=${headers}
    [Return]          ${resp}

ResgatarRecompensaJaResgatada
    ${QR_CODE}=          CriarQRCode
    # Log to console       QR_CODE: ${QR_CODE}
    ${voucher}=          Pegar voucher ID           ${QR_CODE}
    Queimar Beneficio    ${voucher}
    ${resp}=             Verificar Voucher usado    ${QR_CODE}
    [Return]             ${resp}

ResgatarRecompensaPontuacaoMaior
    ${resp}=    CriarQRCodeMaiorque50
    [Return]    ${resp}

ResgatarRecompensaSemBody
    ${resp}=    CriarQRCodeSemBody
    [Return]    ${resp}

ResgatarRecompensaSemToken
    ${resp}=    CriarQRCodeSemToken
    [Return]    ${resp}

ResgatarRecompensaUrlIncorreta
    ${resp}=    CriarQRCodeUrlIncorreta
    [Return]    ${resp}
