*** Settings ***

########################################################################
##    ##                             
##    Libraries de todo o projeto    ##
##    ##
## #####################################################################
## CRIADO POR: EDGAR SILVA    ##
## DATA: 15/12/2020           ##
## ÁREA: QUALITY ASSURANCE    ##
## #####################################################################

Library    DebugLibrary
Library    FakerLibrary       locale=pt_BR
Library    RequestsLibrary
Library    Collections
Library    String
Library    JSONLibrary
Library    Collections        

Library    ../Libraries/uuid_generator.py
Library    ../Libraries/uuid_generator.py

Resource    keywords/kws_customer.robot
Resource    keywords/kws_home.robot
Resource    keywords/kws_rewards.robot
Resource    keywords/kws_invoicePOS.robot

*** Variables ***


