*** Settings ***
Documentation    Testes de api - Cadastro

Resource    ../../resources/base.robot

Test Setup    setupApihome

*** Test Cases ***

CT001: Home - Executar GET com customer_id cadastrado no Loyalty
    [Tags]    ct001
    Executar GET com customer_id cadastrado no Loyalty - Status Code 200

CT002: Home - Executar GET com um customer_id não cadastrado no Loyalty
    [Tags]    ct002
    Executar GET com um customer_id não cadastrado no Loyalty - Status Code 404

CT003: Home - Executar GET sem informar o parametro customer_id
    [Tags]    ct003
    Executar GET sem informar o parametro customer_id - Status Code 404

CT004: Home - Executar GET sem informar o Header x-api-key
    [Tags]    ct004
    Executar GET sem informar o Header x-api-key - Status Code 403

CT005: Home - Executar GET passando uma URL incorreta
    [Tags]    ct05
    Executar GET passando uma URL incorreta - Status Code 404

CT006: Home/Extract - Executar GET com customer_id cadastrado no Loyalty - Status Code 200
    [Tags]    ct06
    Executar GET na API Extract com customer_id cadastrado no Loyalty - Status Code 200

CT007: Home/Extract - Executar GET com um customer_id não cadastrado no Loyalty - Status Code 404
    [Tags]    ct07
    Executar GET na API Extract com um customer_id não cadastrado no Loyalty - Status Code 404

CT008: Home/Extract - Executar GET sem informar o parametro customer_id - Status Code 404
    [Tags]    ct08
    Executar GET na API Extract sem informar o parametro customer_id - Status Code 404

CT009: Home/Extract - Executar GET sem informar o Header x-api-key - Status Code 403
    [Tags]    ct09
    Executar GET na API Extract sem informar o Header x-api-key - Status Code 403

CT010: Home/Extract - Executar GET passando uma URL incorreta - Status Code 404
    [Tags]    ct10
    Executar GET na API Extract passando uma URL incorreta - Status Code 404

CT011: Home/Point Forecast - Executar GET com customer_id cadastrado no Loyalty - Status Code 200
    [Tags]    ct011
    Executar GET na API de Point Forecast com customer_id cadastrado no Loyalty - Status Code 200

CT012: Home/Point Forecast - Executar GET com um customer_id não cadastrado no Loyalty - Status Code 404
    [Tags]    ct012
    Executar GET na API de Point Forecast com um customer_id não cadastrado no Loyalty - Status Code 404

CT013: Home/Point Forecast - Executar GET sem informar o parametro customer_id - Status Code 404
    [Tags]    ct013
    Executar GET na API de Point Forecast sem informar o parametro customer_id - Status Code 404

CT014: Home/Point Forecast - Executar GET sem informar o Header x-api-key - Status Code 403
    [Tags]    ct014
    Executar GET na API de Point Forecast sem informar o Header x-api-key - Status Code 403

CT015: Home/Point Forecast - Executar GET passando uma URL incorreta - Status Code 404
    [Tags]    ct015
    Executar GET na API de Point Forecast passando uma URL incorreta - Status Code 404
