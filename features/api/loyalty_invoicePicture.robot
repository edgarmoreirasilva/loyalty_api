*** Settings ***
Documentation    Testes de api - invoice POS

Resource    ../../resources/base.robot
Test Setup    setupApiRewards

*** Test Cases ***

CT001: Executar Requisição com sucesso enviando informações corretas - Status Code 200 
    [Tags]    ct001
    Executar Requisição com sucesso enviando informações corretas - Status Code 200 
    
CT002: Executar Requisição sem informar o Header x-apy-key - Status Code 403
    [Tags]    ct002
    Executar Requisição sem informar o Header x-apy-key - Status Code 403
    
CT003: Executar Requisição alterando o valor do Header x-api-key - Status Code 403
    [Tags]    ct003
    Executar Requisição alterando o valor do Header x-api-key - Status Code 403
    
CT004: Executar Requisição sem informar o serviço com Store Code Inexistente
    [Tags]    ct004
    Executar Requisição sem informar o serviço com Store Code Inexistente
    
CT005: Enviar um valor inteiro no paramêtro store_code
    [Tags]    ct005
    Enviar um valor inteiro no paramêtro store_code
    
CT006: Enviar um valor Booleano no paramêtro store_code
    [Tags]    ct006
    Enviar um valor Booleano no paramêtro store_code
    
CT007: Enviar caracter especial no paramêtro store_code - Status Code 422
    [Tags]    ct007
    Enviar caracter especial no paramêtro store_code - Status Code 422
    
CT008: Executar requisição com um order_code já utilizado
    [Tags]    ct008
    Executar requisição com um order_code já utilizado
    
CT009: Enviar um valor inteiro no paramêtro order_code - Status Code 422
    [Tags]    ct009
    Enviar um valor inteiro no paramêtro order_code - Status Code 422
    
CT010: Enviar um valor Booleano no paramêtro order_code - Status Code 422
    [Tags]    ct010
    Enviar um valor Booleano no paramêtro order_code - Status Code 422
    
CT011: Enviar caracter especial no paramêtro order_code - Status Code 422
    [Tags]    ct011
    Enviar caracter especial no paramêtro order_code - Status Code 422
    
CT012: Enviar um valor inteiro no paramêtro pos-code - Status Code 422
    [Tags]    ct012
    Enviar um valor inteiro no paramêtro pos-code - Status Code 422
    
CT013: Enviar um valor Booleano no paramêtro pos-code - Status Code 422
    [Tags]    ct013
    Enviar um valor Booleano no paramêtro pos-code - Status Code 422
    
CT014: Enviar caracter especial no paramêtro pos-code - Status Code 422
    [Tags]    ct014
    Enviar caracter especial no paramêtro pos-code - Status Code 422
    
CT015: Enviar um valor inteiro no paramêtro creation-dttm - Status Code 422
    [Tags]    ct015
    Enviar um valor inteiro no paramêtro creation-dttm - Status Code 422
    
CT016: Enviar um valor Booleano no paramêtro creation-dttm - Status Code 422
    [Tags]    ct016
    Enviar um valor Booleano no paramêtro creation-dttm - Status Code 422
    
CT017: Enviar caracter especial no paramêtro creation-dttm - Status Code 422
    [Tags]    ct017
    Enviar caracter especial no paramêtro creation-dttm - Status Code 422
    
CT018: Enviar com uma data menor do que a do dia recorrente no parametro creation-dttm
    [Tags]    ct018
    Enviar com uma data menor do que a do dia recorrente no parametro creation-dttm
    
CT019: Enviar um valor inteiro no paramêtro order-picture-sale-lines - Status Code 422
    [Tags]    ct019
    Enviar um valor inteiro no paramêtro order-picture-sale-lines - Status Code 422
    
CT020: Enviar um valor Booleano no paramêtro order-picture-sale-lines - Status Code 422
    [Tags]    ct020
    Enviar um valor Booleano no paramêtro order-picture-sale-lines - Status Code 422
    
CT021: Enviar caracter especial no paramêtro order-picture-sale-lines - Status Code 422
    [Tags]    ct021
    Enviar caracter especial no paramêtro order-picture-sale-lines - Status Code 422
    
CT022: Executar teste removendo parte do Body da API
    [Tags]    ct022
    Executar teste removendo parte do Body da API
    
CT023: Executar teste enviando o parametro store_code vazio
    [Tags]    ct023
    Executar teste enviando o parametro store_code vazio
    
CT024: Executar teste enviando o parametro order-code vazio
    [Tags]    ct024
    Executar teste enviando o parametro order-code vazio
    
CT025: Executar teste enviando o paramentro pos-code vazio
    [Tags]    ct025
    Executar teste enviando o paramentro pos-code vazio
    
CT026: Executar teste enviando o parametro creation-dttm vazio
    [Tags]    ct026
    Executar teste enviando o parametro creation-dttm vazio
    
CT027: Executar teste enviando o parametro order-picture-sale-lines vazio
    [Tags]    ct027
    Executar teste enviando o parametro order-picture-sale-lines vazio
    
CT028: Executar teste removendo o parametro store_code
    [Tags]    ct028
    Executar teste removendo o parametro store_code
    
CT029: Executar teste removendo o parametro order-code
    [Tags]    ct029
    Executar teste removendo o parametro order-code
    
CT030: Executar teste removendo o paramentro pos-code
    [Tags]    ct030
    Executar teste removendo o paramentro pos-code
    
CT031: Executar teste removendo o parametro creation-dttm
    [Tags]    ct031
    Executar teste removendo o parametro creation-dttm
    
CT032: Executar teste removendo o parametro order-picture-sale-lines
    [Tags]    ct032
    Executar teste removendo o parametro order-picture-sale-lines
    
