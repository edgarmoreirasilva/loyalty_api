*** Settings ***
Documentation    Testes de api - Cadastro

Resource    ../../resources/base.robot

Test Setup    setupApiconsumer

*** Test Cases ***

CT001: Cadastrar-se na promoção com sucesso
    [Tags]    ct001
    Cadastrar-se na promoção com sucesso - Status Code 201
    
CT002: Cadastrar-se na promoção sem informar o CPF
    [Tags]    ct002
    Cadastrar-se na promoção sem informar o CPF - Status Code 422
    
CT003: Cadastrar-se na promoção sem informar o Email
    [Tags]    ct003
    Cadastrar-se na promoção sem informar o Email - Status Code 422
    
CT004: Cadastrar-se na promoção sem informar o name
    [Tags]    ct004
    Cadastrar-se na promoção sem informar o name - Status Code 422
    
CT005: Cadastrar-se na promoção sem informar o customer_id
    [Tags]    ct005
    Cadastrar-se na promoção sem informar o customer_id - Status Code 422
    
CT006: Cadastrar-se na promoção passando um String no parametro cpf
    [Tags]    ct006
    Cadastrar-se na promoção passando um String no parametro cpf - Status Code 422
    
CT007: Cadastrar-se na promoção passando uma Booleano no parametro cpf
    [Tags]    ct007
    Cadastrar-se na promoção passando uma Booleano no parametro cpf - Status Code 422
    
CT008: Cadastrar-se na promoção passando uma Inteiro no parametro email
    [Tags]    ct008
    Cadastrar-se na promoção passando uma Inteiro no parametro email - Status Code 422
    
CT009: Cadastrar-se na promoção passando uma Booleano no parametro email
    [Tags]    ct009
    Cadastrar-se na promoção passando uma Booleano no parametro email - Status Code 422
    
CT010: Cadastrar-se na promoção passando uma Inteiro no parametro name
    [Tags]    ct010
    Cadastrar-se na promoção passando uma Inteiro no parametro name - Status Code 201
    
CT011: Cadastrar-se na promoção passando uma Booleano no parametro name
    [Tags]    ct011
    Cadastrar-se na promoção passando uma Booleano no parametro name - Status Code 201
    
CT012: Cadastrar-se na promoção passando um inteiro no parametro customer_id
    [Tags]    ct012
    Cadastrar-se na promoção passando um inteiro no parametro customer_id - Status Code 201
    
CT013: Cadastrar-se na promoção passando um Booleano no parametro customer_id
    [Tags]    ct013
    Cadastrar-se na promoção passando um Booleano no parametro customer_id - Status Code 201
    
CT014: Tentar se cadastrar removendo o Body Required
    [Tags]    ct014
    Tentar se cadastrar removendo o Body Required - Status Code 422
    
CT015: Tentar se cadastrar removendo do Header o x-api-token
    [Tags]    ct015
    Tentar se cadastrar removendo do Header o x-api-token - Status Code 403
    
CT016: Tentar se cadastrar alterando a URL do Endpoint
    [Tags]    ct016
    Tentar se cadastrar alterando a URL do Endpoint - Status Code 404
    
CT017: Realizar cadastro com um CPF já cadastrado na FIELO
    [Tags]    ct017
    Realizar cadastro com um CPF já cadastrado na FIELO - Status Code 409
    
CT018: Realizar cadastro com um email já cadastrado na FIELO
    [Tags]    ct018
    Realizar cadastro com um email já cadastrado na FIELO - Status Code 409
    
CT019: Realizar cadastro com um name já cadastrado na FIELO
    [Tags]    ct019
    Realizar cadastro com um name já cadastrado na FIELO - Status Code 201
    
CT020: Realizar cadastro com um customer_id já cadastrado na FIELO
    [Tags]    ct020
    Realizar cadastro com um customer_id já cadastrado na FIELO - Status Code 409
    
CT021: Deletar usuario da Campanha
    [Tags]    ct021
    Deletar usuario da Campanha - Status Code 204
    
CT022: Deletar passando um customer_id inexistente
    [Tags]    ct022
    Deletar passando um customer_id inexistente - Status Code 404
    
CT023: Deletar customer sem informar o x-api-key
    [Tags]    ct023
    Deletar customer sem informar o x-api-key - Status Code 403
    
CT024: Alterar a URL do Endpoint DELETE
    [Tags]    ct024
    Alterar a URL do Endpoint DELETE - Status Code 404
    
CT025: Consultar cpf com sucesso
    [Tags]    ct025
    Consultar cpf com sucesso - Status Code 200
    
CT026: Consultar CPF não cadastrado no Loyalty
    [Tags]    ct026
    Consultar CPF não cadastrado no Loyalty - Status Code 404
    
CT027: Consultar CPF deletado do Loyalty
    [Tags]    ct027
    Consultar CPF deletado do Loyalty - Status Code 404
    
CT028: Executar requisição sem informar CPF
    [Tags]    ct028
    Executar requisição sem informar CPF - Status Code 405
    
CT029: Executar requisição removendo o Header x-api-key
    [Tags]    ct029
    Executar requisição removendo o Header x-api-key - Status Code 403
    