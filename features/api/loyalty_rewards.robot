*** Settings ***
Documentation    Testes de api - Rewards

Resource    ../../resources/base.robot
Test Setup    setupApiRewards

*** Test Cases ***
CT001: Resgatar a recompensa com sucesso - Status Code 201
    [tags]    ct001
    Resgatar a recompensa com sucesso - Status Code 201
    
CT002: Resgatar recompensa com customer_id inexistente - Status Code 404
    [tags]    ct002
    Resgatar recompensa com customer_id inexistente - Status Code 404
    
CT003: Resgatar recompensa passando um inteiro no paramentro customer_id - Status Code 422
    [tags]    ct003
    Resgatar recompensa passando um inteiro no paramentro customer_id - Status Code 422
    
CT004: Resgatar recompensa passando um Booleano no paramentro customer_id - Status Code 422
    [tags]    ct004
    Resgatar recompensa passando um Booleano no paramentro customer_id - Status Code 422
    
CT005: Resgatar recompensa passando customer_id vázio - Status Code 404
    [tags]    ct005
    Resgatar recompensa passando customer_id vázio - Status Code 404
    
CT006: Resgatar recompensa com reward_id inexistente - Status Code 404
    [tags]    ct006
    Resgatar recompensa com reward_id inexistente - Status Code 404
    
CT007: Resgatar recompensa passando um inteiro no paramentro reward_id - Status Code 404
    [tags]    ct007
    Resgatar recompensa passando um inteiro no paramentro reward_id - Status Code 404
    
CT008: Resgatar recompensa passando um Booleano no paramentro reward_id - Status Code 422
    [tags]    ct008
    Resgatar recompensa passando um Booleano no paramentro reward_id - Status Code 422
    
CT009: Resgatar recompensa passando reward_id vázio - Status Code 422
    [tags]    ct009
    Resgatar recompensa passando reward_id vázio - Status Code 422
    
CT010: Resgatar recompensa já resgatada - Status Code 406
    [tags]    ct010
    Resgatar recompensa já resgatada - Status Code 406
    
CT011: Resgatar recompensa com pontuação maior do que o saldo existente - Status Code 400
    [tags]    ct011
    Resgatar recompensa com pontuação maior do que o saldo existente - Status Code 400
    
CT012: Resgatar recompensa passando sem o Body - Status Code 422
    [tags]    ct012
    Resgatar recompensa passando sem o Body - Status Code 422
    
CT013: Resgatar removendo do Header o x-api-token - Status Code 403
    [tags]    ct013
    Resgatar removendo do Header o x-api-token - Status Code 403
    
CT014: Resgatar alterando a URL do Endpoint - Status Code 404
    [tags]    ct014
    Resgatar alterando a URL do Endpoint - Status Code 404
    


